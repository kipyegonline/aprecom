import React from "react";
import ReactDOM from "react-dom";

import {library} from "@fortawesome/fontawesome-svg-core";
import {icons} from "./pages/utils/icons";
import {HashRouter as Router, Switch, Route} from "react-router-dom";
import {Provider} from "react-redux";
import store from "./redux/store";
import Papa from "papaparse";
import {v4} from "uuid";
import axios from "axios";
import "typeface-raleway";
import "typeface-roboto";
import * as brands from "./pages/Membership/logos";

const agencies=require("../assets/agency.csv");
     

import "../../node_modules/mdbootstrap/css/mdb.min.css";
import "../scss/main.scss";
import "../css/main.css";

import "bootstrap";
import "../../node_modules/materialize-css/dist/js/materialize";
//import "../../node_modules/materialize-css/dist/css/materialize.min.css";



import{getId}from "./pages/utils/helper";
import AboutUs from "./pages/AboutUs/AboutUs";
import AllServices from "./pages/AboutUs/Services";
import ContactUs from "./pages/Contacts/ContactUs";
import Events from "./pages/Events/Events";
import Home from "./pages/Home/Home";
import Management from "./pages/Management/Management";
import { MembersPage} from "./pages/Membership/Agency";
import ApplicationForm from "./pages/Membership/application";
import Admin from "./pages/admin/admin";
import Login, { SignUp, AccountActivation} from "./pages/admin/auth";
import UpdatePassword, { ResetLink } from "./pages/admin/updatepassword";
import MoreMember from "./pages/Management/MoreMember";
import Membership from "./pages/Membership/Membership";
import SocialMediaProfile from "./pages/Membership/EditSocial";
import EditProfile from "./pages/Membership/editProfile.js";
import PartII from "./pages/Membership/App2";
import SeeMore from "./pages/Membership/MemberCard";
import NotFound from "./pages/Ui/NotFound";
import {Error403, Error404} from "./pages/Ui/ErrorPages";
import { council } from "./pages/Management/council";
import AppUploads from "./pages/admin/uploads";




function App(){
    library.add(...icons);

    return(
        <>
            <Provider store={store}>
                <Router>
                    <Switch>
                        <Route exact path='/' component={Home} />
                        <Route  path='/about-us' component={AboutUs} />
                        <Route path="/services" component={AllServices} />
                        <Route  path='/management' component={Management} />
                        <Route  path='/Events' component={Events} />
                        <Route  path='/membership' component={Membership} />     
                        <Route  path='/workshop' component={Membership} />        
                        <Route  path='/contact-us' component={ContactUs} />
                        <Route path="/:agency-firm" component={SeeMore}/>
                        <Route path="/aprecom-:name" component={MoreMember}/>
                        <Route path="/members" component={MembersPage}/>
                        <Route path="/admin" component={Admin} />
                        <Route path="/login" component={Login} />
                        <Route path="/uploads" component={AppUploads} />
                        <Route path="/sign-up-:category" component={SignUp} />
                        <Route path="/password-reset-link" component={ResetLink} />
                        <Route path="/activate-account-:link" component={AccountActivation} />
                        <Route path="/reset-password-:code" component={UpdatePassword} />
                        <Route path="/parte" component={PartII} />
                        <Route path="/error-404" component={Error404} />
                        <Route path="/error-403" component={Error403} />
                        <Route path="/join-aprecom-form" component={ApplicationForm} /> 
                        <Route path="/edit-social-media-:company" component={SocialMediaProfile} />
                        <Route path="/edit-profile-:company" component={EditProfile }/>
            
             
                        <Route component={NotFound} />
              
          
                    </Switch>
                </Router>
            </Provider> 
        </>
    );
}

if(typeof document !=="undefined"){
    ReactDOM.render(<App/>,document.getElementById("root"));

}


  




//FETCH AGENCIES AND DISPATCH TO STORE
function getAgencies() {
    
    $.ajax({
        url:"./php/agencies/agencies.php?getAgencies=true",
        type:"GET",
        dataType:"json",

    })
    // .then(res=>res.json())
        .then(data => {
           
            data=addImages(data,brands);
            data = data.slice(0, data.length);
            store.dispatch({
                type:"ADD_AGENCIES",
                payload:data
            });
     
      
        })
        .catch(err => {
            console.log("error agencies",err, err.message);
            processNew(agencies);
        });  
  
}


function addImages(data,brands){

    return data.map((datam,i)=>{
  
        const brand=brands["image"+ (i+1)];
        datam.pic = brand;
   
        return datam;
    });
}

//send areas of expertise list  to redux store
const fetchAreas=()=>{
    fetch("./php/services/services.php?getAreas=true")
        .then(res=>res.json())
        .then(res=>store.dispatch({type:"ALL_SERVICES",payload:res}))
        .catch(err=>console.log("areas",err));
};
//fetch and disspatch events to STORE
function fetchEvents() {
    fetch("./php/events/addEvents.php?fetchEvents=true")
        .then(res => res.json())
        .then(data => {
            store.dispatch({
                type: "ADD_EVENTS",
                payload:data
            });
        })
        .catch(err=>console.log("events",err));
  
}
//get username
//store.dispatch({ type: "SET_USERNAME",payload: res.data }))
const fetchusername = () => {
    axios.get(`./php/applications/fetchProfiles.php?getUsername=true&uuid=${getId()}`)
        .then(res => store.dispatch({ type: "SET_USERNAME",payload: res.data }))
        .catch(err => console.log(err.message));
};
   
   
//send council to redux store
store.dispatch({ type: "ADD_COUNCIL", payload: council });

//store.subscribe(()=>console.log("App changes",store.getState()))

if(typeof document !=="undefined"){
    document.addEventListener("DOMContentLoaded",function(){
    //processNew(agencies);
   
        setTimeout(()=> fetchAreas(),2000);
        setTimeout(()=> fetchEvents(),2000);
    
        getAgencies();
        fetchusername();
        //check visits
        setTimeout(recordMetrics,5000);
        
    });
}



//process CSV for agencies

function dispatchData(data){
  
    store.dispatch({
        type:"ADD_AGENCIES",
        payload:data.slice(0,31)
    }); 

}

const processNew=(file)=>{
    let count=0,data;
    Papa.parse(file, {
        worker: false, // Don't bog down the main thread if its a big file
        download:true,
        header:true,
        dynamicTyping:true,    
        complete: function(results ) {               
            data=results.data;
            data=addImages(data,brands);
            dispatchData(data);
       
               
         
         

        }//complete
    });//parse
};

//green color: #81c24f rgb(129,194,79)
//blue color:3#aabeg rgb(58,187,233 )
//red: #e83e3c rgb(242,154,59)
//orange: #f29a3b rgb(242,154,59)

function recordMetrics() {
    let metric = "mdcccxxiv";

    let TwentFoursHours = 1000 * 60 * 60 * 24;
    let now =  Date.now();
    const getItem=()=>JSON.parse(localStorage.getItem(metric));
    const setItem=(now)=>localStorage.setItem(metric, JSON.stringify(now));
    if (localStorage) {

        //visitor
        if (getItem()=== null) {
            
            //send to server first visit
            fetch(`./php/metrics/metrics.php?addMetrics=true&device=${window.innerWidth}&path=${location.pathname}`)
                .then(res => res.text())
                .then(res => {
                    
                    const id = res;
                    now = now + "-" + id;
                    setItem(now);
                })
                .catch(err=>console.log("Metrics",err));
                
        } else {
            
            let daysAgo = getItem().split("-");
            daysAgo = Number(daysAgo[0]);
            //number of seconds since last visit to the site
            const then = now - daysAgo;
           
            //if its more than 24 hours
            if (then > TwentFoursHours) {
                //clear and set fresh 
                let id = getItem().split("-");
                id = Number(id[1]);
                now = now + "-" + id;
                let daysOff=parseInt(then/TwentFoursHours);
                setItem(now);
                
               
                fetch(`./php/metrics/metrics.php?UpdateMetrics=true&
                metricsId=${id}&device=${window.innerWidth}&daysOff=${daysOff}&path=${location.pathname}`)
                    .then(res=>res.text())
                    .then(res => {
                        console.log(res);
                    })
                    .catch(err=>console.log("Metrics",err));
                

            } else {
                //your second or 3rd visit in 24 hours
            }

        }
    }
}