import { C } from "./types"
import { getId } from "../../pages/utils/helper"
import axios from "axios"

export const fetchAppData=(payload)=>({type:C.GET_APPDATA, payload})

export const fetchDirectors = payload => ({ type: C.GET_DIRECTORS, payload })
export const fetchClients = payload => ({ type: C.CLIENT_SERVICES, payload })
export const fetchEmployees = payload => ({ type: C.GET_EMPLOYESS, payload })
export const setUserName=payload=>({type:C.SET_USERNAME,payload})
export const setProfile = payload => ({ type: C.SET_PROFILE, payload })
export const addDirector = payload => ({ type: C.ADD_DIRECTOR, payload })
export const addEmployees=payload=>({ type: C.ADD_EMPLOYEE, payload })
export const addClients=payload=>({type:C.ADD_CLIENT,payload})