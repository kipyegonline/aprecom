import {C} from "./types"
const initState = {
    
    appData: {},
    directors: [],
    clientServices: [],
    employees: [],
    username: {},
    profile:[]
    
}
function appReducer(state=initState,action) {
    
    switch (action.type) {
        case C.GET_APPDATA:
            const profile = action.payload[0]['companyProfile'];
            return {
                ...state,
                appData: action.payload[0],
                profile:profile===null ||profile == "NULL"  ? [] :profile.split("||")
               
            }
        
        case C.GET_DIRECTORS:
            return {
                ...state, directors:action.payload
            }
        case C.CLIENT_SERVICES:
                return {
                    ...state, clientServices:action.payload
            }
        case C.GET_EMPLOYESS:
                return {
                    ...state, employees:action.payload
            }
        case C.SET_USERNAME:
        return {
            ...state, username:action.payload
            }
        case C.SET_PROFILE:
           const setProfile=action.payload
                return {
                    ...state, profile: setProfile !== null || setProfile !="NULL" ? action.payload.split("||") :[]
                    }
        case C.ADD_DIRECTOR:
            return {
                ...state,
                directors:[...state.directors,  action.payload]
            }
            case C.ADD_EMPLOYEE:
                    return {
                        ...state,
                        employees:[...state.employees,  action.payload]
                    }
                    case C.ADD_CLIENT:
            return {
                ...state,
                clientServices:[...state.clientServices,  action.payload]
            }
            
    }
    return state;
}
export default appReducer;