import {combineReducers} from "redux"
import agencyReducer from "./agencies/agencyreducer"
import appReducer from "./appReducer/appReducer"
import ServicesReducer from "./services/servicesReducer"


export default combineReducers({agencyReducer,appReducer, services:ServicesReducer});