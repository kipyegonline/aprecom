import {C} from "./types"
import {v4} from "uuid"

export const addAgency=agency=>({
    type:C.ADD_AGENCIES,
    payload:agency
})

export const editProfile=payload=>({
    type:C.EDIT_PROFILE,
    payload
})

export const addSocialMedia = payload => ({
    type: C.SOCIAL_MEDIA,
    payload
})

   
export const getSocials = (payload) => ({
    type:C.GET_SOCIALS,
    payload
})

