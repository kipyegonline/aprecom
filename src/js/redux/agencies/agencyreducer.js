import { C } from "./types"
import {sendServices} from "./actions"
const initState = {
    
    council: [],
    agencies: [],
    profileEdit: {},
    events: [],
    socialMedia:{}
    
};

export default function agencyReducer(state=initState,action){
    switch(action.type){
        case C.ADD_AGENCIES:
            
            return { ...state, agencies: action.payload }
            break;
       
        case C.ADD_EVENTS:
            return {...state, events: action.payload}
            break;
        
        case C.ADD_COUNCIL:
                       return {...state, council:[...action.payload]}
                       break;
         case C.EDIT_PROFILE:
            return {
                ...state,
                profileEdit: state.agencies.find(agency => agency['id'] == action.payload) }
        case C.SET_PROFILE:
            return { ...state, profile: action.payload == null || undefined ? [] : action.payload.split("  ") }
        case C.SOCIAL_MEDIA:
            const social = state.agencies.find(agency => agency['company'] == action.payload)
            
            return { ...state, socialMedia: social.socials }
        
        case C.GET_SOCIALS:
            return  {...state, socialMedia:action.payload}
           default:
               return state;
    }
   
}