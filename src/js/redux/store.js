import {createStore, applyMiddleware, compose} from "redux"
import thunk from "redux-thunk"

import RootReducer from "./reducer"

const devops= window.__REDUX_DEVTOOLS_EXTENSION__
? window.__REDUX_DEVTOOLS_EXTENSION__()
    : f => f
const middlewares=[thunk]
const store = createStore(RootReducer, compose(applyMiddleware(...middlewares),devops));

export default store;