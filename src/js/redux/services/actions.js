import {C} from "./types"
export const addService = service => ({
    type:C.ADD_SERVICE,
    payload:service,
    
})

export const deleteService=(id)=>({
  type:C.DELETE_SERVICE,
  payload:id
})

export const clearServices = () =>({
    type: C.CLEAR_SERVICES,
    payload:[]  
})
export const ourServices = (payload) => {
    return {
        type: C.OUR_SERVICES,
        payload
    }
}