import {C} from "./types"
const initState = {
    services: [],
    allServices: [],
}
    
function servicesReducer(state=initState, action) {
    


    switch (action.type) {
        case C.ALL_SERVICES:
                return {...state,allServices:action.payload}
                
        case C.OUR_SERVICES:
            return { ...state, services: action.payload };
       
       case C.ADD_SERVICE:
            const service = {
                service: action.payload.area,
                id: action.payload.index,
                
            }
           
       return{...state, services:[...state.services,service]}
           
        
       
        case C.CLEAR_SERVICES:
                return {...state, services:[...action.payload]};
                
            
       case C.DELETE_SERVICE:
                
        return {...state, services:state.services.filter(service=>service.id !== action.payload)}
        
        default:
            return state;
    }
}
export default servicesReducer;