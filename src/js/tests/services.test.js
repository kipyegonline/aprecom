import Enzyme, {shallow, mount} from "enzyme";
import Adapter from "enzyme-adapter-react-16";
import servicesReducer from "../redux/services/servicesReducer";
//import {Services} from "../pages/Membership/App2";
import { C } from "../redux/services/types";

Enzyme.configure({ adapter: new Adapter() });

//testing reducers
describe ("services reducer",()=>{
    test("allServices",()=>{
        const state={services:[]};
        const action={
            type:C.ADD_SERVICE,
            payload:{index:2, area:"branding"}
        }
        const res=servicesReducer( state,action)
        expect(res).toEqual({"services":[{id:2,service:"branding"}]})
    })
    const _click = jest_fn();
    const comp = mount(<Services onClick={_click} />)
    test('Enzyme fudge', () => {
        
        expect(comp.find(".service-areas").length.toBe(0))
    })
    test("Enzyme adapter", () => {
        
        comp.find("button.bg-aprecom3").simulate('click')
        expect(_click).toBeCalled()
    })
})