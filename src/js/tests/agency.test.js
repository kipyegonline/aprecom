import { C } from "../redux/agencies/types"
import store from "../redux/store"
import { setUserName } from "../redux/appReducer/actions"

describe("testing applications", () => {
   
    beforeAll(() => {

        store.dispatch(setUserName("Jules"));

        test("set username", () => {
            expect(store
                .getState()
                .appReducer
                .username).toBe("Joyce")
        })

    test("username defined", () => {
            expect(store.getState().appReducer.username).toBeDefined()
        })
    })//before all block

    test('real love', () => {
        expect(3+5).toBe(8)
    })
})