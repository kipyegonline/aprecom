import {useRef, useState, useEffect} from "react";
import {connect} from "react-redux";
import PropTypes from "prop-types";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import axios from "axios";
import Layout from "../Ui/Layout";
import Modal from "../Ui/Modal";
import {userTrackerId} from "../utils/helper";


function ContactUs(){
  
 

    return(
        <>
            <Layout >
        
                <div className='row mt-4'>
                    <Map/>
                    <Side posts={[]} />
          
                </div>
          
          
            </Layout>
        </>
    );
}


function Map(){
    const handleMap=()=>$(".map-container").find("iframe").addClass("clicked");
          

    const handleMapLeave=()=>$(".map-container").find("iframe").removeClass("clicked");

    return(
        <div  className="col-md-8  ">
            <div
                onClick={handleMap} 
                onMouseLeave={handleMapLeave} 
                className="embed-responsive embed-responsive-16by9 map-container mb-1 mb-lg-0">
      
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3988.8065538287565!2d36.78278711397043!3d-1.2903585359926468!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x182f10a6fd6a6461%3A0xd0985718d69440c6!2sApex%20Porter%20Novelli!5e0!3m2!1sen!2ske!4v1566589140707!5m2!1sen!2ske" 
                    width={500} height={700} frameBorder={0} style={{"border":0}} allowFullScreen={true}></iframe>
            </div>
      
        </div>
    );
}
const Side=()=>{
    const [name,setName]=useState("");
    const [email,setEmail]=useState("");
    const [subject,setSubject]=useState("");
    const [message,setMessage]=useState("");
    const [success, setSuccess]=useState("");
    const [error,setError]=useState("");
    const [spinner,setSpinner]=useState(false);
  
    const form= useRef(null);
  
    const handleSubmit=e=>{
        e.preventDefault();
        setSpinner(true);


        const data = { name, email, subject, message, userId: userTrackerId() };
        
        //send to server
        $.ajax({
            url:"./php/mail/mail.php?handleMails=true",
            data,
            method: "post",
            dataType:"json"
  
        })
            .then(res => {
               
                setTimeout(()=>{
                    setSpinner(false);
                   
                    if(res.status===200){
                        setSuccess(res.msg);
                        form.current.reset();       
                        setMessage("");
                        setTimeout(()=>setSuccess(""),3000);
                    }
                    if(res.status===201){
                        setError(res.msg);
                        setTimeout(()=>setError(""),3000);
                    }
      
     

                },3000);
    


            })
            .catch(err=>{
                setTimeout(()=>{
                    console.log(err);
                    setSpinner(false);      
                    setError("Error submitting.Check your network connection");
                    setTimeout(setError(""),3000);
                },3000);
  
            });



    
    };
  
  
    return (
        <div  className="col-md-4 card box-shadow-white">
   
            <form className="form p-3 my-3" onSubmit={handleSubmit} ref={form}>
    
                <p className="text-center ">
                    <FontAwesomeIcon icon="envelope"  size="3x" color="brown"/></p>
                <div className="form-group">
                    <label><span style={{color:"red"}}>*</span> Name</label>
                    <input type="text" className='form-control'required placeholder="Enter name"
                        onChange={(e)=>setName(e.target.value)}/>
                </div>
                <div className="form-group">
                    <label><span style={{color:"red"}}>*</span> Email address</label>
                    <input type="email" className='form-control'required placeholder="Enter email address"
                        onChange={(e)=>setEmail(e.target.value)}/>
                </div>
                <div className="form-group">
                    <label><span style={{color:"red"}}>*</span> Subject</label>
                    <input type="text" className='form-control'required placeholder="Enter subject"
                        onChange={(e)=>setSubject(e.target.value)}/>
                </div>
                <div className="form-group">
                    <label><span style={{color:"red"}}>*</span> Message</label>
                    <textarea className='form-control mb-2' required
                        cols={350} 
                        rows={3} 
                        value={message}
                        placeholder="Enter message"
                        onChange={(e)=>setMessage(e.target.value)}></textarea>
                </div>
          
                <p className="text-center">
                    <FontAwesomeIcon icon="spinner" style={{display:(spinner) ? "block" :"none"}}  size="3x" color="green" pulse/>
                </p>
                <p className="text-success">{success}</p>
                <p className="text-danger">{error}</p>
                
                <input type="submit" className='btn bg-aprecom1  btn-block mt-3' value="Send"/>
                
            </form>
        </div>
    );};
//store.subscribe(()=>console.log("subscription", store.getState()))


ContactUs.Proptypes={
    handleSubmit:PropTypes.func.isRequired,
    message:PropTypes.string
};


const mapDispatchToProps=(dispatch)=>({
    submit: payload=>dispatch(submitMsg(payload)),
    add:payload=>dispatch(addMsg(payload))
});
const mapStateToProps=state=>({posts:state.submited});
export default connect(mapStateToProps,mapDispatchToProps)(ContactUs);

const Success=()=>(<p className="alert alert-success p-2 my-2">Your message has been delivered succcesfully.We will respond shortly</p>);
const Fail=()=>(<p className="alert alert-danger p-2 my-2">failed to send,Try again later.</p>);