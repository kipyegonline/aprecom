
import {useState, useEffect} from "react";
import axios from "axios";
import Layout from "../Ui/Layout"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

export default function AllServices(){
    const [areas, setAreas] = useState([]);
  useEffect(() => fetchServices("./php/services/services.php?getAreas=true"), []);
  
  const fetchServices=(url)=>{
    axios
       .get(url)
           .then(res =>setAreas(res.data))
           .catch(err => console.log(err.message))
   
   }//inner func
   
    return(
        <Layout>
            <div className="row jumbotron mt-3 ">
                <div className="col-md-12 col-sm-12 mx-auto">
                    {areas.length>0 ?
                <ul className="list-group mt-3">
         { areas.map((area, i) =>
                  <li key={area.id} className="list-group-item">{i +1}. {area.area}</li>)}
              </ul>  : <p className="text-center my-5">
                  <FontAwesomeIcon icon="spinner" size="3x" color="green" pulse/></p> }

                </div>

            </div>
    
    </Layout>
    )
    
}