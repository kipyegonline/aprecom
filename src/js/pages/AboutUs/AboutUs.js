import {Link} from "react-router-dom"
import Layout from "../Ui/Layout"
import banner from "../../../assets/images/logo/aprecom_banner.jpg"
function AboutUs(){
    return(
        <Layout>
            
            <div className="col-md-12 bg-light col-sm-12 my-2 p-3 border-radius-2 card">
           
           <img className="img-fluid w-100 mb-2" src={banner} alt="banner"/>
<p><b>The ASSOCIATION OF PUBLIC RELATIONS AND COMMUNCATION MANAGEMENT FIRMS (APReCoM) </b> 
is a Chapter of the Public Relations Society of Kenya (PRSK) and the Business Management Organisation 
for Kenyan consultancy firms engaged in the practice of public relations and Communications.</p>

<p>Registered on <b>22<sup>nd</sup> November, 2012 </b> under the Society’s act, APReCoM is committed both to the advancement of the profession in Kenya and 
    to the nurturing of the professional goals of our members. <br/>

In addition to encouraging the highest attainable professional standards from members,
                     the Association acts as a spokesperson and regulator for the PR industry in 
                     collaboration with the Public Relations Society of Kenya (PRSK),
  seeking to instil confidence in public relations as a whole.</p>
                <p><Link to={"/services"}>Services provided by APReCoM member firms</Link></p>
  </div>
            </Layout>
    )
}
export default AboutUs;