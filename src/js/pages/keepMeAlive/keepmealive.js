import React, { useEffect, useState } from "react";
import logo_md from "../../../../logo_md.png";

const KeepMeAlive = () => {
const [live,setLive]=useState("d-none")
    let isAlive;
    let movetimer, randomTimer;
    
    const handleLeave = () => isAlive = false;
    
    const handleEnter = () => isAlive = true;
    const handleScroll = () => isAlive = true;
    const handleClick = () => isAlive = true;
    

    const handleMove = (e) => {
        
        isAlive = true;
        
        let x = e.pageX, y = e.pageY;
        
        clearTimeout(movetimer)
        movetimer = setTimeout(() => {
           
            if (x === e.pageX && y === e.pageY) {
                isAlive = false;
            }

        },60000)
    } 


    let lastCoord;
    const randomCoords = (num) => {
        const run=Math.floor(Math.random() * num)
        if (run===lastCoord) {
            return randomCoords;
            
        }
        
        lastCoord = run;
        return run;
    }
    const randomPos = () => {
        const cover = document.querySelector(".alive-cover").getBoundingClientRect();
        if (cover) {
            const img = document.querySelector(".keep-me-alive-img")
        const h=randomCoords(cover.height);
        img.style.top =h + "px"
            img.style.left = randomCoords(cover.width) + "px";
            
        }
        
  
  }
    let counter = 0;
    const checker = () => {
        
        if (isAlive == undefined) {
         setLive("d-none")
            counter = 0;
            clearInterval(randomTimer)
            
     }else if(isAlive===true){
            setLive("d-none");
            counter = 0;
            clearInterval(randomTimer)
            
        } else{
            //set up classes
            counter += 1;
            if (counter === 240) {
                
                setLive("keep-me-alive")
                setTimeout(() => setLive("keep-me-alive alive-longer"), 100);
                setTimeout(() => randomTimer = setInterval(randomPos, 3000), 3000);
            
            }
            
        }
        
    }
    /*Use effect*/
    useEffect(() => {
        if (window.innerWidth > 1000) {
            let timer=setInterval(checker,1000)
     return ()=>clearInterval(timer)
        }
        
 },[])
 
    if (window.innerWidth > 1000) {
        
        document.body.addEventListener("mousemove", handleMove);
        document.body.addEventListener("mouseenter", handleEnter);
        document.body.addEventListener("mouseleave", handleLeave);
        window.addEventListener("scroll",handleScroll)
        document.body.addEventListener("click",handleClick)
       
       
        
    }
    const styles = {
        color: "#fff",
        marginTop: window.innerHeight / 2,
        
        fontSize:"4rem",
        zIndex:120
    }
    return (
        <div className={`row alive-cover ${live}`}>
            <span className="close float-right"></span>
            <div className="col-md-12 col-md-12 ">
                <img src={logo_md} className="keep-me-alive-img rounded-circle" alt="kepp me alive" />
                <h3 className="text-center mx-auto  red lighten-1 p-1" style={styles}>APReCoM  </h3> 

            </div>

        </div>
    )
}
export default KeepMeAlive;



