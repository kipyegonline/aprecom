import okoth_obado from "../../../assets/images/council/okoth_obado.png"
import okoth from "../../../assets/images/council/okoth_obado2.png"
import alfred_nganga from "../../../assets/images/council/alfred_nganga.png"
import desiree_gomez from "../../../assets/images/council/desiree_gomez.png"
import josiah_mwangi from "../../../assets/images/council/josiah_mwangi.png"
import lawrence_gikaru from "../../../assets/images/council/lawrence_gikaru.png";
import nisha_van_hoek from "../../../assets/images/council/nisha_van_hoek.png"
import ooga from "../../../assets/images/council/ooga.png"
import peter_mutie from "../../../assets/images/council/peter_mutie.png"
import sam_karanja  from "../../../assets/images/council/sam_karanja.png"
import oga_omaga from "../../../assets/images/council/oga_omaga.png"
import logo from "../../../assets/images/logo/logo.jpg"
const omangaBio=`Ooga is experienced in B-to-C marketing and B-to-B sales working with private sector across a diverse range of industries, government and semi-government. His core professional competencies include development and implementation of communication and marketing strategies, building and maintaining client relationships, managing supplier negotiations, media lobbying, crisis management, digital content creation, crafting speeches and presentations, copy writing, and script writing for documentaries and infomercials.||

He is an Executive Committee and Founding Member of the Association of PR and Communications Management Firms (APReCoM), Kenya where he serves as Assistant Secretary. Ooga is involved in national youth leadership and development initiatives being a member of the group that formulated Kenya's Youth Leadership and Entrepreneurship Strategy as part of Vision 2030 development blueprint.||

In his spare time Ooga reviews food, entertainment and travel in a blog. Recently he took up studying cinematography as a  hobby, fascinated by how different filming techniques can draw the audience into a plot. A keen road-tripper, Ooga continues to explore both Kenya and other parts of the world.`
const pm=` Mr. Peter Mutie, the Executive Chairman and CEO of PICL, is a distinguished communication professional who has worked in a dynamic mix of sectors in Kenya and Africa. Peter has held senior positions in corporate leadership. || He is Former Chief Executive Officer of Kenya Film Commission, Board Member of Communication Authority of Kenya, BrandKenya Board, Kenya Civil Aviation Authority, among other public and private sector organisations. He is also the immediate Past President of African Public Relations Association (2011 – 2015) and Past Chairman of PRSK (2007 – 2011). 
|| Besides being a Fellow of PRSK, Peter is also a recipient of PRSK Golden Honours, the highest and lifetime award that recognizes outstanding achievement in Public Relations in Kenya. He has taught media and strategic communication in a couple of universities in Kenya. His academic credentials include Masters in Business Administration (MBA), BA (Econ), Post Graduate Diploma in Mass Communication, among other postgraduate qualifications.

He has served in a range of dynamic sectors including publishing, media, energy, development banking, health, entertainment and the UN ||`
const samK=`Sam is an Executive Director H+K Strategies, East Africa. He was previously the Managing Director for the H+K Kenya office, leading a team of upto 18 Communications professionals in executing programs in the market. || Prior to the Managing Director role, he oversaw strategic communication planning, Public Relations counsel and implementation of plans and programs for Airtel across its 17 African operations. He brings a deep insight into the media and PR landscapes from various markets in the continent. His previous stint at Ogilvy PR saw him work with some of the leading multinational brands in the continent such as Coca-Cola, Nokia, Hewlett Packard, Microsoft, Unilever, Reckitt Benckiser, Celtel, amongst others.||

Sam has over 12 years’ experience in various Public relations and marketing Communications facets such as financial/investor relations, crisis communications, Brand PR, Sports Sponsorship, social marketing, internal communications, behavior change communications, media relations and event management. He earned his stripes during his tenures at a leading media company with a presence in East Africa, two stints at leading Public Relations Consultancies and at a publicly listed retailer as the Communications Manager. `
const dG="Desiree is a strategic Consultant, including Communication Strategies, PR Specialization and PR training, A respected industry leader, she is a PRSK Lifetime Achievement ‘Golden Honours’ Award winner.|| She has also been at the forefront in providing mentorship and training opportunities for new entrants into the PR profession and has run an internship and job-shadowing program that seeks to tap new talent from university and integrate their abilities into future PR professionals."
const joe=`Josiah has worked in the communications and public relations industry for the past 15 years. He has worked with global mining firms, leading FMCGs, development partners, global multinationals, and government agencies in several countries in Africa.  His skills include communication strategy development, campaign design, corporate communications, investor relations, crisis communications, media relations, CSR & Sustainability communications and stakeholder relationship management. ||

 

|| A GRI certificate holder, Josiah has great interest in sustainability and the role communication plays in helping businesses understand that this plays a key role in determining their reputation. He’s interacted with sustainability programmes - the Unilever Sustainable Living Plan and Coca-Cola’s Live for A Difference, and Lafarge’s Building Better Cities – working with the local teams to bring the plans to life in Africa. ||

 

An avid Rotarian, Josiah is a sports enthusiast and is also the immediate Vice Chairman of the Nairobi Basketball Association. `
const lgikaru=`Lawrence, the founder of Apex Porter Novelli, brings over 20 years of strategic communication experience to bear for APN clients. In just under a decade has helped build the agency to deliver work for clients that measures to world class standards.. He has build this experience providing leadership in the design and implementation of strategic communication campaigns across the private, service, corporate, social, health, consumer and public sector.|| He has worked in Kenya, Eastern & Southern Africa and the US market.   Under his stewardship APN has built a diverse portfolio of clients and delivered campaigns that have not only delivered real business results for clients but have received the highest industry recognition in the world. The awards include a Global Sabre from the Holmes Report, several Golden World Awards from the International Public Relations Association and the coveted Grand Award from the United Nations in New York.||

Lawrence provides overall leadership to deliver on APN’s value proposition and retains a hands-on-role for the work of APN clients through coaching his team on strategic communication planning to align communication with the business goals of clients and in evaluation and measurements.  Lawrence believes in “ growing slowly and growing right. The philosophy of APN rests on growth through retention of clients which allows for depth and long-term productive relationships”||

||
Lawrence is active in humanitarian service and has served as President of the Rotary Club of Milimani, Nairobi.`

const alf=`
Alfred Ng’ang’a is a passionate public communication champion who takes pride in client success. Trained at the Moi University and the Kenya Institute of Mass Communication, he has enjoyed a successful career working on almost all leading corporate accounts in the East Africa Region.||

He continues to work/has previously handled key accounts such as: Sanlam Kenya, Savannah Cement, Kenyatta National Hospital, Samsung Electronics East Africa, Safaricom Limited, TPS Serena Hotels, Nakumatt Holdings, Bavaria Auto (BMW), Equity Bank, Simba Colt Motors, Reckitt Benckiser East Africa and James Finlay/Swire Group, Janus Continental Group, Idemia Group among others.||

On public affairs, Alfred has handled key clients including the President’s Delivery Unit, Office of the Director of Public Prosecutions (ODPP), National Social Security Fund (NSSF), Kenyatta National Hospital, Vision 2030 Delivery Secretariat, Kenya Tourist Board, Tourism Finance Corporation, among others.||

He served on as a member of the Government of Kenya Taskforce for the Improvement of Government Information and Public Communications Functions to Align Them with Emerging Public Sector Dynamics and Expectations.||

An Ogilvy Professional Awards winner, Alfred’s inter disciplinary experience covers public affairs counsel, strategic corporate communications, media liaison and relations. `
export const obado={
    id:1,
    name:"Okoth Obado",
    email:"obado@aprecom.co.ke",
    title:"Chairman.",
    bio:"",
    job:"Managing Director, Redhouse PR.",
    pic:okoth_obado,
    twitter:"https://www.twitter.com/obadojj",
    linkedin:"https://www.linkedin.com/in/okoth-obado-0a0bab14/"

}
export const alfred={
    id:2,
    name:"Mr. Alfred Ng’ang’a Ndung’u.",
    email:"anganga@aprecom.co.ke.",
    title:"Committtee Member",
    bio:alf,    
    pic: alfred_nganga,
    job:"Managing Partner, Oxygene Marketing and Communications Limited.",
    twitter:"https://www.twitter.com/alfienganga",
    linkedin:"https://www.linkedin.com/in/alfred-nganga-59375512/"
}

export const desiree={
    id:3,
    name:"Desiree Gomez",
    email:"dgomez@aprecom.co.ke",
    title:"Vice-chair",
    bio:dG,
    job:"Partner, Engage Burson Cohn + Wolfe.",
   
    pic:desiree_gomez,
    twitter:"https://www.twitter.com/desgomes",
    linkedin:"https://www.linkedin.com/in/desiree-gomes-6ab024a/"
}
export const josiah={
    id:4,
    name:"Josiah Mwangi",
    email:"jmwangi@aprecom.co.ke",
    title:"Committtee member",
    bio:joe,
    job:"Director, Client, Development & Strategic Services, Apex Porter Novelli.",
    
    pic:josiah_mwangi,
    twitter:"https://www.twitter.com/mwangijosiah",
    linkedin:"https://www.linkedin.com/in/josiah-mwangi-734a59a/"
}


export const lg={
    id:5,
    name:"Lawrence Gikaru",
    email:"lgikaru@aprecom.co.ke",
    title:"Immediate- past chair",
    bio:lgikaru,
    job: "Managing Director, Apex Porter Novelli.",   
    pic:lawrence_gikaru,
    twitter:"https://www.twitter.com/lawrencegikaru",
    linkedin:"https://www.linkedin.com/in/lawrence-gikaru-923b4012/"
}
export const nisha={
    id:6,
    name:"Nisha Van Hoek",
    email:"nishavanhoek@aprecom.co.ke",
    title:"Treasurer",
    bio:`Nisha is a seasoned professional with over 26 years of experience in the PR and Marketing Industry.  She has worked as a Marketing Manager for Sterling Developers Ltd and also for Crown Paints (K) Ltd in different capacities.|| Nisha has been instrumental in managing communication and special events for British Airways (Including the launch of its E-ticketing platform), Agricultural and Processed Food Products Export Development  Authority (APEDA) India (Organizing Buyer Seller Meet both in Kenya and Tanzania) Bobmil Ltd, Seven seas Technologies and Knowledge Transfer Centre, Kenya Re, LG Electronics (Air Conditioning Academy launch), Architectural Association of Kenya (AAK) – Organizing their Annual Golf Tournament, Institute of Quantity Surveyors of Kenya(IQSK)
    -Organizing their Annual Golf Tournament,National Construction Authority (NCA)- Organizing the First Annual Construction
    ,Research Conference Exhibition (ACORCE) among others.|| She is also an active member of the Public Relations Society of Kenya (PRSK) and the treasurer APReCoM.
    Her experience in successfully orchestrating various Corporate Communications and Marketing
    strategies has greatly leveraged APRM as a leading Integrated Communications and Marketing company",`,
    job:"Managing Director, Advance PR and Marketing",
    
    pic:nisha_van_hoek,
    twitter:"https://www.twitter.com/hoeknisha",
    linkedin:"https://www.linkedin.com/in/nisha-van-hoek-6753395a/"
}
export const omanga={
    id:7,
    name:"Ooga Omanga",
    email:"omanga@aprecom.co.ke",
    title:"Secretary",
    bio:omangaBio,
    job:"Partner, Exclamation Marketing Limited.",
    
    pic:ooga,
    twitter:"https://www.twitter.com/oogaomanga",
    linkedin:"https://www.linkedin.com/in/oogaomanga/"
}
export const mutie={
    id:8,
    name:"Peter M. Mutie, FAPRA, FPRSK",
    bio:pm,
    title:"Committee member",
    job:"Executive Director & CEO, Peterson Integrated Communications. ",    
    pic:peter_mutie,
    twitter:"https://www.twitter.com/mutiepetermu",
    linkedin:"https://www.linkedin.com/in/peter-mutie-27a06a25/"
}
export const sam={
    id:9,
    name:"Sam Karanja",
    email:"samkaranja@aprecom.co.ke",
    title:"Assistant Secretary",
    bio:samK,
    job:"Executive Director, H+K Strategies,EA",    
    pic:sam_karanja,
    twitter:"https://www.twitter.com/samkaranja",
    linkedin:"https://www.linkedin.com/in/samuel-karanja-820b1b53/"
}

export const okotho={
    id:11,
    name:"Okoth Obado",
    email:"obado@aprecom.co.ke",
    title:"Outreach, monitoring and Evaluation",
    bio:"",
    job:"Managing Director, Redhouse PR",
    pic:okoth,
    twitter:"https://www.twitter.com/aprecomKenya",
    linkedin:"htpps://www.linkedin.com/aprecomKenya"

}

export const council=[obado,alfred,desiree,josiah,lg,nisha,omanga,mutie,sam,okotho]

