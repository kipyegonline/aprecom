import {Link} from "react-router-dom";
import Layout from "../Ui/Layout";
import * as council from "./council";
import leadership from "../../../assets/images/slides/leadership.jpg";
import leadership_md from "../../../assets/images/slides/leadership_md.jpg";
import leadership_sm from "../../../assets/images/slides/leadership_sm.jpg";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";


function Management() {
    
    const tabletResize = () => {
        let width = window.innerWidth;
        
        if(width>=468 && width <=1030){
            return "col-md-4";
        }else{
            return "col-md-2";
        }
    };
    
    const officebearers=`card ${tabletResize()} col-sm-12 `;
    const committee="card col-md-4 col-sm-12 ";
    return(
        <>
            <Layout>
                <div className="row">
                    <div className={"col-md-12 col-sm-12"}>
                        <h3 className="text-center text-header text-lg mb-1 p-1">The Management team</h3>
                        <CoverPhoto/>
        
                    </div>
                    <OfficeBearers officebearers={officebearers}/>
                </div>{/*row */}
        
                <div className="row mt-2">
                    <div className="col-md-12">
                        <h5 className="text-center p-2 my-1 bg-light">Committee Members</h5>
                    </div>
                    <Member {...council.alfred} classlist={committee}/>
                    <Member {...council.josiah} classlist={committee}/>
                    <Member {...council.mutie} classlist={committee}/>
                </div>{/*row */}
                <div className="row mt-2">
                    <div className="col-md-12"> 
        
                    </div>
         
                </div> {/*row*/}
            </Layout></>
    );
}
export default Management;
const CoverPhoto=()=>(<>
    <img  src={leadership}  
        className="img-fluid w-100 img-rounded" alt="aprecom leadership"/>
    <figure classlist="mt-1"><figcaption className="p-1 font-weight-bold">From Left:Sam Karanja,Alfred Ng'ang'a, Ms. Desiree Gomez,Okoth Obado,Lawrence Gikaru,Nisha Van Hoek, Oooga Omanga and Peter Mutie</figcaption></figure>
</>);

const Member=({id,name,pic,title,job,email,bio,twitter,linkedin,classlist})=>(
    <div className={classlist}>
        <div className="card-header">{title}</div>
        <img src={pic} title={name} className="card-img-top img-thumbnail p-1" alt={name}/>
        <div className="card-body">
            <h5 className="card-title">{name}</h5>
            <h6 className="card-subtitle mb-1 text-muted">{job}</h6>
            <CouncilSocial twitter={twitter} linkedin={linkedin}/>

            <Link className="text-white btn bg-aprecom1 mt-2 p-1 btn-block" to={`/aprecom-${name}`}>
                <FontAwesomeIcon icon="info" /> More</Link>

  
 

        </div>
    </div>);
const OfficeBearers=({officebearers})=>(<div className="row mt-2">
    <div className="col-md-12">
        <h5 className="text-center p-2 my-1 bg-light">Committee Office Bearers</h5>
    </div>
    <Member {...council.obado} classlist={officebearers}/>
    <Member {...council.desiree} classlist={officebearers}/>
    <Member {...council.lg} classlist={officebearers}/>
    <Member {...council.omanga} classlist={officebearers}/>
    <Member {...council.nisha} classlist={officebearers}/>
    <Member {...council.sam} classlist={officebearers}/>


</div>);
//srcSet={`${leadership_md} 674w, ${leadership_sm} 100w`} 


export const CouncilSocial=({twitter,linkedin,})=>(
    <p className="my-1 mr-3">
           <a href={twitter} target="_blank"> <FontAwesomeIcon className=" mr-2" icon={["fab","twitter"]} size={"2x"} color="#00acee "/></a>
        
            <a href={linkedin} target="_blank"><FontAwesomeIcon className="" icon={["fab","linkedin"]} size={"2x"} color="#0072b1"/></a>
            </p>
)