import {useState, useEffect,useRef} from "react";
import {Link} from "react-router-dom";
import Layout from "../Ui/Layout";
import {council} from "./council";
import {CouncilSocial} from "./Management"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

const MoreMember = ({ match }) => {
    
    let user = match.params.name;
    window.scrollTo({ top: 0, left:0, behavior: "smooth" });
    
    const id = Number(council.find(coun => coun.name === user).id);
    
    const [previous,setPrevious]=useState(0);
    const [next,setNext]=useState(id);
    const [info,setInfo]=useState({});
    const [bios,setBios]=useState([]);
    

    useEffect(() => {
    
        finder(id);
    

    }, []);

    const finder=id=>{
        const user=council.find(member=>member.id==id);
        setInfo(user);
    
        setBios(user.bio.split("||"));
    };

    const handleNext=(id)=>{



        setNext(locked=>{
        
            if(locked==council.length-1)
            {
                finder(1);
    

                return 1;
            }else{
                const val= locked+1;       
                finder(val);
                return val;
            }
        
        });

   
    };

    const handlePrevious=(index)=>{
   
        setNext(locked=>{
            if(locked==1)
            {
                finder(council.length-1);
                return council.length-1;
            }else{
                const val=locked-1;       
                finder(val);
                return val;
            }
 
        });

    };
    const checkWidth=()=>{
        if(window.innerWidth<768){
            return"w-100 p-1  rounded-circle ";
        }else if(window.innerWidth<1030){
            return"w-100 p-1  rounded-circle ";
        } else {
            return  "w-75 my-auto mx-auto  rounded-circle";
        }
    };
    
        
    const { title, pic, job, bio, name, twitter,linkedin} = info;

  
    
    
    return  (
        <>
            <Layout>
                <div className="row">
                    <Arrows hp={handlePrevious} hn={handleNext} next={next} id={id}/>
               
                
                </div>
                {/**top */}
                <div className="row  my-2 " >
                    
                        <div className="card col-md-4 col-sm-12" style={{background:"#ddd"}}>
                            <div className="card-header font-weight-bold">{title}</div>
                            <img src={pic} title={name} className={checkWidth()} alt={name}/>
                            <div className="card-body">
                                <h5 className="card-title">{name}</h5>
                                <h6 className="card-subtitle mb-2 text-muted">{job}</h6>
                                <CouncilSocial twitter={twitter} linkedin={linkedin}/>
                                
  
                        </div>
                            
    
                      
                    </div>{/**col */}
                    <div className="col-md-8 py-3 col-sm-12" style={{background:"#ddd"}}>
                    
                        
                        { bios.length> 0 ? bios.map((b, i) => <p key={i} className="text-justify">{b}</p>) : <p> loading profile...</p>}
                    </div>
                    <Arrows hp={handlePrevious} hn={handleNext} next={next} id={id}/>
                   
                    
                </div>
     
            </Layout>
        </>
    );
};
const Arrows=({hp,hn,next,id})=>(<div className="col-md-12 col-sm-12 my-2 clearfix " style={{height:50}} >
    <button 
        className="float-left cursor  btn mr-2" 
        onClick={()=>hp(id)}>
        <FontAwesomeIcon icon="caret-left" size="2x" color="rgb(231, 228, 228)"/>
    </button>
    <button 
        onClick={hn} 
        className="cursor btn float-right mb-3" >
        <FontAwesomeIcon icon="caret-right" size="2x" color="rgb(231, 228, 228)"/></button>

    <p className="text-center mt-3 cursor "><Link to={"/management"}>Return to main{" "}  <FontAwesomeIcon icon="undo"/></Link></p>
    <p className="text-muted font-weight-bold text-center"> {next} of {council.length - 1}</p>
</div>);
export default MoreMember;
 
