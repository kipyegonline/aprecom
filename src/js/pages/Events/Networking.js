import fun1 from "../../../assets/images/shots/fun1.jpg"
import fun2 from "../../../assets/images/shots/fun2.jpg"
import fun3 from "../../../assets/images/shots/fun3.jpg"
import fun4 from "../../../assets/images/shots/fun4.jpg"
import fun5 from "../../../assets/images/shots/fun5.jpg"
import fun6 from "../../../assets/images/shots/fun6.jpg"
import fun7 from "../../../assets/images/shots/fun7.jpg"
import fun8 from "../../../assets/images/shots/fun8.jpg"
export const pics=[
    {img:fun1,
    id:1,
    loaded:true,
caption:"APReCoM peer networking"},
{img:fun2,
    id:2,
    loaded:false,
caption:"APReCoM MOU"},
{img:fun3,
    id:3,
    loaded:false,
caption:"APReCoM  peernetworking"},
{img:fun4,
    id:4,
    loaded:false,
caption:"APReCoM peer networking"},
{img:fun5,
    id:5,
    loaded:false,
caption:"APReCoM  peernetworking"},
{img:fun6,
    id:6,
    loaded:false,
caption:"APReCoM  peernetworking"},
{img:fun7,
    id:7,
    loaded:false,
caption:"APReCoM networking"},
{img:fun8,
id:8,
loaded:false,
caption:"APReCoM peer networking"}];


export default function Networking(){

    return(
        <div className="row">
            <div className="col-md-12">
                {pics.map((pic,i)=><Net key={i} {...pic}/>)}
            </div>
            
        </div>
    )
}
const Net=({caption,img})=>(<ul className="brand-list network pics">
<li className="float-left">
    <figure>
    <img  className="img-thumbnail img-fluid network-img" title={caption} src={img} alt={"Network"}/>

        <figcaption className="text-center text-white bg-aprecom4">{caption}</figcaption>
    </figure>
    </li>
    
</ul>)