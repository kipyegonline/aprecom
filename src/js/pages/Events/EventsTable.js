import {useState,useEffect, useRef} from "react"
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome"

import Modal from "../Ui/Modal"
import { fetchData } from "../utils/helper"

function EventsTable(){
    const [title,setTitle]=useState("")
    const [events,setEvents] =useState([])
    const [eventId, setEventId] = useState(0)
    const [loadSpinner,setLoadSpinner]=useState(false)
    
    const fetchEvents = () => {
        
        let url = "./php/events/addEvents.php?fetchEvents=true";
        fetchData(url,setEvents)
        
    }
    useEffect(() => {
        fetchEvents();
        let timer=setTimeout(()=>setLoadSpinner(true),2000);
        return () => clearTimeout(timer);
    }, [])
    const handleRegister=(course,index)=>{
       
        $("#eventsModal").modal({ show: true });
        
        setTitle(course)
        setEventId(index)
    }
   
    const Main =(
        <div className="row my-3">
            <div className="col-md-12 col-sm-12">
        <h5 className="text-header font-weight-bold text-shadow text-center p-2">APReCoM Upcoming Events 2020</h5>
        </div>
        <div className="table-responsive col-md-12 col-sm-12">
                <Modal id="eventsModal" title={title}><RegisterUser eventId={eventId}/></Modal>
            {events.length>0 ? 
        <table className="table table-bordered table-hover table-striped">
            <thead>
            <tr><th>#</th><th>Course</th><th>Date</th><th>Location</th><th>Points</th><th>Fee</th><th>Register</th></tr>
            </thead>
            <tbody>
                            {events.filter(event=> new Date(event.eventdate) >= new Date())
                            .sort((a,b)=> new Date(a.eventdate)- new Date(b.eventdate))
                            .map((event, i) => <TableData key={event.id} index={i} f={() => handleRegister(event.coursename, event.id)} {...event} />)}
            </tbody>
        </table> : 
        <div>
        <p className="text-center p-1 text-danger">There are no events at the moment. Please sign up to receive event notifications</p>
                        <RegisterUser eventId={0}/>
               </div>
        }
        </div>
        </div>
    )
return loadSpinner ? Main : <p className="text-center"><FontAwesomeIcon icon="spinner" size="3x" color="green" pulse/></p>
}
    

const TableData=({eventdate,location,coursename,points,fee,f,index})=>(
    <tr><td>{index+1}.</td><td>{coursename}</td><td>{new Date(eventdate).toDateString()}</td><td>{location}</td><td>{points}</td><td>{fee}</td><td><button className="btn  text-white bg-aprecom3 btn-sm" onClick={f}>Register</button></td></tr>)

export default EventsTable

function RegisterUser({eventId}){
    const [name, setName]=useState("")
        const [email, setEmail]=useState("")
        const [phone, setPhone]=useState("")
        const [success,setSuccess]=useState("")
        const [error,setError]=useState(false);
    const [showing, setShowing] = useState(false)
    const [spinner,setSpinner]=useState(false)
   
    const btn=useRef(null)
        ;
        const form=useRef(null)
        const handleBlur=(e)=>{
           if(e.target.value.length===0){
               e.target.classList.add("red-border")
           }else{
            e.target.classList.remove("red-border") 
           }
           //validate name
           if(e.target.name==="userName"){
        if(e.target.value.indexOf(" ") === -1){
       
       setSuccess("Please add your second name")
      
      setShowing(true)
      setTimeout(()=>setShowing(false),3000)
               }
           }//name
//validate email
if(e.target.name==="email"){
      
    if(e.target.value.indexOf("@") === -1){
   setSuccess("Please enter a valid email address")
   
  setShowing(true)
  setTimeout(()=>setShowing(false),3000)
           }
       }
       //validate phone 
       if(e.target.name==="phone"){
        if(e.target.value.trim().length <10){
            setSuccess("Please enter a valid phone number")
      
      setShowing(true)
      setTimeout(()=>setShowing(false),3000)
               }
           }
        }
    const handleSubmit=e=>{
        
        
        e.preventDefault();

        if (name.length > 5 && email.length > 5 && phone.length > 9) {
            btn.current.disabled = true;
            setSpinner(true)
            const payload = { email:email, phone:phone, name:name, eventId:eventId }
            
           
            $.ajax({
                url: "./php/events/event.php?register=true",
                method:"POST",
                data:payload,
                dataType:"json"
            })
         .then(res=>{
            
             setSpinner(false)
             if (res.status == 200) {
                setSuccess(res.msg);
                setError(true);
                setShowing(true);
             } else {
                setSuccess(res.msg);
                setError(false);
                setShowing(true);
             }
            
            
             setTimeout(() => {
                 btn.current.disabled = false;
                form.current.reset()
                
                setEmail("");
                setName("");
                setPhone("");
                 $("#eventsModal").modal("hide");
                 setShowing(false)
            
            },10000)
         })
         .catch(err=>{
             console.log(err)
                setSuccess("Failed to send, try again later.")
                setSpinner(false)
                setError(false)
                
                btn.current.disabled = false;
                form.current.reset()
                setTimeout(()=>setShowing(false),5000)
             
            
            

            
         })
        }else{
            setSuccess("All fields are required.")
            setShowing(true)
            setTimeout(()=>setShowing(false),5000)
        }
       
    }
    return(
        <>
        <div className="row">
            <div className="col-md-8 offset-md-2 card box-shadow p-4">
                <form className="form" onSubmit={handleSubmit} ref={form}>
                    <p className="text-center font-weight-bold">Register for Aprecom Event</p>
                    <div className="form-group">
                        <label className="font-weight-bold">
                            <FontAwesomeIcon icon="user" size="lg" className="mr-2" color="brown"/>
                            Name:</label>
                    <input className="form-control my-2 p-1"
                     onChange={e=> setName(e.target.value.trim())}
                      placeholder="Enter name" 
                      name="userName"
                       onBlur={handleBlur}
                       type="text"/>
                    </div>
                    <div className="form-group">
                    <label className="font-weight-bold">
                    <FontAwesomeIcon icon="envelope" size="lg" className="mr-2" color="brown"/>
                    Email Address:</label>
                    <input className="form-control my-2 p-1"
                     onBlur={handleBlur}
                     name="email"
                     onChange={e=>setEmail(e.target.value.trim())} 
                      placeholder="Enter email address"
                       type="email"/>
                    </div>
                    <div className="form-group">
                    <label className="font-weight-bold">
                    <FontAwesomeIcon icon="phone-alt" size="lg" className="mr-2" color="brown"/>
                        Phone Number:</label>
                    <input className="form-control my-2 p-1" 
                     onBlur={handleBlur}
                     name="phone"
                    onChange={e=>setPhone(e.target.value.trim())}
                     placeholder="Enter phone number" 
                      type="telephone"/>
                        </div>
                        <div className="form-group" style={{ display: (showing) ? "block" : "none" }}>
                            <span className="float-right close" onClick={()=>setShowing(!showing)}>X</span>
                        <p  
                     className={ (error) ? "alert-success" : "text-danger"}>{success}</p>
                 {(spinner) ? <p className="text-center"><FontAwesomeIcon icon="spinner" pulse color="green" size="lg"/></p> :null}
                        </div>
                    
                    <input ref={btn} className="btn btn-block bg-aprecom1 my-2" type="submit" value="Register"/>
                    
                    
                </form>

            </div>

        </div>
        </>
    )
}