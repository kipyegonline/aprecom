import Layout from "../Ui/Layout"
import eventPic from "../../../assets/images/slides/workshop1.jpg"
import EventsTable from "./EventsTable"
import Networking from "./Networking"
function Events(){
    
return(<>
    <Layout>
        {/* 
<figure>
    <img src={eventPic} alt={"workshop and events"} className="img-fluid w-100 "/>
    <figcaption className="text-center text-lg bg-aprecom3 py-2">Aprecom workshop</figcaption>
</figure>*/}
<EventsTable/>
<p className="text-center text-lg bg-aprecom2 text-white">Gallery</p>
<Networking/>
</Layout>
</>)
}
export default Events;