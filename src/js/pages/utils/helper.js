
export function fetchData(url,setState){
    fetch(url)
        .then(res => res.json())
        .then(res => {
            
            setState(res)
        })
       .catch(err=>console.error(err))
}
export function sendData(url,data) {
    $.ajax({
    url,
    data,
    type:"POST"
    })
    .then(res => console.log(res))
    .catch(err=>console.log(err))
}

export function  getId(){
    if (typeof document !== "undefined") {
        if (window.localStorage) {
            return  localStorage.getItem("venividivici");
        }
    }
    
}

export function userTrackerId() {
    let metric = "mdcccxxiv";
    const token=JSON.parse(localStorage.getItem(metric));
    let userId = token.split("-");        
    userId = Number(userId[1]);
    return userId;
}