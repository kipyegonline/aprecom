import { useState } from "react";

export const categories = ["Select category", "Full ", "Associate", "Affiliate"];
export default function Looper() {
  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [category, setCategory] = useState("");
  const [datez, setDate] = useState("");
  const [address, setAddress] = useState("");
  const [profile, setProfile] = useState("");

  const {
  day, date, month, year
} = today;
  return (
    <>
      {name} {email}
      {category}
      {profile}
      <br />
      <p>
        {date} {day} {month} {year}
      </p>
      <form className="form" style={{ width: 500 }}>
        <InputSet weka={weka} f={setName} label={"undefined"} />
        <InputSet weka={weka} f={setEmail} label={"null"} />
        <InputSelect f={setCategory} v={category} options={categories} />
        <TextArea f={setProfile} v={profile} label={"NaN"} />
        <InputSet weka={weka} f={setName} label={"undefined"} />
        <InputSet weka={weka} f={setEmail} label={"null"} />
        <InputSelect f={setCategory} v={category} options={categories} />
        <TextArea f={setProfile} v={profile} label={"NaN"} />
        <InputSet weka={weka} f={setName} label={"undefined"} />
        <InputSet weka={weka} f={setEmail} label={"null"} />
        <InputSelect f={setCategory} v={category} options={categories} />
        <TextArea f={setProfile} v={profile} label={"NaN"} />
        <InputSet weka={weka} f={setName} label={"undefined"} />
        <InputSet weka={weka} f={setEmail} label={"null"} />
        <InputSelect f={setCategory} v={category} options={categories} />
        <TextArea f={setProfile} v={profile} label={"NaN"} />
        <InputSet weka={weka} f={setName} label={"undefined"} />
        <InputSet weka={weka} f={setEmail} label={"null"} />
        <InputSelect f={setCategory} v={category} options={categories} />
        <TextArea f={setProfile} v={profile} label={"NaN"} />
        <InputSet weka={weka} f={setName} label={"undefined"} />
        <InputSubmit {...toa} />
      </form>
    </>
  );
}

// text

const Input = ({
 f, type, classlist, placeholder, name, value
}) => {
  const handleInput = (e) => {
    //if (e.target.value.length === 0) return;
    f(e.target.name,e.target.value);
  };
  const handleBlur = e => {

    if (e.target.type === "url") {
     
      if (e.target.value.trim().length>0  && !e.target.value.includes("http")) {
        e.target.value="https://"+ e.target.value
        
        f(e.target.name,e.target.value)
      }
    }
    if(e.target.value.trim().length===0){
      e.target.classList.add("border-red")
    }else{
      e.target.classList.remove("border-red")
    }
  }
  return (
    <input
      type={type}
      name={name}
      className={classlist}
     onBlur={handleBlur}
      onChange={handleInput}
      value={value}
    />
  );
};
// select
const Select = ({ children, f, v,name }) => {
  const handleSelect = (e) => {
    if (e.target.value === "Select category") return;
    f(e.target.name,e.target.value);
  };
  return (
    <select className="form-control" onChange={handleSelect} name={name} value={v}>
      {children}
    </select>
  );

 };
const Option = ({ value }) => <option value={value}>{value}</option>;
// label
const Label = ({ label, children }) => (
  <div className="form-group">
    <label className="label">
     
      {label}
    </label>
    {children}
  </div>
);

// TEMPLATE
export const InputSet = ({ weka, f, label,value }) => (
  <Label className="label" label={label}>
    <Input {...weka} f={f} value={value} />
  </Label>
);
export const InputSubmit = ({ classlist, value, type,f }) => (
  <input type={type} name="submitBtn" onClick={f} className={classlist} value={value} />
);
export const InputSelect = ({
 options, f, v, label,name
}) => (
  <Label label={label}>
    <Select f={f} v={v} name={name}>
      {options.map((option, i) => (
        <Option key={i} value={option} />
      ))}
    </Select>
  </Label>
);

export const TextArea = ({ f, v, label,placeholder,name}) => {
  const handleTextArea = e => {
    
    f(e.target.name,e.target.value);
  };
  return (
    <Label label={label}>
      <textarea
        value={v}
        name={name}
        className="form-control"
        cols={50}
        rows={3}
        autoFocus={true}
        maxLength={1000}
        placeholder={placeholder}
        onChange={handleTextArea}
      />
    </Label>
  );
};


export const InputRadio=({f,name,v1,v2,classlist,label })=>{
  const handleRadio=e=>f(e.target.name,e.target.value);
  return(
    <Label label={label}>
      <input type="radio" name={name} value={v1} onChange={handleRadio} className={classlist}/>Yes
      <input type="radio" name={name} value={v2} onChange={handleRadio} className={classlist}/>No
    </Label>
  )
}
const weka = {
  placeholder: "Enter some text",
  type: "text",
  name: "name",
  classlist: "form-control p-1 my-2",
  label: "Enter name"
};
const toa = {
  type: "submit",
  classlist: "btn btn-primary btn-md",
  value: "submit form"
};
const months = [
  "January",
  "February",
  "March",
  "April",
  "May",
  "June",
  "July",
  "August",
  "September",
  "October",
  "November",
  "December"
];
const weekDays = [
  "Sunday",
  "Monday",
  "Tuesday",
  "Wednesday",
  "Thursday",
  "Friday",
  "Saturday"
];
const today = {
  date: weekDays[new Date().getDay()],
  day: new Date().getDate(),
  month: months[new Date().getMonth()],
  year: new Date().getFullYear(),
  minutes:  new Date().getMinutes(),
  seconds:  new Date().getSeconds()
};



