import {faEnvelope, faHome,faPhoneAlt,faBell, faSpinner, faAt,faClock,faEdit, faUser, faKey,faPeopleCarry,faBullhorn,faCalendarCheck,
    faCheck, faCheckDouble, faCheckCircle,faSignOutAlt,faUserFriends,faSignInAlt, faHandshake,
    faInfo, faAddressBook,faCalendarAlt,faUsers,faUndo, faTimes,faExclamationTriangle, faBriefcase, faCaretLeft,
  faCaretRight,faMobile,faMapMarkerAlt,faGlobe, faMapPin,faArrowAltCircleUp,
  faUmbrella,faUserCog,faEye, faBuilding} from "@fortawesome/free-solid-svg-icons"
    import {fab} from "@fortawesome/free-brands-svg-icons"
   
    export const icons=[fab,faEnvelope,faHome,faPhoneAlt,faEnvelope, faAt,faClock,faSpinner,
        faKey, faEdit,faPeopleCarry,faBullhorn,faCalendarCheck,faBell,faUser,
      faCheck, faCheckDouble, faCheckCircle,faUserFriends,faSignInAlt, faInfo,faBriefcase,
      faHandshake, faSignOutAlt,faAddressBook,faCalendarAlt,faUsers,faUndo,faTimes,faExclamationTriangle,faMobile,faMapMarkerAlt,faGlobe,
    faCaretRight,faCaretLeft, faUmbrella, faUserCog,faMapPin, faEye, faBuilding,faArrowAltCircleUp];