import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {useEffect} from "react"
import Footer from "./Footer";
import Nav from "./Nav";
import Header from "./Header";
import Contactus from "../Contacts/ContactUs";
import KeepMeAlive from "../keepMeAlive/keepmealive"
function Layout({ children }) {
  
  useEffect(()=>window.scrollTo({top:0,behavior:"smooth"}),[])

  window.onscroll=function(e){
    
    if(window.scrollY>640){
     
      $(".navigator").css("visibility","visible").addClass("ease-in");
    }else{
      $(".navigator").css("visibility","hidden");
      
    }
  }
  
const handleScroll=()=>window.scrollTo({top:0,behavior:"smooth"});
return(
    <>
    <section className='container-fluid'>
       
    
        <Header/>
        <Nav/>
        <KeepMeAlive/>
        
        {children}
        <Footer/>
        <SiteConstA/>
        <FontAwesomeIcon icon="arrow-alt-circle-up" color="red" onClick={handleScroll} className="navigator" 
         size={window.innerWidth >480 ? "3x" : "2x"}  />
        </section></>
)
}
export default Layout;

  
  const SiteConstA=()=>(<div className="col-md-12   red lighten-1 p-1 mt-0 w-100 ">
  <h5 className="text-center text-white  p-2">Copyright  &copy; 2012 - {new  Date().getFullYear()}  All Rights Reserved. </h5>
 </div>)