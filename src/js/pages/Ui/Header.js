import {useState, useEffect} from "react";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome"
import {socials,SocialMedia} from "./Nav"
import bg from "../../../assets/images/logo/aprecomHeader7.jpg"
import bg_md from "../../../assets/images/logo/aprecomHeader_md.jpg"
import bg_sm from "../../../assets/images/logo/aprecomHeader_sm.jpg"

 const Header=()=>{
    let img
    if (typeof window !== 'undefined') {
        
      
     
     if(window.innerWidth<480){
         img=bg_sm
     }else if(window.innerWidth<480){
         img=bg_md
     }else{
        img=bg
     }
    }
     const style={
    backgroundImage:`url(${img})`,
    backgroundSize:"contain",
    backgroundPosition:"top "}
     return(<header className="header my-0 mx-0 py-0" style={style}>
         <Hours/>
          <SocialMedia classlist={"social-nav social-menu ml-5 float-right"} iconSize={"2x"} {...socials}/>
    

</header>)}


const Hours=()=>{
   
  
    const dateMaker=()=>{
        const day=new Date().getDay();  
      
       if( day===6 ){
           
           return [false,"Opens on Monday at 8 am"];
        
       }else if( day===6 ){
        return [false,"Opens tomorrow at 8 am."];
       } else {
           const heure=new Date().getHours();
          if(heure >=0 && heure <8){
            return [true,"Opens at  8 am."];
          }else if(heure>=8 && heure <=16){
            
            return [true,"Closes at 5 pm."];
           }else if(heure >=17 && heure <=20){
            return [false,"Closed for the day."];
           }
           
           else{
            
            return [false,"Opens tomorrow at 8 am."];
           }
        
       }
    }
   const [open,setOpen]=useState(dateMaker()[0]);
   const [teller ,setTeller]=useState(dateMaker()[1]);
  
    return (<ul className="header-list mt-1 pt-2">
   
    <li><FontAwesomeIcon icon="clock"   className="mr-2" size="lg" spin/> {"   "}
{(open) ? <span className="badge badge-pill bg-success text-white">Open</span> :<span className="badge badge-pill bg-danger text-white">Closed</span>} <small className="text-muted">{teller}</small></li>
    <li><FontAwesomeIcon icon="phone-alt" color="brown" className="mr-2" size="lg" /> {"   "}<b>21962-00505 </b></li>
    <li><FontAwesomeIcon icon="envelope"  className="mr-2"size="lg" color="brown" /> {"   "} <b>jambo@aprecom.co.ke</b></li>
    
    </ul>)}
    export default Header;