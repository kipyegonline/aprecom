import {FontAwesomeIcon} from "@fortawesome/react-fontawesome"
import {SocialMedia,socials} from "./Nav"
import  {pics} from "../Events/Networking"
import prskLogo from "../../../assets/images/brands/prsklogo.svg"
import isa from "../../../assets/images/brands/isa.png"
import icco from "../../../assets/images/brands/icco.png"

function Footer(){
    return(
        <>
        <footer className=" container mb-0 aqua-gradient ">
            <div className="row mb-2">
            
                <div className="col-md-3 col-sm-12 mt-2"><Menu/></div>
                <div className="col-md-6 col-sm-12 border-left">
                    <h4 className="text-center text-white border-bottom">Pictorial slides</h4>
                   
                    <Slider pics={pics}/>
                </div>
                
                <div className="col-md-3  col-sm-12 border-left"><Connect/></div>
                
            </div>
            
            </footer></>
    )
}
export default Footer;

const Menu=()=>(
  <>
  <h4 className="text-center text-white border-bottom">Partners</h4>
<ul className="footer-link bg-light mb-3">
      <li className="my-2">
        <a href={"https://www.prsk.co.ke/"} target="_blank">
        <img className="img-fluid" src={prskLogo} width={200}/>
        </a>      
      </li>
      <hr className="divider"/>
      <li className="my-2">
        <a href={"https://isaafrica.education/"} target="_blank">
        <img  className="img-fluid"src={isa} width={200}/>
        </a>      
      </li>
      <hr className="divider"/>
      <li className="my-2">
        <a href={"https://iccopr.com/"} target="_blank">
        <img className="img-fluid" src={icco} width={200}/>
        </a>      
      </li>
      <hr className="divider"/>
     
     
    </ul>
    </>) 
    const pindrop="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3988.8065538287565!2d36.78278711397043!3d-1.2903585359926468!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x182f10a6fd6a6461%3A0xd0985718d69440c6!2sApex%20Porter%20Novelli!5e0!3m2!1sen!2ske!4v1566589140707!5m2!1sen!2ske" 
    const Connect=()=>(
        <>
        <h4 className="text-center border-bottom ">Connect with Us</h4>
        <ul className="list-group">
          <li  className="list-group-item " style={{color:"black"}}>
          <FontAwesomeIcon icon="building" className="mr-2" color="brown" size="lg" /><b>APRECOM SECRETARIAT</b><br/>
        (APEX PORTER NOVELLI)</li>
          <li  className="list-group-item " style={{color:"black"}}> <FontAwesomeIcon className="mr-2"  size="lg" icon="map-marker-alt" color="brown"/>
          Suite 405, 3rd Floor, Apple Wood Park || Wood Street, off Wood Avenue</li>
          <li className="list-group-item " style={{color:"black"}}> <FontAwesomeIcon  className="mr-2" size="lg" icon="map-pin" color="brown"/> 
         <a href={pindrop} target="_blank">Google Map Pin drop</a> </li>
          <li className="list-group-item " style={{color:"black"}}><FontAwesomeIcon  className="mr-2" size="lg" icon="envelope" color="brown"/>
           jambo@aprecom.co.ke</li>
          <li className="list-group-item " style={{color:"black"}}> <FontAwesomeIcon  className="mr-2" size="lg" icon="phone-alt" color="brown"/> 
          21962-00505 </li>
          <li className="list-group-item">{"  "}<SocialMedia {...socials} iconSize={"2x"} classlist={"social-nav social-footer"}/></li>
         
          
          
          
        </ul>
   
       
        </>
    )

    


  const Slider=({pics})=>{
    const startCarousel=()=> setTimeout(()=>$(".carousel").carousel({pause:false,interval:3000}),2000)
    return (
    <div id="carouselExampleControls"  onLoad={startCarousel} className="carousel slide " data-ride="carousel">
    <ol className="carousel-indicators">
      {pics.map((slide,i)=> <li  className='d-none d-md-block' data-target="carouselExampleControls" key={i} data-slide-to={1} className={(slide.loaded) ? "active" : ""}></li>)}
      
    </ol>
    <div className="carousel-inner">
    {pics.map((pic,i)=><Slide key={i} {...pic}/>)}
      </div>{/** */}
      </div>
  
      )}

      const Slide=({img,caption,loaded,imgMd,imgSm})=>(<div className={(loaded) ? "carousel-item active" : "carousel-item"}>
      <img className="d-block w-100" src={img}  alt={caption}/>
      <div className="carousel-caption d-none d-md-block">     
     </div> {/** carousels*/}
  <p className="orange accent-3 p-2 ">{caption}</p>
  
  </div>)