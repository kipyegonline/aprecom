import {FontAwesomeIcon} from "@fortawesome/react-fontawesome"
import Layout from "../Ui/Layout"
function NotFound({location}){
return(
    <>
    <Layout>
        <div className="mx-auto my-5 p-2 bg-light error-div">
       
        <h3 className="text-white mt-5 text-lg">
            Ooops! We could not find  the resource <span className="text-danger">{location.pathname}</span></h3>
            <p className="p-2"><a className="btn btn-primary btn-md" href="http://www.prhub.co.ke/aprecom/#/">Take me home</a></p>
        </div>
    
    </Layout>
    </>
)
}
export default NotFound;