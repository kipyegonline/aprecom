import {useState} from "react";
import {NavLink, Link} from "react-router-dom";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import { connect } from "react-redux";
import logoSm from "../../../../logo_sm.png";




const stylo={background:"rgba(242,154,59,.75)","marginTop":0};
export const socials={
    twitter:"https://twitter.com/APReCoMKenya",
    facebook:"https://www.facebook.com/AprecomKenya",
    youtube:"https://www.youtube.com/user/AprecomKenya",
    linkedin:null,
};

const Nav=({username})=>{
  
    const para = document.getElementById("aprecon-header");
    let timer;
   
 
     
    return (
        <>
            <header style={stylo}>
       
       
                <SocialMedia classlist={"social-nav social-navbar"} {...socials} iconSize={"2x"}/>
                <Menu socials={socials} username={username.username} />
            </header>
    
    
        
    
        </>
    );};

const mapStateToProps = state => ({ username: state.appReducer.username });

export default connect(mapStateToProps)(Nav);

export const SocialMedia=({classlist,twitter,facebook,youtube,linkedin,iconSize})=>(
    <ul className={classlist}>
        <li><a href={twitter} target="_blank">
            <FontAwesomeIcon icon={["fab","twitter"]} size={iconSize} color="#00acee "/></a></li>
        <li><a href={facebook} target="_blank">    
            <FontAwesomeIcon icon={["fab","facebook"]} size={iconSize} color="#39569c"/></a></li>

        <li><a href={linkedin} target="_blank">    
            <FontAwesomeIcon icon={["fab","linkedin"]} size={iconSize} color="#0072b1"/></a></li>

        <li><a href={youtube} target="_blank">    
            <FontAwesomeIcon icon={["fab","youtube"]} size={iconSize} color="#b2071d"/></a></li>

    </ul>);

const Menu=({username})=>{
    const [clicked,setClicked]=useState(true);
    const [fixNav,setNav]=useState(false);
    const fixedNav="navbar navbar-expand-lg  navbar-light orange lighten-1  fixed-nav";
    const normalNav="navbar navbar-expand-lg mt-1 navbar-light bg-light";
    const style={
        borderRadius:".5rem",
        borderBottom:"2px solid red"};
    const uuid = localStorage.getItem("venividivici");
    const handleLogout = () => {
        localStorage.removeItem("venividivici");
        location.href = "index.html";
    };
    
      
    const handleScroll=(e)=>{     
          
        const nav=document.querySelector("nav");
        let start=nav.getBoundingClientRect().height;
        let navOffset=start + nav.offsetTop;
        if(nav){          
    let past=window.scrollY;        
           
      
            if(past> navOffset){
                setNav(true);
                document.body.style.paddingTop=`${nav.offsetHeight}px`;
            }else{
                setNav(false);
                document.body.style.paddingTop=`${0}px`;
            }
        }
   
   
   
   
    };
   


    if(window.innerWidth>1030){
        addEventListener("scroll",handleScroll);
        
    }
   
   
   
    
    return (
        
        <nav className={(fixNav) ? fixedNav : normalNav} >  

           <span></span>
            <button className="navbar-toggler float-right mb-2" type="button" data-toggle="collapse"
                data-target="#Aprecom" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"
                onClick={()=>setClicked(prevState=>!prevState)}>
                {(clicked) ? <span className="navbar-toggler-icon "></span> : 
                    <FontAwesomeIcon icon="times" size="lg"  fixedWidth/>}
            </button>

            <div className="collapse navbar-collapse" id="Aprecom">
                <ul className="navbar-nav  ">
                    <li className=" nav-item ">
                        <NavLink className="nav-link "  activeStyle={style} to={"/"} onClick={()=>location.href="index.html"}>
                            <FontAwesomeIcon icon="home" size="lg" color="black"/>{" "}Home<span className="sr-only">(current)</span></NavLink >
                    </li>
                    <li className="nav-item">
                        <NavLink className="nav-link"  activeStyle={style} to={"/about-us"}>
                            <FontAwesomeIcon icon="umbrella" size="lg" color="black"/>{" "}About Us</NavLink>
                    </li>    
                    <li className="nav-item">
                        <NavLink className="nav-link" activeStyle={style}  to={"/management"}>
                            <FontAwesomeIcon icon="user-cog" size="lg" color="black"/>{" "} Management</NavLink>
                    </li> 
                    <li className="nav-item">
                        <NavLink className="nav-link"  activeStyle={style} to={"/members"}>
                            <FontAwesomeIcon icon="users" size="lg" color="black"/>{" "}Members</NavLink>
                    </li> 
      
      
                    <li className="nav-item">
                        <NavLink className="nav-link" activeStyle={style}  to={"/events"}>
                            <FontAwesomeIcon icon="calendar-alt" size="lg" color="black"/>{" "}Events & workshop</NavLink>
                    </li>   
      
                    <li className="nav-item">
                        <NavLink className="nav-link" activeStyle={style}  to={"/contact-us"}>
                            <FontAwesomeIcon icon="address-book" size="lg" color="black"/>{" "}Contact Us</NavLink>
                    </li>
                    {(!uuid) ?
                        <li className="nav-item">
                            <Link className="btn btn-sm bg-aprecom1 mt-1 ml-5" to={"/membership"}>
                                <FontAwesomeIcon icon="sign-in-alt" className="mr-2" size="lg" color="red"/>{" "}Join Aprecom</Link>
                        </li> :
                        <>
                            <li className="nav-item">
                                <Link className="btn btn-sm bg-aprecom3  ml-5" to={"/parte"}>
                                    <FontAwesomeIcon icon="user" size="lg" color="block"/>{" "}{username}</Link>
                            </li>
                            <li className="nav-item">
                                <Link className="btn btn-sm bg-aprecom1 ml-1" to={"/"} onClick={handleLogout}>
                                    <FontAwesomeIcon icon="sign-out-alt" size="lg" className="mr-2"  color="block"/>{" "}log out</Link>
                            </li>
                        </>}
       
      
        
                </ul>
    
            </div>
        </nav>

    );};


