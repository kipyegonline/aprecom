import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import Layout from "../Ui/Layout"
export const Error404 =()=>{
   
   
    return (
    <Layout>
    <div className="mx-auto  card my-5 p-2 bg-light error-div">
        
        
    <h3 className="text-white  text-lg mt-5">
Ooops! This resource  does not exist on my server  </h3>
        <p className="p-2"><a className="btn btn-primary btn-md" href="http://www.prhub.co.ke/aprecom/#/">Take me home</a></p>
    </div>
    </Layout>
)}


export const Error403 =()=>{
   
    
   return (
    <Layout>
    <div className="mx-auto  card my-5 p-2 bg-light error-div">
       
        
    <h3 className="text-white  text-lg mt-5">
   This resource is restricted </h3>
        <p className="p-2"><a className="btn btn-primary btn-md" href="http://www.prhub.co.ke/aprecom/#/">Take me home</a></p>
    </div>
    </Layout>
)}