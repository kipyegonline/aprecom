import {useState, useEffect} from "react";
import {Link} from "react-router-dom";
import Layout from "../Ui/Layout";
import { fetchData } from "../utils/helper";

import MembersTable from "./brands";
import {whyJoin }from "./datajoin";
import NewsItems from "./news";
import pic1 from "../../../assets/images/slides/aprecom1.jpg";
import  imgMd1 from "../../../assets/images/slides/aprecom1_md.jpg";
import  imgSm1 from "../../../assets/images/slides/aprecom1_sm.jpg";
import pic2 from "../../../assets/images/slides/aprecom2.jpg";
import imgMd2 from "../../../assets/images/slides/aprecom2_md.jpg";
import imgSm2 from "../../../assets/images/slides/aprecom2_sm.jpg";
import pic3 from "../../../assets/images/slides/aprecom3.jpg";
import  imgMd3 from "../../../assets/images/slides/aprecom3_md.jpg";
import  imgSm3 from "../../../assets/images/slides/aprecom3_sm.jpg";
import pic4 from "../../../assets/images/slides/aprecom4.jpg";
import  imgMd4 from "../../../assets/images/peer-networking4_md.png";
import  imgSm4 from "../../../assets/images/peer-networking4_sm.png";
import pic5 from "../../../assets/images/slides/aprecom5.jpg";
import  imgMd5 from "../../../assets/images/peer-networking5_md.png";
import  imgSm5 from "../../../assets/images/peer-networking5_sm.png";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

const slides=[{img:pic1,imgMd:imgMd1,imgSm:imgSm1,title:"networking",loaded:true},
    {img:pic2,imgMd:imgMd2,imgSm:imgSm2,title:"networking",loaded:false},
    {img:pic3,imgMd:imgMd3,imgSm:imgSm3,title:"networking", loaded:false},
    {img:pic4,imgMd:imgMd4,imgSm:imgSm4,title:"networking", loaded:false},
    {img:pic5,imgMd:imgMd5,imgSm:imgSm5,title:"networking", loaded:false},];

function Home() {
  
  
    
    return(
    
        <Layout>  
          
            <Carousel slides={slides} />
       
            <Def/>
        
        
            <div className="row">
                <div className="col-md-7">
                    <h5 className="text-shadow text-header text-center  border-bottom-1 ">WHY JOIN APRECOM?</h5>
                    <div className='row'>
                        <Workshops/>
                        <FastTrackProgram/>
                        {/*<NewsItems/>*/}
                    </div>
                </div>
                <div className="col-md-5 col-sm-12 box-shadow-white">
                    <NewsItems/>
                    <hr className="divider"/>
                    <UpComingEvents/>
          
                    <EmbedTweet/> 
                </div>
         
              
            </div>
       
       
            <div className="col-md-12 mt-3"><MembersTable/></div>
       
       
        
        </Layout>    );
}
export default Home;

//main slider
export const Carousel=({slides})=>{
    useEffect(function (){
        startCarousel();
    
    
    }, []);
    
    function startCarousel() {
        
        setTimeout(function () {
            
            $(".main-carousel").carousel({ pause: "hover", interval: 7000 })
        }, 3000)
     }
    
    return (
        <>
            <div id="carouselExampleControls"   className="carousel main-carousel slide " data-ride="carousel" data-interval="5000">
                <ol className="carousel-indicators">
                    {slides.map((slide,i)=> <li  className='d-none d-md-block' data-target="carouselExampleControls" key={i} data-slide-to={1} className={(slide.loaded) ? "active" : ""}></li>)}
    
                </ol>
                <div className="carousel-inner">
    
                    {slides.map((slide,i)=><Slider key={i} {...slide}/>)}
    
                </div>{/**Inner */}
                <a className="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                    <span className="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span className="sr-only">Previous</span>
                </a>
                <a className="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                    <span className="carousel-control-next-icon" aria-hidden="true"></span>
                    <span className="sr-only">Next</span>
                </a>
            </div>
        </>
    );};

//mini slider
const Slider=({img,title,loaded,imgMd,imgSm})=>(
    <div className={(loaded) ? "carousel-item active" : "carousel-item"}>
        <img className="d-block w-100" src={img}  alt={title}/>
        <div className="carousel-caption d-none d-md-block">
            <h5>...</h5>
            <p>...</p>
        </div>
    </div>);
//aprecom definition

const Def=()=><h6 className="p-2 my-2 alert alert-info">The ASSOCIATION OF PUBLIC RELATIONS AND COMMUNCATION MANAGEMENT FIRMS (APReCoM) 
is a Chapter of the Public Relations Society of Kenya (PRSK) and the Business Management Organisation for Kenyan consultancy firms engaged in the practice of public relations and Communications.</h6>;
const Workshops=()=>(
    <div className="col-md-12 col-sm-12 mb-2 whyJoin ">
    
        <ul className="list-group mt-2">
            {whyJoin.map((join,i)=> <WhyJoinIn key={i} index={i+1} {...join}/>)}
        </ul>



    
  
    </div>
);
//embed tweets component
const EmbedTweet=()=>(
    <>
   
        <a className="twitter-timeline   p-1 text-center" data-height="501" href="https://twitter.com/APReCoMKenya?ref_src=twsrc%5Etfw">Tweets by APReCoMKenya</a>

    </>);
//list of reason component
const WhyJoinIn=({msg, title,index,icon})=>(
    <><li className="join-list ">
        <FontAwesomeIcon icon={icon} size="2x" className="ml-5 mr-2" color="red" />{"  "}
        {title}</li>
    <p className="card-text">{msg}</p></>);




//upcoming events

const UpComingEvents=()=>{
    const [events,setEvents]=useState([]);
    const fetchEvents = () => {
        
        let url = "./php/events/addEvents.php?fetchEvents=true";
        fetchData(url,setEvents);
    };
    useEffect(()=>fetchEvents(),[]);

    return(
        <>
            <h5 className="text-center text-white p-1 light-green accent-4 ">Upcoming Events & workshops</h5>
            {events.length >0 ? 
                <ul className="list-group">
                    {events.filter(event=> new Date(event.eventdate) >= new Date())
                        .sort((a,b)=> new Date(a.eventdate)- new Date(b.eventdate))
                        .map(event=><li key={event.id} className="list-group-item" style={{color:"black"}}>{event.coursename} 
                            <Link  className="ml-3" to={"/events"}>Register</Link></li>)}
                </ul> : <p className="text-center"><FontAwesomeIcon icon="spinner" pulse color="green" size="3x"/></p>}
            <hr className="divider"/>
        </>
    );
};

const FastTrackProgram=()=>{

    $(".fast-track").animate(1000).slideDown(1000);
    return(
        <div className="row my-3 fast-track card " style={{display:window.innerWidth>768 ?"block" :"none"}}>
            <div className="col-md-12 col-sm-12">
                <h5 className="text-center orange darken-3 text-white p-2">APReCoM Fastrack Program</h5>
                <p>The APReCoM Fastrack Program is a deliberate action to bridge the gap between academic achievements and work place outcome expectations in the ever changing and dynamic nature of Public Relations influenced by global and local insights.</p>
                <h5 className="text-center" style={{borderBottom:"2px solid #ccc"}}><FontAwesomeIcon icon="user"  className="mr-2" size="lg"/>
 Who is it for?</h5>
                <p>The Program will identify and nurture fresh young under graduates and/or budding executives with less than three years experience in the field of Public Relations and/or a young graduate in a different academic field with a clear vision and passion to pursue a career in Public Relations or related field. </p>


            </div>

        </div>
    );};

/*<h5 className="text-center">Opportunity for Participants</h5>

<p>The program sets an opportunity for the target group to augment their academic achievements with on-the-job skills that will equip them with skill competencies that will help them meet the dynamic needs of the practice.</p> 
 <h5 className="text-center">Opportunity for Mentors</h5>

<p>The Program provides a life time opportunity for mentors to motivate and transfer professional skills they have developed over the years to upcoming generations of Public Relations leaders ready to make instant impact working in consultancy firms, in-house public relations departments across the economic divide.   </p>
*/