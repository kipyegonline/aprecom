import {useState, useEffect,useRef} from "react";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {fetchData} from "../utils/helper";


export default function NewsItems(){
    const [items,setItems]=useState([]);
   
    useEffect(function () {
        fetchData("./php/events/sendNews.php?getNewsItems=true", setItems);
        startCarousel();
     },[]);
   
    

    function startCarousel() {
        
        setTimeout(function () {
            
            $(".news-slide").carousel({ pause: "hover", interval: 6000 })
        }, 3000)
     }
    
    return(
        <>
       
            <div className="col-md-12 my-2  mb-2 card p-2">
                <h5 className="text-center text-white p-1  mb-0 light-green accent-4 ">APReCoM latest news.</h5>
                {items.length>0 ?
                    <div id="carouselExampleControls"   className="carousel news-slide slide " data-ride="carousel" data-interval="5000">
                        <div className="carousel-inner">
                            {items.map((item,i)=><NewsItem key={i} i={i} {...item}/>   )}
                        </div>
                    </div>    
                    : <p className="text-center"><FontAwesomeIcon icon="spinner" pulse color="green" size="3x"/></p>}   
            </div> 
       
        
        </>
    );
}
//news bar
const NewsItem=({mediahouse,onDate,headline,para,link,i})=>{
    const loaded="card-body carousel-item active";
    const loading="card-body carousel-item";
    return (
    <div  className={i===0 ? loaded : loading}>
              <h5 className=" p-1 mb-3 text-white red lighten-1 ">{headline}</h5> 
    <h6 className="card-subtitle  ">{mediahouse} 
    <small className="ml-3 text-muted">{new Date(onDate).toLocaleString()}</small></h6>
   
            <p>{para}<a href={link}  target={"_blank"}className="text-danger">{"  "} see more</a></p>
        </div>    
    );
};




 