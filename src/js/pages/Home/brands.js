import {useState, useEffect} from "react"
import {connect} from "react-redux"
import Papa from "papaparse";
import * as brands from "../Membership/logos"

const agencies=require("../../../assets/agency.csv")
function MembersTable({csvData}){
    

    return (
        <div className="row">
        <div className="box-shadow-white col-md-12 col-sm-12">
        <h5 className="text-center text-shadow text-lg p-1 mb-2" style={{borderBottom:"2px solid #ccc"}}>MEMBERS</h5>
        <ul className="brand-list">
   {csvData.map(data=><MakeLists key={data.id} {...data}/>)}
        </ul>
        </div>
        </div>
    )
}

const MakeLists=({pic,website,company})=>(<li className="">
    <a href={website} target="_blank"><img  className={"img-fluid brand-image"} src={pic} title={company} alt={company} /></a>
</li>);
const mapStateToProps=state=>({
    csvData:state.agencyReducer.agencies
});

export default connect(mapStateToProps)(MembersTable);

