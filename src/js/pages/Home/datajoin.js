export const whyJoin=[
    {
        title:"SYMBOL OF PROFESSIONALISM",
        icon:"briefcase",
        msg:"The association name will be synonymous with professionalism, quality and excellence in the industry."
    },
    {
        title:"INDUSTRY INTELLIGENCE",
        icon:"bell",
        msg:"The association will conduct quarterly industry surveys on opportunities and potential business leads."
    },
    {
        title:"PEER NETWORKING",
        icon:"users",
        msg:"Through the association, members will be able to interface regularly to share ideas, insights and solutions. The association will organize periodic roundtables where industry issues will be raised and ideas on how to move member agencies forward will be discussed."
    },
    {
        title:"NEW BUSINESS LEADS",
        icon:"handshake",
        msg:"The association will endeavor to ensure that all key new business pitches/projects are channeled through the association secretariat hence giving the members first opportunity. The association will also endeavor to be the first contact point for potential clients seeking to find an agency."
    },
    {
        title:"SOURCE FOR AGENCY BEST PRACTICES",
        icon:"bullhorn",
        msg:"The association will have a wide spectrum of agency management resources for various situations such as drafting of agency-client contract, working with a procurement client, pitching procedures, responding to government tenders etc. Potential clients will also be able to refer to the association to certify the best practices in engaging agencies."
    },
    
    
]