//login info
import React, { useState, useRef } from "react";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {v4} from "uuid";
import { Link, withRouter } from "react-router-dom";
import { connect } from "react-redux";
import axios from "axios";

import { getId } from "../utils/helper";

const UpdatePassword = ({ match }) => {
    const [newPassword, setNewPassword] = useState("");
    const [confirmPassword, setConfirmPassword] = useState("");
    const [successmsg, setSuccessmsg] = useState("");
    const [error, setError] = useState("");
    const [spinner,setSpinner]=useState(false);
    const ver=match.params.code;
    
    //198a8f11-4cc6-49c0-b11b-ebb1354024a4
    const handleSubmit=e=>{
        e.preventDefault();
      

        if (newPassword.trim().length===0) {
            setError("Enter new password");
            setTimeout(()=>setError(""),2000);
            
        } else if (confirmPassword.trim().length === 0) {
            setError("Confirm new password");
            setTimeout(()=>setError(""),2000);
        } else if (newPassword.trim() !== confirmPassword.trim()) {
            setError("Passwords do not match.Try again");
            setTimeout(()=>setError(""),2000);
        }
        else if (newPassword.trim().length > 6 && confirmPassword.trim().length > 6) {
            //everything is valid so far
            setSpinner(true);
            
            $.ajax({
                type: "POST",
                url:"./php/auth/resetpassword.php?updatePassword=true",
                data: {
                    newPassword: newPassword.trim(),
                    confirmPassword: confirmPassword.trim(),
                    verification: ver,
                    newVerification:v4()
                },
                dataType:"json"
            })
                .then(res=>{
                    //delay response by 2 seconds
                    setTimeout(()=>{
                        setSpinner(false);
                        if(res.status==201){
                            setError(res.msg);
                            setSuccessmsg("");
                            setTimeout(()=>setError(""),2000);
                        }
                        else{
                            setSuccessmsg(res.msg);
                            
                        }
                    },2000);
                        
                })
                .catch(err=>{
                    //delay response by 3 seconds
                    setTimeout(()=>{
                        setSpinner(false);
                        console.error(err);
                    },3000);
                });
            
            
        } else {
            setError("password must be 6 or more character including a number and uppercase letters");
            setTimeout(()=>setError(""),2000);
        }
    };
    return (
        <>
        
            <div className="row bg-transparent ">
                <div className="col-md-4 offset-md-4" >
                    <div className="card card-body">
                        <form className="form p-2" onSubmit={handleSubmit}>
                            <h5 className="text-center">APReCoM password reset centre</h5>
                            <hr className="divider"/>
                            <label><FontAwesomeIcon icon="key" className="mr-2" color="brown" size="lg" />
        Enter new password.</label>
                            <input type="password" className="form-control mb-4"
                                onChange={e=>setNewPassword(e.target.value)} value={newPassword} />
                            <label><FontAwesomeIcon icon="key" className="mr-2" color="brown" size="lg" />
                            Confirm password.</label>
                            <input type="password" className="form-control " 
                                onChange={e => setConfirmPassword(e.target.value)} 
                                value={confirmPassword}
                            />
                            
                            {(spinner) ? <p className="text-center mt-2" > <FontAwesomeIcon icon="spinner" size="lg" color="green" pulse/> </p> : null}
                            <p className="text-danger mt-3">{error}</p>
                            <p className="text-success mt-3">{successmsg}{/*Submit*/}</p>
                            <input  type="submit" className=" btn btn-info btn-sm mt-2 btn-block" value="Update password" />
                            <p className="text-center mt-4"><Link  to={"/login"}>Login</Link> | <Link to={"/"}>Aprecom website</Link></p>
                        </form>
                    </div>
                </div>
            </div>
        </>
    );
};
export default withRouter(UpdatePassword);



export const ResetLink = () => {
    const [email, setEmail] = useState("");
    const [successmsg, setSuccessmsg] = useState("");
    const [error,setError]=useState("");
    const [spinner,setSpinner]=useState(false);
    const handleSubmit=e=>{
        e.preventDefault();
        
        if (email.trim().length > 5 && email.includes("@")) {
            setSpinner(true);
            axios.get(`./php/auth/resetpassword.php?EmailresetPassword=true&email=${email}`)
            
                .then(res => {
                    //delay for 2 seconds
                    console.log(res)
                    setTimeout(()=>{

                        setSpinner(false);
                        const data = res.data;
                        
                        //handle errors
                        if (data.status == 201) {
                            setError(data.msg);
                            setTimeout(()=>setError(""),3000);
                        }
                   
                        else  if (data.status == 200) {
                            setSuccessmsg(data.msg);
                            setEmail("")
                        
                        }
                    },2000);  

                })
                .catch(err=>console.log(err));
        }else{
            setError("Enter an email address");
            setTimeout(()=>setError(""),2000);
        }
    };
    return (
        <div className="row">
            <div className="col-md-4 offset-md-4">
                <form className="form p-2" onSubmit={handleSubmit}>
                    <h5 className="text-center">APReCoM password reset</h5>
                    <label><FontAwesomeIcon icon="envelope" className="mr-2" color="brown" size="lg" />
        Enter your email address.</label>
                    <input type="email" className="form-control"
                        onChange={e=>setEmail(e.target.value)} value={email} />                       
                           
                    <p className="text-danger mt-2">{error}</p>
                    <p className="text-success mt-2">{successmsg}
                    </p>
                    {(spinner) ? <p className="text-center"><FontAwesomeIcon icon="spinner" size="lg" color="green" pulse/></p> : null}
                    {/*Submit*/}
                    <input  type="submit" className=" btn btn-info btn-md btn-block mt-2" value="submit"  />
                    <p className="text-center mt-3"><Link to={"/"}>Return to Aprecom website</Link></p>
                </form>

            </div>
        </div>
    );
};