import {useState,useEffect,useRef} from "react"
import { fetchData } from "../utils/helper"

export default function EventsPanel() {
    const [users, setUsers] = useState([])
    const getRegistered = () => {        
       /* $.ajax({
            type: "GET",
            url:"./php/events/event.php?getRegisteredUsers=true",
            dataType:'json'
        })
        .then(res=>console.log(res))
        .catch(err=>console.log(err))*/
       fetchData("./php/events/event.php?getRegisteredUsers=true", setUsers);
    
    }
    useEffect(() => getRegistered(), [])
    

    return (
        <>
            <div className="row">
                <div className="col-md-4 offset-md-1 "><AddEvents /></div>
                <div className="col- col-md-6"><AdminRegister users={users}/></div>
            </div>
        
        
        
        </>
    )
}

const AddEvents = () => {
    const [coursename, setCourseName] = useState("");
    const [date, setDate] = useState("");
    const [location, setLocation] = useState("");
    const [des, setDes] = useState("");
    const [fee, setFee] = useState("");
    const [points, setPoints] = useState("");
    const [success, setSuccess] = useState("")
    const [error,setError]=useState("")
    const form = useRef(null)
    const btn=useRef(null)
    const handleBlur = (e) => {
        if (e.target.value.trim().length === 0){
            e.target.classList.add("border-red");
        }else {
            e.target.classList.remove("border-red");
        }
        
  }
    const handleSubmit = (e) => {
        e.preventDefault();
        if (coursename.length >0 && date.length > 0 && location.length > 0 && des.length > 10 && fee.length > 0 && points.length > 0) {
            btn.current.disabled=true;
            $.ajax({
                url: "./php/events/addEvents.php?addEvents=true",
                data:{coursename,date,location,des,fee,points},
                method:"POST"
            })
                .then(res => {
                    
                    setSuccess(res)
                    setTimeout(() => {
                        form.current.reset();
                        btn.current.disabled = false;
                        setDate("");
                        setCourseName("");
                        setDes("");
                        setFee("");
                        setLocation("");
                        setPoints("");
                        setSuccess("")
                    },3000)
                    
                })
            .catch(error=>setError(error))
            
        } else {
            alert("All fields are required");
        }
    }
    return (
        <>
        <div className="card my-2">
            <form onSubmit={handleSubmit} className="form p-3" ref={form}>
                <p className="text-center p-2 alert alert-info ">Add upcoming events</p>
                <div className="form-group">
                        <label className="font-weight-bold">Course Title:</label>
                    <input className="form-control my-2 p-1"
                     onChange={e=> setCourseName(e.target.value.trim())}
                      placeholder="Enter title" 
                       onBlur={handleBlur}
                       type="text"/>
                    </div>
                    <div className="form-group">
                    <label className="font-weight-bold">Event date:</label>
                    <input className="form-control my-2 p-1"
                    id="remail" onBlur={handleBlur}
                     onChange={e=>setDate(e.target.value.trim())} 
                      placeholder=""
                       type="date"/>
                    </div>
                    <div className="form-group">
                        <label className="font-weight-bold">Location:</label>
                    <input className="form-control my-2 p-1"
                     onChange={e=> setLocation(e.target.value.trim())}
                      placeholder="Enter location" 
                       onBlur={handleBlur}
                       type="text"/>
                    </div>
                    
                <div className="form-group">
                        <label className="font-weight-bold">Fee:</label>
                    <input className="form-control my-2 p-1"
                     onChange={e=> setFee(e.target.value.trim())}
                      placeholder="Enter event fee" 
                       onBlur={handleBlur}
                       type="text"/>
                </div>
                <div className="form-group">
                        <label className="font-weight-bold">Points:</label>
                    <input className="form-control my-2 p-1"
                     onChange={e=> setPoints(e.target.value.trim())}
                      placeholder="Enter points" 
                       onBlur={handleBlur}
                       type="text"/>
                </div>
                <div className="form-group">
                        <label className="font-weight-bold">Description:</label>
                    <textarea className="form-control my-2 p-1"
                     onChange={e=> setDes(e.target.value.trim())}
                      placeholder="Describe the course" 
                      id="rname" onBlur={handleBlur}
                       ></textarea>
                </div>
                <div>
                    <p className="text-success p-1">{success}</p>
                    <p className="text-danger p-1">{error}</p>
                </div>
                <input type="submit" ref={btn} className="btn btn-primary btn-block" value="Add Event"/>
            </form>
        </div>
        
        </>
    )
}

function AdminRegister({users}){
    const [selected, setSelected] = useState([])
    const [event, setEvent] = useState([])
    const [placeholder, setPlaceholder] = useState("Select Event to view");
    let url = "./php/events/event.php?getEvents=true";
   const newUsers=users.slice();
    
    useEffect(() => fetchData(url, setEvent), []);

    const handleSelect = e => {
        if (e.target.value === "select") return;
        const chosen = newUsers.filter(user => user.event_id == e.target.value)
        if (chosen.length > 0) {
            setSelected(chosen)  
        }else{
            setPlaceholder("0 results found");
            setSelected([])
            setTimeout(setPlaceholder("Select Event to view"), 4000)
            

        }
         
       
    }
    
    return (
        <div  className="row">
            <div className="col-md-12">
                <h5 class="text-center my-2 alert alert-primary">Events List</h5>
            <select onChange={handleSelect} className="form-control w-50 my-3">
                    <option value="select">Select Event</option>
                    {event.map(event => <option key={event.id} value={event.id}>{event.coursename}</option>)}

                </select>
                <p className="text-center">{selected.length}</p>
                {selected.length > 0 ?
                    <table className="table table-bordered table-striped mt-2">
                        <thead>
                            <tr><th>#</th><th>Name</th><th>Email</th><th>Phone</th></tr>
                        </thead>
                        <tbody>
                            {selected.map((select,i) => <tr key={select.id}><td>{i + 1}</td><td>{select.name}</td><td>{select.email}</td><td>{select.phone}</td></tr>)}
                        </tbody>
                    </table> : <p>{placeholder}</p>}
            </div>
            
                
           
        </div>
    )

}