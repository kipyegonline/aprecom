import { useState, useEffect, useRef } from "react"

export default function NewsAdmin() {
    const [onDate, setDate] = useState("")
    const [mediaHouse, setMediaHouse] = useState("")
    const [headline, setHeadline] = useState("")
    const [para,setPara]=useState("")
    const [link, setLink] = useState("")
    const [error, setError] = useState("")
    const [success, setSuccess] = useState("")
    const form = useRef(null)
    const btn=useRef(null)

    const handleBlur = e => {
        if (e.target.value.length === 0)
        { e.target.classList.add("border-red") } else {
            e.target.classList.remove("border-red")
        }
    }
    const handleSubmit=e=>{
        e.preventDefault();
        if (onDate.length !== 0 && mediaHouse.length !== 0 && headline.length !== 0 && para.length !== 0 && link.length !== 0) {
            btn.current.disabled = true;
            $.ajax({
                url: "./php/events/news.php?newsItems=true",
                type:"POST",
                data:{onDate,mediaHouse,headline,para,link}
            })
                .then(res => {
                    if (res === "0") {
                    setError("Cannot add news try again later.")
                    } else {
                        setSuccess('News item added successfuly');
                    }
                    setTimeout(() => {
                        setError("")
                        setSuccess("")
                        form.current.reset();
                        setPara("")
                        btn.current.disabled=false
                    },3000)
                })
            .catch(err=>console.log(err))//setError(err))
            
        } else {
            setError("There are a missing fields");
            setTimeout(()=>setError(""),3000)
        }
    }
    return (
        <>
            <div className="row">
                <div className="col-md-12">
                    
                    <form className="form card p-3" ref={form} onSubmit={handleSubmit}>
                        <p className="text-center p-2 font-weight-bold bg-aprecom2">Add news items</p>
                        <div className="form-group">
                    <label className="font-weight-bold">1. Headline:</label>
                    <input className="form-control my-2 p-1"
                               
                                onBlur={handleBlur}
                     onChange={e=>setHeadline(e.target.value.trim())} 
                      placeholder=""
                       type="text"/>
                    </div>
                        
                <div className="form-group">
                        <label className="font-weight-bold">2. Date:</label>
                    <input className="form-control my-2 p-1"
                     onChange={e=> setDate(e.target.value.trim())}
                      placeholder="Enter date" 
                       onBlur={handleBlur}
                       type="date"/>
                    </div>
                    
                    <div className="form-group">
                        <label className="font-weight-bold">3. Media House:</label>
                    <input className="form-control my-2 p-1"
                     onChange={e=> setMediaHouse(e.target.value.trim())}
                      placeholder="Enter media house" 
                       onBlur={handleBlur}
                       type="text"/>
                    </div>
                    
                <div className="form-group">
                        <label className="font-weight-bold">Link:</label>
                    <input className="form-control my-2 p-1"
                     onChange={e=> setLink(e.target.value.trim())}
                      placeholder="Enter link" 
                       onBlur={handleBlur}
                       type="text"/>
                </div>
               
                <div className="form-group">
                        <label className="font-weight-bold">Short paragraph:</label>
                    <textarea className="form-control my-2 p-1"
                     onChange={e=> setPara(e.target.value)}
                                placeholder="Brief news" 
                                value={para}
                      onBlur={handleBlur}
                       ></textarea>
                </div>
                        <div>
                        <p className="text-success">{success}</p>
                        <p className="text-danger">{error}</p>
                       </div>
                    
            
                <input type="submit" ref={btn} className="btn btn-primary btn-block" value="Add News"/>

                    </form>

                </div>
            </div>
            
        
        </>
    )
}