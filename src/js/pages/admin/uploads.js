import Papa from "papaparse"
import { useState } from "react"
import Layout from "../Ui/Layout"
function AppUploads() {
    const [successMsg, setSuccess] = useState("");
    const [stored,setStored]=useState(false)
    const[file,setFile]=useState("")
    let url;
   
        const sendData = (dataArr,url) => {
            dataArr.forEach(data => {
              
                $.ajax({
                    type: "POST",
                    url,
                    data,
                    dataType:"json"
                })
                    .then(res => {
                    console.log(res)
                    if (res.status == 200) {
                        setSuccess(res.msg)
                        setStored(true)
                    } else {
                        setSuccess(res.msg)
                    
                    }
                    setTimeout(()=>{
                        setSuccess("")
                        setFile("")
                    })
                })
                .catch(err=>console.error(err))
            })
           
           
          }
    
    
    const handleFile = e => {
        if(url==undefined){
            alert("Select what you're uploading")
        } else {
            
            setFile(e.target.files[0].name);
            processNew(e.target.files[0], sendData, url)
            setTimeout(() => {
                
                setFile("")
            }, 2000)
        }
        
    }
    const handleSelect = e =>{
        let selected = e.target.value;
        if (selected === "agencies") {
            url="./php/agencies/agencies.php?addAgencies=true"
        }
        else if (selected === "services") {
            url="./php/services/services.php?addAreas=true"
        }else if(selected==="select"){
            return false;
        }
}

    const processNew=(file,callback,url)=>{
        let count=0,data;
       Papa.parse(file, {
           worker: false, // Don't bog down the main thread if its a big file
           download:true,
           header:true,
           dynamicTyping:true,    
           complete: function(results ) {               
               data=results.data
               
             data = data.slice(0, data.length - 1)  
             console.log(data)
             callback(data,url)       
               
               
      
           }//complete
       })//parse
      }
    
    return(
        <>
            <Layout>
            <div className="row my-3 p-3">
                <div className="col-md-4 offset-md-4 bg-light">
                        <div className="form-group">
                            <label>Select type of file</label>
                    <select onChange={handleSelect} className="form-control w-50">
                        <option value="select">Choose..</option>
                        <option value="agencies">Agencies</option>
                        <option value="services">Services</option>
                        </select>
                    </div>
                    <div className="form-group">
                    <label>Click to upload</label>
                            <input type="file" className="file form-file" onChange={handleFile}
                                value={file.name}
                    />
                    </div>

                    <p className={(stored) ? "text-success mt-2": " text-danger mt-2"}>{successMsg}</p>

                </div>
                
                </div>
                </Layout>
        </>
    )
}
export default AppUploads