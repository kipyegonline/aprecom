//login
//login info
import React, { useState, useRef,useEffect } from "react";
import {v4} from "uuid";
import { Link, withRouter,Redirect } from "react-router-dom";
import { connect } from "react-redux";
import axios from "axios";
import{ FontAwesomeIcon} from "@fortawesome/react-fontawesome";

import { getId } from "../utils/helper";
import * as formActions from "../../redux/appReducer/actions.js";
import *  as agent from "../../redux/agencies/actions";
import * as server from "../../redux/services/actions";

function Login({history, addSelectedServices, getAppData, getDirectors, getClients, getEmployees, getUsername}) {
    const [email,setEmail]=useState("");
    const [password, setPassword] = useState("");
    const [loginErr, setLoginErr] = useState("");
    
    const form = useRef(null);
    
    
    const fetchServices = (url, f) => {
        
        axios.get(url)
            .then(res => f(res.data))
            .catch(err => console.log(err.message));
       
    };
    const fetchUserData=()=>{
        
        
        const url1 = `./php/services/getServices.php?getServices=true&uuid=${getId()}`;
        const url2 = `./php/applications/app.php?getAppData=true&uuid=${getId()}`;
        const url3 = `./php/applications/fetchProfiles.php?getDirectors=true&uuid=${getId()}`;
        const url4=`./php/applications/fetchProfiles.php?getclientServices=true&uuid=${getId()}`;
        const url5 = `./php/applications/fetchProfiles.php?getEmployees=true&uuid=${getId()}`;
        const url6 = `./php/applications/fetchProfiles.php?getUsername=true&uuid=${getId()}`;
        
        const reqs = [
            fetchServices(url1, addSelectedServices),
            fetchServices(url2,getAppData),
            fetchServices(url3, getDirectors),
            fetchServices(url4, getClients),
            fetchServices(url5,getEmployees),
            fetchServices(url6, getUsername)];
        Promise.all(reqs);
        
        history.push("/join-aprecom-form");
         
    };

    
    const handleLogin = (e) => {
   
        e.preventDefault();
        if(email.length===0 && password.length===0){
            $("#email-login").addClass("border-red");
            $("#password-login").addClass("border-red");
            setLoginErr("Enter email/password to log in");
            setTimeout(()=>setLoginErr(""),4000);
        }else if (email.length ===0 ) {
            $("#email-login").addClass("border-red");
        } else 
        if (password.length === 0  ) {
            $("#password-login").addClass("border-red");
            setLoginErr("Enter password");
            setTimeout(()=>setLoginErr(""),4000);
        } else if(password.length <6) {
            $("#password-login").addClass("border-red");
            setLoginErr("Enter password with 6 characters with a atleast an uppercase letter and a number");
            setTimeout(()=>setLoginErr(""),4000);
        }
        else if (email.length >5  && password.length > 6) {
          
            const data={email,password};
            $.ajax({
                type:"POST",
                url: "./php/auth/login.php?handleLogin=true",
                data,
                dataType:"json"
              
            })
                .then(res => {
              
                    if (res.status === 200) {
                        localStorage.setItem("venividivici", res.token);
                    
                        // location.href = "index.html#/join-aprecom-form";
                        fetchUserData();
                    
            
                    
                    }
                    if (res.status == 201) {
                        setLoginErr(res.text);
                        setTimeout(()=>setLoginErr(""),4000);
                    }
              
              
              
              
                })
              
              
                .catch(err=>console.log("log err",err));
        }
      
  
  
    };
    const uuid = getId();
     
    const loginCode= (
        <div className="container">
            <div className='row login-container'>

                <div className='col-md-5 col-sm-12 offset-md-4 card p-4'>
                    <p className='text-center text-bold alert alert-primary'>APRECOM applicant login</p>
                    <form className='form p-4' action='#' id='login-form' ref={form} onSubmit={handleLogin}>
                        <div className='form-group'>
                            <label>
                                <FontAwesomeIcon icon="envelope" className="mr-2" color="brown" size="lg" />
    Email:</label>
                            <input type='text' 
                                className='form-control' 
                                placeholder='Enter email address' id='email-login'
                                onChange={(e)=>setEmail(e.target.value.trim())}
                            />

                        </div>
                        <div className='form-group'>
                            <label><FontAwesomeIcon icon="key" className="mr-2" color="brown" size="lg" />
    Password:</label>
                            <input type='password' className='form-control' placeholder='Enter password'
                                onChange={(e)=>setPassword(e.target.value.trim())}              id='password-login' />
                        </div>
                        <input type='submit' className='btn btn-primary btn-block mb-3' value='Login' />
                        <p className='text-danger mt-1'>{loginErr}</p>
                        <p><Link className="text-center mt-3 p-1 mr-2" to={"/sign-up-"+ "full"}>Create account</Link> |
                            <Link className="ml-2" to={"/password-reset-link"}>Forgot password</Link></p>
                        <p className="text-center"><Link to={"/"}>Return to Aprecom website</Link></p>
          
                    </form>

                </div>
            </div>
        </div>
    );
     
    return uuid ? <Redirect to={"/"} /> : loginCode;
}



const mapDispatchToProps=(dispatch)=>({
   
    
    addSelectedServices:data=>dispatch(server.ourServices(data)),
    getAppData: data => dispatch(formActions.fetchAppData(data)),
    getDirectors: data => dispatch(formActions.fetchDirectors(data)),
    getClients: data => dispatch(formActions.fetchClients(data)),
    getEmployees: data => dispatch(formActions.fetchEmployees(data)),
    getUsername: data => dispatch(formActions.setUserName(data)),    

});

export default withRouter(connect(null,mapDispatchToProps)(Login));







   


//sign up
export const SignUp = ({match}) => {
    const [name, setName] = useState("");    
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const [confirmPassword, setConfirmPassword] = useState("");
    const [success, setSuccess] = useState("");
    const [successful, setSuccessful] = useState(false);
    const [emailExist,setEmailExists]=useState(false);
    const[emailXmsg,setXmsg]=useState("");
    const [fail,setFail]=useState("");
    const [spinner,setSpinner]=useState(false);
    const form = useRef(null);
    const btn=useRef(null);

    const handleBlur = e => {
        //verify email
        if (e.target.type === "email") {
            if (e.target.value.trim().length === 0) {
                e.target.classList.add("border-red");
            } else if (!e.target.value.includes("@")) {
                e.target.classList.add("border-red");
                setFail("Enter a proper email address");
                setTimeout(()=>setFail(""),3000);
            }
            else {
                e.target.classList.remove("border-red");
                
                fetch(`./php/auth/signup.php?authEmail=true&email=${e.target.value.trim()}`)
                    .then(res => res.json())
                    .then(res => {
                      
                        if (res.status === 201) {
                            setXmsg(res.text);
                            setEmailExists(res.exist);
                        }
                    })
                    .catch(err=>{
                        setXmsg("Error verifying email.Check your internet connection");
                        setTimeout(()=>setXmsg(""),4000);
                        console.log(err.message);});
            }
            
            
        }
      

        //verify username
        if (e.target.name ==="username") {
            if (e.target.value.trim().length > 0 && e.target.value.trim().length <= 5) {
                setFail("Your username should have more than 5 letters");
                setTimeout(()=>setFail(""),2000);
                e.target.classList.add("border-red");
            } else {
                e.target.classList.remove("border-red");
            }
        }
        
    };
    const handleClose = () => setSuccessful(false);
    
    const handleSubmit=(e) => {
        e.preventDefault();
        if (name.trim().length ===0 && emailExist === false && email.trim().length === 0 && password.trim().length === 0 && confirmPassword.trim().length === 0) 
        {
            setFail("All fields are  required.");
            setSuccess("");
        }
        else   if (name.trim().length <=5) {
            setFail("Name is required.");
            setTimeout(()=>setFail(""),2000);
        }else
        if (email.trim().length === 0) {
            setFail("Email is required.");
            setTimeout(()=>setFail(""),2000);
        }else if (password.trim().length === 0) {
            setFail("Password is required.");
            setTimeout(()=>setFail(""),2000);
        }
        else if (confirmPassword.trim().length === 0) {
            setFail("Password confirmation is required.");
            setTimeout(()=>setFail(""),2000);
        }

        else if (password.trim().length < 6 && confirmPassword.trim().length < 6) {
            setFail("Passwords must be 6 characters long with atleast 1 uppercase letter and a number.");
            setTimeout(()=>setFail(""),2000);
        } else
        if (password !== confirmPassword) {
            $(".pwords").css("border","1px solid red");
            setFail("Passwords do not match.");
            setTimeout(()=>setFail(""),2000);
            return false;
        } else {
            if (name.trim().length >=5 && emailExist !== true && email.trim().length !== 0 && password.trim().length !== 0 && confirmPassword.trim().length !== 0) {
                btn.current.disabled = true;
                setSuccess("Success");
                $(".form-control").removeClass("border-red");
                setSpinner(true);
                const data = { name, email, password, confirmPassword,verified:v4(),category:match.params.category };
                $.ajax({
                    url: "./php/auth/signup.php?createUser=true",
                    type: "POST",
                    data,
                    dataType:"json"
                })
                    .then(res => {
                        //delay by 2000
                        setTimeout(()=>{
                            setSpinner(false);
                            if (res.status == 201) {
                                setFail(res.text);
                                btn.current.disabled=false;
                                setTimeout(() => setFail(""), 3000);
                            }else
                            if (res.status == 200) {
                                
                                setSuccess(res.msg);
                                setSuccessful(true);
                                setTimeout(() => {
                                    setName("");                            
                                    setEmail("");
                                    setPassword("");
                                    setConfirmPassword("");
                               
                               
                                    form.current.reset();
                                    btn.current.disabled=false;
                                
                                },3000);
                            }

                        },2000);
                       
                        
                       
                        
                    })
                    .catch(err=>{
                        setTimeout(()=>{
                            setFail(err.message);
                            setSpinner(false);
                            btn.current.disabled=false;
                            console.error(err);
                        },2000);
                    });//catch block
                    
                
            } else {
                $(".form-control").css("border", "1px solid red");
                setFail("All fields are mandatory");
                setTimeout(()=>setFail(""),2000);
                return false;
            }
        }
        
        
        

    };
    return (
        <div className="row">
            <div className="col-md-3"></div>       
        
            <div className="card card-body col-md-6 mt-3 p-3">
                <p className='text-center text-bold p-2 my-2 alert alert-primary'>APRECOM applicants account.</p>
                <p className="my-2">Sign up for a personal account that will enable your apply for your firm</p>
                <form className="form" ref={form} onSubmit={handleSubmit}>
                    <div className="form-group">
                        <label><FontAwesomeIcon icon="user" className="mr-2" color="brown" size="lg" />
                            Username</label>
                        <input type="text" name="username"
                            onBlur={handleBlur}
                            className="form-control"
                            onChange={(e)=>setName(e.target.value)} placeholder="Enter name"/>
                    </div>                    

                    <div className="form-group">
                        <label> <FontAwesomeIcon icon="envelope" className="mr-2" color="brown" size="lg" />
                        Email address</label>
                        <input type="email" className="form-control"
                            onBlur={handleBlur}
                            onChange={(e) => setEmail(e.target.value)}
                            placeholder="Enter your email address." />
                        <small className="mt-2 text-danger">{emailXmsg}</small>
                    </div>

                    <div className="form-group">
                        <label><FontAwesomeIcon icon="key" className="mr-2" rotate={90} color="brown" size="lg" />
                            Password</label>
                        <input type="password" className="form-control pwords"
                            onChange={(e)=>setPassword(e.target.value)} placeholder="Enter password"/>
                    </div>
                    <div className="form-group">
                        <label><FontAwesomeIcon icon="key" className="mr-2" color="brown" size="lg" />
                            Confirm password.</label>
                        <input type="password" name="passwordconfirm"
                            onBlur={handleBlur}
                            className="form-control pwords"
                            onChange={(e)=>setConfirmPassword(e.target.value)} placeholder="Confirm password"/>
                    </div>
                    <div>
                        <p className="text-danger my-1 ">{fail}</p>
                        <p  style={{display:(successful) ? "block": "none"}}
                            className=" alert alert-success   p-1 my-1 ">{success}
                            <span className="text-danger float-right" onClick={handleClose}>X</span></p>
                        
                    </div>
                    {(spinner) ?  <p className="text-center my-2"><FontAwesomeIcon icon="spinner" size="lg" color="green" pulse/></p> :null}

                    <input type="submit" ref={btn} className="btn btn-primary btn-block mt-2" value="Sign Up" />
                    <p className="mt-2 text-center">
                        <Link  className="mr-2" to={"/login"}>Log in </Link>|<Link to={"/"}>Return to Aprecom website</Link>
                    </p>
                    

                </form>
            </div>
        </div>
    );
};

 

export const AccountActivation = ({ match }) => {
    const [activated, setActivated] = useState(false);
    const [msg,Setmsg]=useState("");
    const vvid = match.params.link;
    
    const verifyUser =(vvid)=>{
        $.get(`./php/auth/signup.php?activateAccount=true&vvid=${vvid}&newId=${v4()}`)
        // .then(res => res.json())
            .then(data => {
                
                data=JSON.parse(data);
                if (data.status ===200) {
                    setActivated(true);
                    localStorage.removeItem("venividivici");
                    Setmsg("Success! Your account is active. Click here to");
                } else {
                    setActivated(true);
                    Setmsg(data.msg);
                }
                
            })
            .catch(err=>{
                setTimeout(()=>{
                    console.log(err);
                    Setmsg("The server is unreachable. please check your network and try again");
                    setActivated(true);
                },3000);
            
            });
    };
    useEffect(() => {
        
        setTimeout(verifyUser(vvid),3000);
    },[]);
    
    const success = (<h3 className="text-info text-center">{msg}
        <button style={{display:(activated ? "block":" none")}}>
            <Link to="/login">Login</Link></button> | <Link to={"/"}>Return to Aprecom website</Link></h3>);
    return(
        <div className="row">
            <div className="col-md-6 offset-md  card bg-light">
                {(activated) ? success :
                    <FontAwesomeIcon icon="spinner" className="mx-auto" color="green" pulse size="4x" />}
                
            </div>
        </div>
    );
};

