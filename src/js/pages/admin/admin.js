import { useState, useEffect, Children } from "react"
import {hashRouter as Router,Route,Switch,NavLink, Redirect} from "react-router-dom"
import axios from "axios"
import EventsPanel from "./adminEvents"
import NewsAdmin from "./adminNews";
import Layout from "../Ui/Layout"
import Applications from "./applications";
import {getId} from "../utils/helper";
export default function Admin() {
  let uuid = getId();
    if(uuid){
        uuid=uuid.split("-");
        uuid=uuid.slice(0,1).join(" ");
    }

    const admin= (
      <>
       <Layout>
          <Route component={AdminMenu} />
          <Route exact path="/admin" component={Areas}/>
        <Route path="/admin/news" exact component={NewsAdmin} />
          <Route path="/admin/events" component={EventsPanel} />
          <Route path="/admin/view-events" component={EventsPanel} />
      <Route path="/admin/applications" component={Applications}/>
        </Layout>
        </>
    )
    return (uuid) ? admin : <Redirect to={"/"}/>
}

const AdminMenu = ({ children, match }) => {
  const selectedStyle={
    color: "red",
    border:"1px solid red"
  }
  console.log('matches',match)
  return(
    <>
      
    <div className="row">
      <div className="col-md-12">      
      <ul className=" admin-nav bg-primary mt-3">
                <li className="nav-item">
                   <NavLink to={"/admin"} style={selectedStyle} className="nav-link">Home </NavLink></li>
                <li className="nav-item">
                  <NavLink to={"/admin/events"} activeStyle={selectedStyle} className="nav-link">Events</NavLink></li>
               
              <li className="nav-item">
                <NavLink to={"/admin/news"} activeStyle={selectedStyle} className="nav-link">News</NavLink></li>
                <li className="nav-item">
              <NavLink to={"/admin/applications"}
                activeStyle={selectedStyle} className="nav-link">Applications</NavLink></li>
      </ul>
  
      </div>
      
    </div>
    </>
  )
}



export function Areas(){
    const [input,setInput]=useState("");
  const [areas, setAreas] = useState([])
  useEffect(() => fetchServices("./php/services/services.php?getAreas=true"), []);
  
  const fetchServices=(url)=>{
    axios
       .get(url)
           .then(res =>setAreas(res.data))
           .catch(err => console.log(err.message))
   
   }//inner func
    const handleInput=()=>{
      if (input.trim().length === 0) return;
      const data={areas:input.trim()}
      $.ajax({
        url: "./php/services/services.php?addAreas=true",
        data,
        method:"post"
      })
      .then(res=>console.log(res))
        .catch(err => console.error(err))
      
      setTimeout(()=>setInput(""),100)
    }
    return(
      <>
      <div className="row mx-auto my-3 p-4">
        <div className="bg-light col-md-4 ">
        
      <div className="form-group">
      <p className="p-2 text-white bg-primary">Areas of Specialisation</p>
      <input type="text"  className="form-control my-2" 
       onChange={e=>setInput(e.target.value)} 
       placeholder="Enter text"
       value={input}/>
              <button className="btn btn-info" onClick={handleInput} >Enter</button>
              </div>

              <hr className="divider mb-2" />
              
            </div>  
            <div className="bg-light col-md-6">
              <p>Areas added so far so good</p>
              <ul className="list-service">
                {areas.map((area, i) =>
                  <li key={area.id} className="list-group-service">{i +1}. {area.area}</li>)}
              </ul>
            </div>
       

      </div>
          

      </>
    )
  }




