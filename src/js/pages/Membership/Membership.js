
import {Link} from "react-router-dom"
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome"
import Layout from "../Ui/Layout"



function Membership() {
  
    
  return (
    <>
      <Layout>
        <div className="row mb-4">
          <MembershipInfo />
       
        </div>
      </Layout></>
  );
}

function MembershipInfo(){
  const category={
    full:"Full",
    assoc:"Associate",
    aff:"Affiliate"
  }
  const uuid=localStorage.getItem("venividivici");
   
    return(
        <>
        <div className='col-md-12'>
            <h3 className='text-uppercase text-header text-center text-shadow my-2'>Membership</h3>
    <p className='text-lg mb-1 '><span className="font-weight-bold">(a.){"  "}</span>
            Membership of the Association shall consist of those Firms whose managers are fully paid up members of the Public Relations Society of Kenya. However, the Association may admit other categories of membership for the furtherance of its objectives. </p>
     <p className='text-lg mb-1'><span className="font-weight-bold">(b.){"  "}</span>A current, valid certificate of membership signed by the Chairman of the association for all members will be in force at all times.</p>
    <p className='text-lg mb-1'><span className="font-weight-bold">(c.){"  "}</span>The following will be the categories of membership and joining procedure.</p>
        
        </div>
        <div className="col-md-4">
        <div className="card">
        <div className="card-header text-center">
        1. FULL MEMBER
        
  </div>
  
  <ul className="list-group list-group-flush">
    <li className="list-group-item">1.Be registered as a business under the Laws of Kenya</li>
    <li className="list-group-item">2. Provide proof that 50% of the business revenue is generated 
    from Public Relations & Communication Management activities</li>
    <li className="list-group-item">3. Provide proof that the business has operated for a minimum of 3 years</li>
    <li className="list-group-item">4. Provide proof that the business has a minimum of five (5) employees working directly on public relations & communication management activities 
</li>
              <li className="list-group-item">
                { !uuid ? <Link className="btn btn-sm bg-aprecom4 text-white btn-block mt-1" to={"/sign-up-" + category.full}><FontAwesomeIcon icon="sign-in-alt" className="mr-2" size="lg" color="red"/>Apply</Link> :<Link className="btn btn-sm bg-aprecom4 text-white btn-block mt-1" to={"/join-aprecom-form"}>My application</Link>}</li>
    
  </ul>
</div>

        </div>{/**col 1 */}
        <div className="col-md-4">
        <div className="card">
        <div className="card-header text-center">
        2. ASSOCIATE MEMBER
        
  </div>
  
  <ul className="list-group list-group-flush">
    <li className="list-group-item">1.Be registered as a business under the Laws of Kenya</li>
    <li className="list-group-item">2.The business does not meet the criteria for full member but has potential to become one within three (3) years  </li>
    
    <li className="list-group-item alert alert-danger">3. An associate member who does not meet the criteria for full membership within the stipulated period will be expelled from membership </li>
              <li className="list-group-item">
                { !uuid ? <Link className="btn btn-sm bg-aprecom4 text-white btn-block  mt-1" to={"/sign-up-" + category.assoc}><FontAwesomeIcon icon="sign-in-alt" className="mr-2" size="lg" color="red"/>Apply</Link> :
                <Link className="btn btn-sm bg-aprecom4 text-white btn-block mt-1" to={"/join-aprecom-form"}>My application</Link>}
                  
                </li>
  </ul>
  </div>
</div>{/**col 2 */}
<div className="col-md-4">
        <div className="card">
        <div className="card-header text-center">
        3. AFFILIATE MEMBER
       
  </div>
  
  <ul className="list-group list-group-flush">
    <li className="list-group-item">1.If it is an organisation, be legally registered with evidence of such registration</li>
    <li className="list-group-item">2.Demonstrate capacity to benefit the Association and its members </li>
    
    <li className="list-group-item alert alert-danger">3.Not pay annual subscription fees </li>
              <li className="list-group-item">
                {!uuid ? <Link className="btn btn-sm bg-aprecom4 text-white btn-block mt-1   " to={"/sign-up-" + category.aff}><FontAwesomeIcon icon="sign-in-alt" className="mr-2" size="lg" color="red"/>Apply</Link>
                  : <Link className="btn btn-sm bg-aprecom4 text-white btn-block mt-1" to={"/join-aprecom-form"}>My application</Link>}</li>
    
  </ul>
  </div>
</div>{/**col 3 */}


        </>
    )
}



export default Membership;