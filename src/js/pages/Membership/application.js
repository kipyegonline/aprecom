import { useState } from "react";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import { Redirect } from "react-router-dom";
import {connect} from "react-redux";
import { v4 } from "uuid";
import {getId} from "../utils/helper";
import  PartII  from "./App2";

import Layout from "../Ui/Layout";
import {InputSet, InputSubmit, TextArea, InputSelect,categories, InputRadio } from "../utils/utils";




class ApplicationForm extends React.Component{
    constructor(props){
        super(props);
        this.state={
            dyears:[],
            errores:[],
            ownership:"",
            category:"",
            year:"",
            company:"",
            email:"",
            phone:"",
            website:"",
            address:"",
            yearsOp:"",
            regNumber:"" ,                         
            telephone:"" ,             
            posta:"" ,
            membershipContact :"",
            showing:false,
            successMsg: "",
            errorMsg:"",
            partB:false,
            spinner:false
        };
        this.handleInput=this.handleInput.bind(this);
        this.handleSubmit=this.handleSubmit.bind(this);
        this.getYears=this.getYears.bind(this);
        this.handleSelect=this.handleSelect.bind(this);
        this.form=React.createRef();
        
    }
    componentDidMount(){
        this.getYears();
    }
    UNSAFE_componentWillReceiveProps(nextProps) {
        const { appData } = nextProps;
    
    
        this.setState({
            partB: appData.edited === "0" ? true : false,
            category:appData.category
        });
    }
    handleInput(target,value){
       
        this.setState({[target]:value});
        

    }
    getYears(){
        const years=[];
        for(let i=2000; i<= new Date().getFullYear();i++){
            let y={};
            y.id=i+1;
            y.year=i;
            years.push(y);
        }
        this.setState({dyears:years});
    }
    handleSelect(e){
        if(e.target.value==="select Year")return;
        
        this.setState({year:e.target.value});

    }
    handleSubmit(e) {
        e.preventDefault();
      
        //alert("Cannot submit now, website under development");
        const { ownership, category, year, company, email, phone, website, address, yearsOp, regNumber,
            telephone,
            posta,
            membershipContact } = this.state;

        const data = {
            ownership, category, year, company, email, phone, website, address, yearsOp, regNumber,
            telephone,
            posta,
            membershipContact, verified: getId()
        };
        
        let err = [];
 
  
        const objectEntries = Object.entries(data);
      
        objectEntries.forEach(entry => {
            if (entry[1].length <= 0) {
                err.push(entry[0]);
            }
         
        });
        
        //check for empty fields
        if (err.length > 0) {
            this.setState({ errores: err });
            //put red borders
            const fiels = [...document.querySelectorAll(".form-control")];
            fiels.filter(item => item.value.trim().length <= 0)
                .map(item => item.classList.add("border-red"));
          
            return false;
        }
        
        //iniate an ajax request by checking for userID
        if (getId()) {
            this.setState({ spinner: true });
            $.ajax({
                method: "post",
                url: "./php/applications/app.php?handleApplication=true",
                data
       
            })
                .then(res => {
         
                    res = JSON.parse(res);
                    if (res.status == 200) {
                        this.setState({ showing: true, successMsg: res.msg });
            
                        setTimeout(() => {
                            this.form.current.reset();
                            this.setState({
                                ownership: "",
                  
                                year: "",
                                company: "",
                                email: "",
                                phone: "",
                                website: "",
                                address: "",
                                yearsOp: "",
                                regNumber: "",
                                telephone: "",
                                posta: "",
                                membershipContact: "",
                                successMsg: "",
                                errores: [],
                                showing: false,
                                partB: true,
                                spinner:false
                            });
             
                            err = [];
                        }, 3000);
                    }
          
                    if (res.status == 201) {
                        this.setState({ showing: true, errorMsg: res.erro, spinner:false });
                    }
        
                })
                .catch(error => console.error(error.message));

     
         
        
            console.log("eer is", err, data);

        }
    }//end AJAX
    render() {
        const { category, year, dyears, errores, showing, successMsg,spinner,errorMsg, partB } = this.state;
        const userId = getId();
        
    

        const App1 = (
            <Layout>
             
                <form className="form my-3 p-2 application-form card" ref={this.form} onSubmit={this.handleSubmit}>
                    <div className="row">
                        <div className="col-md-12 col-sm-12 ">
                            <h3 className="text-center my-2 p-1">Application Form ({category} )</h3>
                            <p className="alert alert-primary text-center">Part 1 of 2</p>
                        </div>
                        <div className="offset-md-2"></div>
                
                        <div className="col-md-4 col-sm-12 ">
                    
                            {/*name*/}
                            <InputSet weka={dname} f={this.handleInput} label={dname.label} />
                   
           
                            {/*Phone*/}
                            <InputSet weka={dphone} f={this.handleInput} label={dphone.label} />
                            {/*Phone*/}
                            <InputSet weka={dtelephone} f={this.handleInput} label={dtelephone.label} />
                            {/*Email*/}
                            <InputSet weka={demail} f={this.handleInput} label={demail.label} />
                            {/*webiste*/}
                            <InputSet weka={dposta} f={this.handleInput} label={dposta.label} />
                            {/*physical address*/}
                            <InputSet weka={daddress} f={this.handleInput} label={daddress.label} />

                        </div>
                        <div className="col-md-4 col-sm-12 ">
                            {/*ownership*/}
                            <InputRadio {...downership} f={this.handleInput} />
                            <InputSet weka={dregNumber} f={this.handleInput} label={dregNumber.label} />
                            <InputSet weka={dyearsOp} f={this.handleInput} label={dyearsOp.label} />
                            <div className="form-group">
                                <label>10. Year founded</label>
                                <select className="form-control" onChange={this.handleSelect} value={year}>
                                    <option value="select Year">select Year</option>
                                    {dyears.reverse().map(year => <option key={year.id} value={year.year}>{year.year}</option>)}
                                </select>
                            </div>
                            {/*webiste*/}
                            <InputSet weka={dwebsite} f={this.handleInput} label={dwebsite.label} />
                            {/*Membership*/}
                            <InputSet weka={dcontact} f={this.handleInput} label={dcontact.label} />
                    
                            {(errores.length > 0) ? <p className="text-danger" style={{ display: (showing) ? "none" : "false" }}>
                                {errores.length} fields are  missing.({errores.join(",")})</p> : null}
                            {(spinner) ? <p className="text-center"><FontAwesomeIcon icon="spinner" size="lg" pulse /></p> : null}
                            <p className="text-danger">{errorMsg}</p>
              
                
                
                
                            {/*submit*/}
                            <InputSubmit {...submit} />
                        </div>
                
                
                    </div>
                </form>
            </Layout>
        );
        //main decision poin
        if (userId) {
            return partB ? <Redirect to="/parte"/> : App1;
        } else {
            return  <Redirect to={"/login"}
            />;
        }
    
    }
}

const mapStateToProps=state=>({appData:state.appReducer.appData});
export default connect(mapStateToProps)(ApplicationForm);

const dname= {
    placeholder: "Enter company name",
    type: "text",
    name: "company",
    classlist: "form-control p-1 my-2",
    label: "1. Enter Company name"
};
const demail= {
    placeholder: "Enter email address",
    type: "email",
    name: "email",
    classlist: "form-control p-1 my-2",
    label: "4. Email address"
};
const dregNumber= {
    placeholder: "Enter company registration number",
    type: "text",
    name: "regNumber",
    classlist: "form-control p-1 my-2",
    label: "8.Company Registration Number"
};
const downership={
    placeholder: "",
    type: "radio",
    v1:"Yes",
    v2:"No",
    name: "ownership",
    classlist: "radio mx-2",
    label: "7.  Is the company owned locally ?"
};
const dphone= {
    placeholder: "Enter mobile",
    type: "telephone",
    name: "phone",
    classlist: "form-control p-1 my-2",
    label: "2. mobile number"
};
const dtelephone= {
    placeholder: "Enter Telephone",
    type: "telephone",
    name: "telephone",
    classlist: "form-control p-1 my-2",
    label: "3. Telephone"
};
const daddress= {
    placeholder: "Enter your  physical address",
    type: "text",
    name: "address",
    classlist: "form-control p-1 my-2",
    label: "6. Physical address:"
};
const dposta= {
    placeholder: "Enter your  postal  address and code",
    type: "text",
    name: "posta",
    classlist: "form-control p-1 my-2",
    label: "5. Enter your  postal address and code:"
};
const dabout= {
    placeholder: " brief description of your company",
    type: "text",
    name: "about",
    classlist: "form-control p-1 my-2",
    label: "8. Brief description of the organisation (Max:350 words):"
};
const dcontact= {
    placeholder: " Membership contact",
    type: "text",
    name: "membershipContact",
    classlist: "form-control p-1 my-2",
    label: "12. APReCoM membership contact."
};
const dwebsite= {
    placeholder: "www.aprecom.co.ke",
    type: "text",
    name: "website",
    classlist: "form-control p-1 my-2",
    label: "11. website address (optional)"
};
const dyearsOp={
    placeholder: "Years in operation",
    type: "number",
    name: "yearsOp",
    classlist: "form-control p-1 my-2",
    label: "9. years in operation"
};
  
  
const submit= {
    type: "submit",
    classlist: "btn bg-aprecom1 btn-block mt-3 text-white",
    value: "Proceed"
};

 
//localStorage.setItem('areas',JSON.stringify([]))