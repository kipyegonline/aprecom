import { InputSet, InputSubmit, TextArea } from "../utils/utils";
import { Link, Redirect } from "react-router-dom";
import { connect } from "react-redux";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import * as agent from "../../redux/agencies/actions";
import Layout from "../Ui/Layout";
import {getId} from "../utils/helper";

class SocialMediaProfile extends React.Component{
    constructor(props) {
        super(props);
        this.state={
            twitter: "",
            facebook: "",
            youtube: "",
            linkedin: "",
            googlemap: "",
            id:JSON.parse(localStorage.getItem("uuid")),
            success:"",
            error: "",
            spinner:false,
            company:this.props.match.params.company
        };
        this.handleInput=this.handleInput.bind(this);
        this.handleSubmit=this.handleSubmit.bind(this);
        this.form= React.createRef();
        
    }

    componentDidMount() {
        const { company } = this.state;
        //get existing socials for me
        this.timer=setTimeout(()=>this.props.getSocials(company),1000);
        
    }
    UNSAFE_componentWillReceiveProps(nextProps) {
       
        const comp = nextProps.match.params.company;
        const { twitter, facebook, youtube, linkedin, googlemap, status } = nextProps.socials;
        if (status == 200) {
            this.setState({
                company: comp,
                twitter,
                facebook,
                youtube,
                linkedin,
                googlemap,
            });
        }
        
    }
    handleInput(target,value){
  
        this.setState({[target]:value});
    }
    handleSubmit(e){
        e.preventDefault();
        const { twitter, facebook, youtube, linkedin, googlemap, id,company } = this.state;
        
        const data = {twitter, facebook, youtube, linkedin, googlemap, id };
       
        if (twitter.trim().length !== 0 || facebook.trim().length !== 0 || youtube.trim().length !== 0 || linkedin.trim().length !== 0 || googlemap.trim().length !== 0) {
            this.setState({spinner:true})
            $.ajax({
                type:"POST",
                data:data,
                url:"./php/applications/edit.php?insertSocials=true",
                dataType:"json"
            })
                .then(res => {
                    this.setState({spinner:false})
                    if (res.status == 200) {
                        this.setState({ success: res.msg });
                        setTimeout(()=>this.props.history.push(`/${company}-firm`),2000);
                    } else {
                        this.setState({ error: res.msg });
                        setTimeout(()=>this.setState({error:""}),2000);
                    }
               
                })
                .catch(err=>console.log("errr",err));
        } else {
            alert("Please enter one or two social media platforms");
        }
       
    }
    render() {
        const { error, success, company,twitter,facebook,linkedin,googlemap,youtube,spinner } = this.state;
       
        
        return (<>
            <Layout>
                <div className="row">
                    <div className="col-md-6 offset-md-3 bg-secondary my-2">
                        <form  className="mx-auto p-3 form bg-light" onSubmit={this.handleSubmit} ref={this.form}>
                            <h3 className="p-2 mt-3 alert alert-info">Edit {company} social media profiles</h3>
                            {/*Twitter*/}
                            <InputSet weka={dtwitter} f={this.handleInput} label={dtwitter.label} value={twitter}/>
                            {/*Facebook*/}
                            <InputSet weka={dfacebook} f={this.handleInput} label={dfacebook.label} value={facebook}/>
                            {/*Linkedin*/}
                
                            <InputSet weka={dlinkedin} f={this.handleInput}  label={dlinkedin.label}
                                value={linkedin}
                            />
                            <InputSet weka={dyoutube} f={this.handleInput} label={dyoutube.label}
                                value={youtube}
                            />
                            {/*Google map pin drop*/}
                            <InputSet weka={dgooglemap} f={this.handleInput} label={dgooglemap.label}
                                value={googlemap} />
                            {(spinner) ? <p className="text-center"><FontAwesomeIcon icon="spinner" pulse color="green" size="2x" /></p> : null}
                            <p className="text-danger my-1">{error}</p>
                            <p className="text-success my-1">{success}</p>
                            {/*submit*/}
                            <InputSubmit {...submit}/> 
                            <p className="text-center mt-2"> <Link 
                                to={`/${company}-firm`}>Return to profile</Link></p> 
                            

                        </form>
        
                    </div>
                </div>
            
            </Layout>
        </>
        );
    }
}


const mapStateToProps = state => ({
    socials:state.agencyReducer.socialMedia
});
const mapDispatchToProps = (dispatch) => ({
    getSocials:payload=>dispatch(agent.addSocialMedia(payload))
});
export default connect(mapStateToProps,mapDispatchToProps)(SocialMediaProfile);

const dtwitter= {
    placeholder: "https://www.twitter.com/aprecom",
    type: "url",
    name: "twitter",
    classlist: "form-control p-1 my-2",
    label: "1. Enter Twitter url link (optional):"
};
const dfacebook= {
    placeholder: "www.facebook.com/aprecomKenya",
    type: "url",
    name: "facebook",
    classlist: "form-control p-1 my-2",
    label: "2. Facebook page link (optional):"
};
const dyoutube= {
    placeholder: "Enter YouTubeChannel link",
    type: "url",
    name: "youtube",
    classlist: "form-control p-1 my-2",
    label: "4. YouTube channel (optional):"
};
const dgooglemap= {
    placeholder: "Paste Google pin here",
    type: "url",
    name: "googlemap",
    classlist: "form-control p-1 my-2",
    label: "5. Google map pin drop (optional):"
};
const submit= {
    type: "submit",
    classlist: "btn btn-primary btn-block",
    value: "submit profile"
};
const dlinkedin= {
    placeholder: "Enter LinkedIn link",
    type: "url",
    name: "linkedin",
    classlist: "form-control p-1 my-2",
    label: "3. Enter LinkedIn Link (optional):"
};