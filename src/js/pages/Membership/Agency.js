import {useState,useEffect} from "react";
import {Link} from "react-router-dom";
import { connect } from "react-redux";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {SocialMedia,socials} from "../Ui/Nav";
import Layout from "../Ui/Layout";

import {userTrackerId} from "../utils/helper";

function Agencies({agency}){

  
    return(
        <>
  
            <CurrentMembers agencies={agency}/>  

            <hr className="divider"/>

        </>
    );
}
//{agency.socials.status===200 ? agency.socials :socials}
export default Agencies;
export const CurrentMembers = ({ agencies, f }) => {
    const handleProfileClicks = (id) => {
                
        const userId = userTrackerId();
        sendMetrics(id, userId);
    };
        
    const sendMetrics = (id,userId) => {
        fetch(`./php/metrics/profileMetrics.php?clickMetrics=true&profileMetrics&profileId=${Number(id)}&userId=${userId}`)
            .then(res=>res.text())
            .then(data=>console.log(data))
            .catch(err=>console.log(err));
    };
    return (
        <>
            { agencies.length>0 ?
                <div className=" col-md-12 col-sm-12 p-2  mx-auto table-responsive">
                    <h6 className='text-header text-center alert alert-info'>CURRENT MEMBERS DIRECTORY</h6>
                    <table className="table table-bordered table-striped table-hover table-hover table-responsive">
                        <thead className='thead-light'>
                            <tr><th scope="col">#</th><th>Company</th><th scope="col">logo</th><th>Social media</th><th>Website</th><th>Company profile</th></tr>
                        </thead>
                        <tbody>
                            {agencies.map((agency, i) => <Member key={agency.id} index={i} {...agency}
                                socials={agency.socials.status == 200 ? agency.socials : socials}
                                f={handleProfileClicks} />)}
                        </tbody>
                    </table>
                </div>
                :
                <p className="text-center m-5"><FontAwesomeIcon icon="spinner" pulse size="3x" color="green"/></p>}
        </>
    );
};
const Member = ({ id, company, website, email, phone, type, pic, index,socials,f }) => {
   
    return (
        <tr><td>{index + 1}</td>
            <td>{company}</td>
            <td><a href={website} target="_blank"><img src={pic} title={company} className=" w-100"/></a></td>
            <td><SocialMedia classlist={"social-table social-nav"} {...socials} iconSize="2x" /></td><td><a  href={website} target="_blank">{website}</a></td><td>
                <Link to={company + "-firm"} className="text-white btn btn-block green accent-4 btn-sm"
                    onClick={() => f(id)}>profile </Link></td>
        </tr>);};







function MembersPageA({data}){
    

    return(
        <Layout>
            <CurrentMembers agencies={data}/>
        </Layout>
    );
}
const mapStateToProps=state=>({
    data:state.agencyReducer.agencies
});
    
export const MembersPage=connect(mapStateToProps)(MembersPageA);