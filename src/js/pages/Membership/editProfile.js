import React, { Component, useState, useRef } from "react";
import { connect } from "react-redux";
import {Link} from "react-router-dom";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import *  as agent from "../../redux/agencies/actions";
import { InputSet, InputSubmit, TextArea } from "../utils/utils";

import Layout from "../Ui/Layout";

class EditProfile extends Component{
    constructor(props){
        super(props);
        this.state={
            id: 0,
            company:"",
            phone:"",
            email:"",
            website:"",
            about:"",
            address:"",
            editor:"",
            editornum: "",
            error: "",
            success:"",
            spinner:false
        };
        this.handleInput=this.handleInput.bind(this);
        this.handleSubmit=this.handleSubmit.bind(this);
        this.form=React.createRef();
    
    
    }
    componentDidMount() {
        const id = JSON.parse(localStorage.getItem("uuid"));
        
        setTimeout(()=>this.props.editProfile(id),1000);
    
    }
    handleInput(target,value){
    //if (value.trim().length === 0) return;
        this.setState({[target]:value});
    }
    UNSAFE_componentWillReceiveProps(nextProps){
   
        const { data } = nextProps;
        
       
   
        if (data !== undefined) {
            
            let website = data.website;
            this.setState({
  
                phone:data.phone,
                email:data.email,
                website,
                about:data.companyProfile,
                address:data.address,
                editor:data.telephone,
                editornum: data.posta,
                company:data.company

            });
        }
    
    }

    handleSubmit(e){
        e.preventDefault();
        this.setState({ spinner: true });
        const {phone,
            email,
            website,
            about,      
            address,
            editor,
            editornum,
            company}=this.state;


    
    
    
        const data = {
            id:JSON.parse(localStorage.getItem("uuid")),
            phone,
            email,
            website,
            about,
            address,
            editor,
            editornum,
  
        };

    

        $.ajax({
            url:"./php/applications/edit.php?handleEdit=true",
            type:"POST",
            data,
            dataType:"json"
        })
  
            .then(res => {
                this.setState({ spinner: false });
                if (res.status === 200) {
                    this.setState({success:res.msg});
                    setTimeout(()=>this.props.history.push(`/${company}-firm`),2000);
       
                } else {
                    this.setState({error:res.msg});
                    setTimeout(() => {
                        this.form.current.reset();
                        this.setState({
                            error: "",
                            success: ""
                        });
       
        
                    }, 3000);
                }
      
      
            })
            .catch(err=>console.error(err));

    }
  
    render(){
        const { error,
            success,
            phone,
            email,
            website,
            about,      
            address,
            editor,
            editornum,company,spinner} = this.state;
    
        return (
            <Layout>
                <form className="form p-2" onSubmit={this.handleSubmit} ref={this.form}>
                    <h3 className="mt-3 p-1 alert alert-info text-center">Edit {company} Profile</h3>
                    <div className="row my-5">
                        <div className="col-md-4 offset-md-2 col-sm-12">
                            {/*Phone*/}
                            <InputSet weka={dphone} f={this.handleInput} label={dphone.label} value={phone} />
                            {/*Edited by*/}
                            <InputSet weka={deditor} f={this.handleInput} label={deditor.label} value={editor}/>
                            {/*Email*/}
                            <InputSet weka={demail} f={this.handleInput} label={demail.label} value={email} />
                            {/*website*/}
                            <InputSet weka={dwebsite} f={this.handleInput}  label={dwebsite.label} value={website}/>
            

                        </div>
                        <div className="col-md-4 col-sm-12">      
            
                            <TextArea {...dabout} f={this.handleInput} v={about} />
        
                            {/*Contact */}
                            <InputSet weka={deditornum} f={this.handleInput} label={deditornum.label} value={editornum}/>
                            {/*physical address*/}
                            <InputSet weka={daddress} f={this.handleInput} label={daddress.label} value={address} />
                            <div className="mt-2">
                                <p className="text-danger ">{error}</p>
                                <p className="text-success ">{success}</p>
                                {(spinner) ? <p className="text-center"><FontAwesomeIcon icon="spinner" pulse color="green" size="2x" /></p> : null}
                            </div>
        
        
                            {/*submit*/}
                            <InputSubmit {...submit}/> 
                            <p className="text-center mt-2"> <Link 
                                to={`/${company}-firm`}>Return to profile</Link></p> 
                        </div>
        
                    </div>
                </form>
            </Layout>
        );
    }
}

const mapStateToProps=(state)=>({data:state.agencyReducer.profileEdit});
const mapDispatchToProps = (dispatch) =>({
    editProfile:id=>dispatch(agent.editProfile(id))
});

export default connect(mapStateToProps,mapDispatchToProps)(EditProfile);

export function ServicesModal({getService,allServices,services,deleteService, removeServices}){

  
  

    const handleSaves=()=>{
        console.log("saved",services);
        setTimeout(()=>removeServices(),10000);
        $("#serviceModal").modal("hide");
    };
 


  
    return(
        <>
            <div className="row">
                <div className="col-md-12">
      
                    <Services options={allServices} f={getService} h={handleSaves} d={deleteService}/>
   
                </div>
            </div>
        </>
    );
}



const submit= {
    type: "submit",
    classlist: "btn btn-primary btn-block mb-2",
    value: "submit profile"
};
const dphone= {
    placeholder: "Enter mobile",
    type: "text",
    name: "phone",
    classlist: "form-control p-1 my-2",
    label: "1. Mobile"
};
const demail= {
    placeholder: "Enter email address",
    type: "email",
    name: "email",
    classlist: "form-control p-1 my-2",
    label: "3. Email address"
};
const dwebsite= {
    placeholder: "www.aprecom.co.ke",
    type: "url",
    name: "website",
    classlist: "form-control p-1 my-2",
    label: "4. website address"
};
  
 
  
const daddress= {
    placeholder: "Enter your physical address",
    type: "text",
    name: "address",
    classlist: "form-control p-1 my-2",
    label: "7. Physical address:"
};
const deditor= {
    placeholder: "Telephone",
    type: "tel",
    name: "editor",
    classlist: "form-control p-1 my-2",
    label: "2. Enter Telephone number"
};
const dabout= {
    placeholder: " Enter a brief description of the company",
    type: "text",
    name: "about",
    classlist: "form-control p-1 my-2",
    label: "5. About Us (350 words):"
};
const dvision= {
    placeholder: "Type the company vision",
    type: "text",
    name: "vision",
    classlist: "form-control p-1 my-2",
    label: "10. Vision:"
};
const dmission= {
    placeholder: "Type company Mission",
    type: "text",
    name: "mission",
    classlist: "form-control p-1 my-2",
    label: "11. Mission:"
};
const deditornum= {
    placeholder: "14.Enter postal addrees and code",
    type: "text",
    name: "editornum",
    classlist: "form-control p-1 my-2",
    label: "6 Postal address &  code"
};