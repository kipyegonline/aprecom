import { useState, useEffect } from "react"
import {Redirect} from "react-router-dom"
import { connect } from "react-redux"
import axios from "axios"
import {v4} from "uuid"
import Layout from "../Ui/Layout"
import Modal from "../Ui/Modal"
import *  as agent from "../../redux/agencies/actions"
import * as formActions from "../../redux/appReducer/actions.js";
import * as server from "../../redux/services/actions.js";
import ApplicationForm from "./application"
import {InputSubmit, TextArea } from "../utils/utils"
import {sendData,getId} from "../utils/helper"




 class PartII extends React.Component{
constructor(props){
   super(props)
this.state={
    directors:[],
    members: [],
    clients:[],
    profiler:"",
    profileAdd:false,
    completed:false
}
this.handleProfile=this.handleProfile.bind(this)
this.sendServices=this.sendServices.bind(this)
this.submitProfile=this.submitProfile.bind(this)
this.handleInput=this.handleInput.bind(this)
this.handleDirectors=this.handleDirectors.bind(this)
this.handleClients=this.handleClients.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this)
    this.showModal = this.showModal.bind(this)
    this.fetchServices = this.fetchServices
    this.handleCompletion=this.handleCompletion.bind(this)
     }
     
     componentDidMount() {
         
        const { addSelectedServices, getAppData, getDirectors, getClients, getEmployees, getUsername } = this.props;
         const url1 = `./php/services/getServices.php?getServices=true&uuid=${getId()}`
         const url2 = `./php/applications/app.php?getAppData=true&uuid=${getId()}`
         const url3 = `./php/applications/fetchProfiles.php?getDirectors=true&uuid=${getId()}`
         const url4=`./php/applications/fetchProfiles.php?getclientServices=true&uuid=${getId()}`
         const url5 = `./php/applications/fetchProfiles.php?getEmployees=true&uuid=${getId()}`
         const url6 = `./php/applications/fetchProfiles.php?getUsername=true&uuid=${getId()}`
        
         const reqs = [
            this.fetchServices(url1, addSelectedServices),
         this.fetchServices(url2,getAppData),
         this.fetchServices(url3, getDirectors),
         this.fetchServices(url4, getClients),
         this.fetchServices(url5,getEmployees),
         this.fetchServices(url6, getUsername)]
         Promise.all(reqs)        
            
   this.handleCompletion()
         
    
         
         
     }

     handleCompletion() {
        const { services, members, directors, profile, clients, } = this.props;
        if (services.length > 0 && directors.length > 0 && profile.length > 0 && members.length > 4 && clients.length > 0) {
            this.setState({completed:true})
          }
          
     }

     UNSAFE_componentWillReceiveProps(nextProps) {
         
         const { appData } = nextProps;
         let newProfile=[]
         
         this.setState({
             
             profileAdded: appData.companyProfile !==null ? true:false
         })
     }
fetchServices(url,callback) {
         axios
            .get(url)
                .then(res =>callback(res.data))
                .catch(err => console.log(err.message))
        
        }//inner func
         
     
handleProfile(name,value){

this.setState({[name]:value})
}
handleInput(name,num){
   
    if(name.trim().length===0 || num.trim().length===0)return;
    const member = { name, num, uuid:getId() };
    this.setState({ members: [...this.state.members, member] })
    if(name.length>3 && num.trim().length>3){
        this.props.addEmployees(member)
        sendData("./php/applications/profile.php?addEmployees=true",member);
    }
}
handleDirectors(name,num){
    if(name.trim().length===0 || num.trim().length===0)return;
    const member = { name, num, uuid:getId() };
this.setState({directors:[...this.state.directors,member]})
if(name.length>3 && num.trim().length>3){
    this.props.addDirector(member);
    sendData("./php/applications/profile.php?addDirectors=true",member);
}
}
handleClients(name,num){
    if(name.trim().length===0 || num.trim().length===0)return;
    const member = { name, num, uuid:getId() }
    this.setState({ clients: [...this.state.clients, member] })
    if(name.length>3 && num.trim().length>3){
        this.props.addClients(member)
        sendData("./php/applications/profile.php?addclientServices=true",member);
    }
}
submitProfile(){
    const { profiler } = this.state;
    
    if(profiler.length===0)return;
    
    this.setState({ profileAdded: true })
    if (profiler.trim().length > 50) {
        
        const data={profile:profiler,uuid:getId()}
        sendData("./php/applications/profile.php?addProfile=true", data);
        this.props.setProfile(profiler);
    }
    }
     showModal() {
       
        if (getId()) {
         $("#servicesApp-modal").modal({ show: true })
            this.props.clearServices();
        } else {
            
            
            alert("It seems you are not logged in,");
      }
    
     }

    sendServices (dataArr){        
        dataArr.forEach(data => {
                      
                data.uuid = getId();
                $.ajax({
                    type: "POST",
                    url: "./php/services/services.php?agencyService=true",
                    data
                })
                .then(res=>console.log(res))
                .catch(err=>console.error(err))     
            })
         }

handleSubmit(e){
    e.preventDefault();
    
    const { services, members, directors, profile, clients, appData } = this.props;
    
    if (e.target.name === "submitBtn") {
        if (services.length > 0 && directors.length > 0 && profile.length > 0 && members.length > 4 && clients.length > 0) {
          this.setState({completed:true})
        }else{
            alert("Your information is incomplete,please check")
        }
    }
    
}

    render(){
        const { profileAdded,profiler } = this.state;
        const { services,members, directors,profile, clients,appData,completed } = this.props
        const uuid = getId();
        
      
const part1=(  <Layout>
   <form className="form my-2 bg-light pl-5 py-3" onSubmit={this.handleSubmit}>
       <div className="row my-2 p-1">
            <div className="col-md-12 col-sm-12"><h3 className="text-center text-header">Application Form part II</h3>
            <p className="alert alert-primary text-center">Part 2 of 2</p></div>
           <div className="col-md-5 col-sm-12 p-2 ">

   { (!profileAdded) ? <> <TextArea {...dprofile} f={this.handleProfile} v={profiler} />
                    <button className="btn btn-sm bg-aprecom3" onClick={this.submitProfile}>Add profile </button>
                    <span className="text-danger ml-5"
                        onClick={() => this.setState({ profileAdded: true })}>X</span> </> :
              <>
              <div className="card p-2 mb-2">
                            <p className=" text-center text-">1. Profile</p>
                            {profile.length > 0 ? profile.map((prof, i) => <p key={i}>{prof}</p>) :
                                <p> No profile yet....</p>} <span
                                    className="float-right badge badge-info badge-pill"
                                onClick={() => this.setState({
                                    profileAdded: false,
                                    profiler:profile.join(" ")
                                })}>Edit</span>
              
</div>
</> }
               <p>2.Please provide Names of Two Company Directors: </p>
               <ClientMembers members={this.props.directors} f={this.handleDirectors} limit={2} val2={"Mobile/telephone"}/>
               <hr className="divider"/>
                <p> 3. Please provide <b>five</b> Client Services employees registered with PRSK:</p>
                <p>{members.length} of 5 members</p>
               <ClientMembers members={this.props.members} f={this.handleInput} limit={5} val2={"PRSK Membership No"}/>
               
           </div>
            <div className="col-md-5 col-sm-12 p-2 offset-md-1 ">
                <p>4. Click to add Services offered by your firm.</p>
                {services.length <= 0 ? <button onClick={this.showModal} className="btn btn-sm bg-aprecom4">Add Services</button> : <p className="alert alert-primary">{services.length} services selected</p>}
                <ul>
                    {services.map(service=> <li key={service.id}>{service.service}</li>)}
                </ul>
                <Modal id="servicesApp-modal"> <Services
                    d={this.props.deleteService}
                    f={this.props.addService}
                    h={this.sendServices} 
                    services={this.props.services}
                    options={this.props.allServices} /> </Modal>
                          
           <p>5. Please state two Clients for whom you provide any of the services above to:</p>
               <ClientMembers members={this.props.clients} f={this.handleClients} limit={2} val2={"Client Contact Name."}/>
                { (completed) ? null : <InputSubmit {...submit} f={this.handleSubmit} />}
                
           </div>
       </div>

   </form>
   

        </Layout>)
        if (uuid ) {
            return appData.company === null ? <ApplicationForm /> : part1;
        } else {
            return <Redirect to={"/login"}/>
        }
      
        
    }
    
}

const ClientMembers=({f,members,limit,val2})=>{

    const [name,setName]=useState("")
    const [num,setNum]=useState("")
    const [five, setFive] = useState(false)
    

    
    
    
    
   const handleInput=(e)=>{
if(e.target.name==="name"){
       setName(e.target.value)
    
    
}
if(e.target.name==="num"){
        setNum(e.target.value)
       
}
   }
   
   const handleClients=()=>{
       f(name,num)

        members.length>limit  ? setFive(true) :null;

       setTimeout(()=>{setName(""),setNum("")},100)
    };
    return(
        <>
        { (members.length>0) ? <table className="table table-bordered table-hover table-striped">
    <thead><tr><th>#</th><th>Name</th><th>{val2}</th></tr></thead>
    <tbody>{members.map((member,i)=> <Members key={i} index={i} {...member}/>)}</tbody>
        </table> :null}

            {members.length < limit ?
                <div className="form-group mt-3" style={{ display: (five) ? "none" : "block" }}>
                    <label>Name</label>
                    <input type="text" className="form-control mb-2" name="name" value={name} onChange={handleInput} />
                    <label>{val2}</label>
                    <input type="text" className="form-control mb-2" name="num" value={num} onChange={handleInput} />
                    <button className="btn btn-md bg-aprecom3" onClick={handleClients}>Add </button>
                </div> : <a href={"#/parte"}>Edit</a>}
        </>
    )
}
const Members = ({ name, index, num, f }) => (<tr><td>{index + 1}</td><td>{name}</td><td>{num}</td></tr>)





export const Services = ({ f, d, options, h, services }) => {
    const [success,setSuccess]=useState("")
    
   const  handleModal=()=> {
    
      
       if (services.length > 0) {
           h(services) 
           setSuccess("Changes saved!")
       } else {
            return alert("You have not selected anything")
       }
       
       setTimeout(() => {
           setSuccess("")
           $(".service-areas").removeClass("bg-aprecom1");
        $("#servicesApp-modal").modal("hide")         
        
       },3000)
       
        
       
    }
    
    const handleService=(e,index,area) => {
        

    if (e.target.dataset.check==="false") {
        e.target.classList.add("bg-aprecom1")
        e.target.dataset.check="true"
        f({ index, area });
            
        } else {
        e.target.classList.remove("bg-aprecom1")
        e.target.dataset.check = "false";
        d(index);
        
        
        }        
        
        
    }
    
    const styleList={display:"inline-block",margin:".15rem",padding:".15rem"}

    return(
        <>
            <div className="row">
                <div className="col-md-12 col-sm-12">
                    <p>{servicesTag}</p>
                <p className="text-primary text-center ">{services.length} selected</p>
    <ul style={{listStyle:"none"}}>
      
                        {options.map(option => <li key={option.id} style={styleList}><button className={"btn btn-block service-areas "} data-check={false} onClick={(e) => handleService(e, option.id, option.area)}>{option.area}</button></li>)} </ul>
                    <p className="p-1 text-success">{success}</p>
                    <button
                        className={" ml-5 btn btn-md bg-aprecom3 text-white"}
                    onClick={handleModal}>Save Changes</button>
                </div>
            </div>
    
               
    </>
    )
}



const dprofile= {
    placeholder: "Brief description",
    name:"profiler",
  classlist: "form-control p-1 my-2",
    label: "1. Enter company profile (200 words)."
  };
  const submit= {
    type: "submit",
    classlist: "btn bg-aprecom1 btn-block mt-3 text-white",
    value: "submit All"
  };

  const servicesTag=`4. What are the company’s key areas of specialisation and/or business activities?  Click where applicable and save changes.
  `
  const mapStateToProps=state=>({
   
      allServices: state.services.allServices,
      services: state.services.services,
      appData: state.appReducer.appData,
      directors: state.appReducer.directors,
      clients: state.appReducer.clientServices,
      members: state.appReducer.employees,
      username:state.appReducer.username,
    profile:state.appReducer.profile
});

const mapDispatchToProps=(dispatch)=>({
   
    addService:(serve)=>dispatch(server.addService(serve)),
    deleteService:id=>dispatch(server.deleteService(id)),    
    clearServices:()=>dispatch(server.clearServices()),
    editProfiled: profile => dispatch(agent.editProfile(profile)),
     addSelectedServices:data=>dispatch(server.ourServices(data)),
    getAppData: data => dispatch(formActions.fetchAppData(data)),
    getDirectors: data => dispatch(formActions.fetchDirectors(data)),
    getClients: data => dispatch(formActions.fetchClients(data)),
    getEmployees: data => dispatch(formActions.fetchEmployees(data)),
    getUsername: data => dispatch(formActions.setUserName(data)),
    setProfile: profile => dispatch(formActions.setProfile(profile)),
    addDirector: director => dispatch(formActions.addDirector(director)),
    addClients:client=>dispatch(formActions.addClients(client)),
    addEmployees:employee=>dispatch(formActions.addEmployees(employee)),
    

})
   

export default connect(mapStateToProps,mapDispatchToProps)(PartII);


