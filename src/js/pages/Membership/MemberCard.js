import {useState, useEffect} from "react";
import {withRouter,Link} from "react-router-dom";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {connect} from "react-redux";
import Layout from "../Ui/Layout";
import Modal from "../Ui/Modal";
import { SocialMedia, socials } from "../Ui/Nav";
import {getId} from "../utils/helper";

import *  as agent from "../../redux/agencies/actions";
import * as server from "../../redux/services/actions";
import *  as formData from "../../redux/appReducer/actions";
import {Services} from "./App2";


const SeeMore = ({ match, data, services, allServices, profile, setProfile, editProfiled, addService, deleteService, ourServices, clearServices,getSocials }) => {
    //find agency data
    data = data == undefined ? [] : data;
    const clickedcompany = match.params.agency;
    window.scrollTo({top:0,behavior:"smooth"});
    let bdata = data.find(item => item.company == clickedcompany);  
    //store agency id
    if(bdata){localStorage.setItem("uuid", JSON.stringify(Number(bdata.id)));}
  
    const clickedId=JSON.parse(localStorage.getItem("uuid"));
    //fetch services
    const fetchServices=(clickedId)=>{
      
        $.ajax({
            url: `./php/services/getServices.php?agencyService=true&serviceId=${clickedId}`,
            type: "GET",
            dataType: "json"
        })
        
            .then(res => {
                
                const services = res.slice(0, res.length - 1);
                const {companyProfile} = res.slice(-1, res.length)[0];
                
                ourServices(services);
                setProfile(companyProfile);
            })
            .catch(err=>console.log(err ));
    };
    const fetchSocials=(clickedId)=>{
        fetch(`./php/agencies/agencies.php?fetchSocials=true&id=${clickedId}`)
            .then(res=>res.json())
            .then(res => {
       
                getSocials(res);
            });
    };
    //some events for working on agencies whenever users reload on use keyboards
    window.onload = function () {
    
        fetchServices(clickedId);       
        
        
       
    };
    
    
    //fetch data whenever a user refreshes a page
    useEffect(() => {
     
        fetchSocials(clickedId);
        if (bdata) {
            
            setProfile(bdata.companyProfile);
            if (bdata.service_edit === "1") {
                fetchServices(clickedId);
            }else{
                clearServices();
            }
        }
     
    }, []);
   
    //
    return (
        <Layout>
            {data.length>0 ?   <Carded 
                data={bdata}
                addService={addService}
                services={services}
                profile={profile}
                allServices={allServices}
                deleteService={deleteService}
                clearServices={clearServices}
                editProfile={editProfiled}
                socialMedia={ bdata.socials.status==200 ? bdata.socials :socials}
            />    :null }
        </Layout>
    );  };
//socialMedia={bdata.socials.status === 200 ? bdata.socials : socials}
  
   
const mapStateToProps=state=>({
    data:state.agencyReducer.agencies,
    services:state.services.services,
    profile:state.appReducer.profile,
    allServices:state.services.allServices,
});

const mapDispatchToProps=(dispatch)=>({
       
    addService:(serve)=>dispatch(server.addService(serve)),
    deleteService:id=>dispatch(server.deleteService(id)),
       
    editProfiled: profile => dispatch(agent.editProfile(profile)),
    ourServices: data => dispatch(server.ourServices(data)),
    clearServices: () => dispatch(server.clearServices()),
    setProfile:data=>dispatch(formData.setProfile(data)),
    getSocials:data=>dispatch(agent.getSocials(data))
        
    
});

export  default connect(mapStateToProps,mapDispatchToProps)(SeeMore);
/**
     * <SocialMedia classlist={"social-table social-nav"} {...socials}/>
     */
const Carded=({data, allServices, addService, editProfile, deleteService,services,clearServices,profile,socialMedia})=>{
      
    
    const handleServiceClick = () => {
        $("#servicesApp-modal").modal({ show: true, backdrop: "static" });
        clearServices();
    }; 
    

    const sendServices = (dataArr) => {
        dataArr.forEach(data => {
            data.uuid=JSON.parse(localStorage.getItem("uuid"));
            $.ajax({
                type: "POST",
                url: "./php/services/services.php?agencyService=true",
                data
            })
                .then(res=>console.log(res))
                .catch(err=>console.error(err));
        });
        $("#service-btn").animate(1000).hide();
       
    };
    let uuid = getId();
    if(uuid){
        uuid=uuid.split("-");
        uuid=uuid.slice(0,1).join(" ");
    }
      
    //console.log("uuid",data);
      
    return (<>
       
        {/**Service edit  modal*/}
        <Modal id="servicesApp-modal" title={data.company}>
            <Services options={allServices} services={services} f={addService}
                h={sendServices}
                d={deleteService} />
            
           
        </Modal>
        <div className="row my-3">
            <div className="col-md-12 col-sm-12">   <h5 className="card-title red lighten-1 text-white p-1 text-center mb-2">{data.company} </h5> </div>
            <div className="col-md-4  col-sm-12 card">
                <div className="card-body">         
          
                    <ul className="list-group list-group-flush mb-2">
                        <li className="list-group-item"><img src={data.pic} className=" img-thumbnail"/></li>
                        <li className="list-group-item"> <FontAwesomeIcon icon="mobile" className="mr-2" color="brown" size="lg" />
                            {data.phone}</li>
                        <li className="list-group-item"><FontAwesomeIcon icon="phone-alt" className="mr-2" color="brown" size="lg" />
                            {data.telephone}</li>
                        <li className="list-group-item"><FontAwesomeIcon icon="envelope" className="mr-2" color="brown" size="lg" /> 
                            {data.email}</li>
                        <li className="list-group-item"><FontAwesomeIcon icon="globe" className="mr-2" color="brown" size="lg" /> <a href={data.website} target="_blank">{data.website}</a></li>
                        <li className="list-group-item"><FontAwesomeIcon icon="map-marker-alt" className="mr-2" color="brown" size="lg" />{data.address}</li>
                        <li className="list-group-item text-muted">
                            <FontAwesomeIcon icon="eye" className="mr-2" color="brown" size="lg" /> Profile views: {data.counter}</li>
          
                        <li className="list-group-item">{data.nickName}{" "}Social media channels:
                            <SocialMedia classlist={"social-table social-nav"} {...socialMedia} iconSize={"3x"} /></li>
                        {(uuid=="1") ?
                            <li className="list-group-item">
                                Profile Views: {data.counter}</li> : null}
                        {(uuid=="1") ?
                            <li className="list-group-item">
                                <Link to={`/edit-social-media-${data.company}`} > Edit social media</Link></li> : null}
                  
                        {(uuid=="1") ? <li className="list-group-item">
                            <Link to={`/edit-profile-${data.company}`}>Edit profile</Link></li> : null}
                  
                        <li className="list-group-item"><Link to={"/members"}>
                            <FontAwesomeIcon icon="undo"/>{" "} Return to members page</Link></li>
                    </ul>  
                    {data.service_edit == "0" &&  uuid=="1" ?
                        <button
                            id="service-btn"
                            className="btn btn-block text-white bg-aprecom2"
                            onClick={handleServiceClick}>Add Services</button> : null}
        
                </div>
            </div>
            <div className="col-md-7 col-sm-12 p-2 ml-1   card">
                {profile.length > 0 ? <AboutCompany profile={profile} nickName={data.nickName} /> : null}
                {services.length > 0 ? <OurServicesList services={services}/> :
                <p className="text-center"><FontAwesomeIcon icon="spinner" pulse size="3x" color="green"/> </p> }
                
                
            
            </div>        

            {/*End row*/} </div>
        
    </>);
};


const AboutCompany = ({ profile,nickName }) => {
    
    let profileExist;
    if (profile.length > 1) {
        profileExist = (
            <>
                <h6 className="text-center p-2 text-white  light-blue accent-4 font-weight-bold text-header-text-lg text-shadow">About {nickName}</h6>
                <hr className="divider" />
                { profile.map((prof, i) => <p key={i}>{prof}</p>) }
        
            </>);
    }else{
        profileExist =""
    }
  
    return profileExist;
};
const bg = ["bg-aprecom1", "bg-aprecom2", "bg-aprecom3", "bg-aprecom4"];
const servicesTitle= ["Our Core Offerings", "Our Services", "Our Specialties", "Our Specialist Expertise","Our Expertise", "Our Work","What We Do", "Our Solutions"];
const pickers=(arr)=>{
    
    return arr[Math.floor(Math.random() * arr.length)];
};
const OurServicesList = ({services}) => (
    <div className="row">
        <div className="col-md-12 col-sm-12 p-1">
            <h5 className="text-center font-weight-bold my-1 p-1"> {pickers(servicesTitle)}</h5>
        </div>
            
        <div className="col-md-12 col-sm-12">
            <ul className="list-service">
                {services.map((service, i) => {
                           
                    return (<li
                        key={service.id}
                        className="list-group-service info-color text-white">
                        {service.service}
                    </li>);
                })
                }
            </ul>
            <p className="mt-5 text-center"><Link to={"/members"}>
                <FontAwesomeIcon icon="undo" />{" "}Return to members page</Link></p>
            
        </div>
    </div>
);