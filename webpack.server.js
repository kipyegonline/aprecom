const path=require("path")
module.exports={
    target:"node",
    entry:"./server.js",
    output:{
        path:path.resolve(__dirname,"dist"),
        filename:"bundle.js"
    },
    module:{
        rules:[{
            test:/\.js$/,
            exclude:/node_modules/,
            use:[{
                loader:"babel-loader",
            options:{presets:["@babel/react",["@babel/env",{"targets":{"browsers":"last 2 versions"}}]]}
        },]

        },
        {
            test:/\.(png|jpg|jpeg|csv|svg)$/,
            use:[{
                loader:'file-loader',
                options:{
                    name:'[name].[hash].[ext]',
                    outputPath:'images',
                    publicPath:'./images'
                }
                
            }]
        }]
    }
}