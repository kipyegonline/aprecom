<?php

require("../db.php");

if(isset($_GET["handleMails"]) &&  $_GET["handleMails"]=="true"){
   $name=strip_tags($_POST["name"]);
   $email=strip_tags($_POST["email"]);
   $subject=strip_tags($_POST["subject"]);
   $message=strip_tags($_POST["message"]);
   $userId=$_POST['userId'];
   
   if(!empty($name) && !empty($email) && !empty($subject) && !empty($message)){
      $to = "info@prhub.co.ke"; // Add your email address 
      $email_subject = "Website Contact Form:  $name";
      $email_body = "You have received a new message from your website contact form.\n\n".
      "Here are the details:\n\nName: $name\n\nEmail: $email \n\nMessage:\n$message";
      $headers = "From: APReCoM \n"; // This is the email address the generated message will be from. 
      $headers .= "Reply-To: $email"; 
      $res=[];
      if(mail($to,$email_subject,$email_body,$headers, '-f noreply@prhub.co.ke'))
         {
            $res['status']=200;
            $res['msg']="Your messsage has been submitted succcessfully. We will get back to you soon";
            echo json_encode($res);
            storeMails($name,$email,$subject,$message);
         }else{
            $res['status']=201;
            $res['msg']="Error submitting your message.Check your network and try again later.";
            echo json_encode($res);
         }
         
      
        

  
}

}

function storeMails($name,$email,$subject,$message,$userId){
   global $connection;
  
   try{
      $sql="INSERT INTO website_form (`name`,`email`,`subject`,`message`,`userId`) VALUES (:username,:email,:subject,:message)";
      $stmt=$connection->prepare($sql);
      $stmt->bindParam(":username",$name);
      $stmt->bindParam(":email",$email);
      $stmt->bindParam(":subject",$subject);
      $stmt->bindParam(":message",$message);
      $stmt->bindParam(":userId",$userId);
      $stmt->execute();
      
      }
      catch(PDOException $e){
      echo "error adding mails to DB";
      }
}
?>