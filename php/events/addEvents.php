<?php
 require_once("../db.php");
 include_once('../funcs/func.php');
if(isset($_GET['addEvents']) && $_GET['addEvents']=="true"){
  
    $course=getValue("coursename");
    $eventDate=getValue("date");
    $location=getValue("location");
    $des=getValue("des");
    $fee=getValue("fee");
    $points=getValue("points");
    try{
$sql="INSERT INTO `events_table` (`coursename`,`eventdate`,`location`,`description`,`fee`,`points`)
VALUES(:course,:eventdate,:location,:description,:fee,:points)
";
$stmt=$connection->prepare($sql);
$stmt->execute(Array(
":course"=>$course,
":eventdate"=>$eventDate,
":location"=>$location,
":description"=>$des,
":fee"=>$fee,
":points"=>$points));

 $connection->lastInsertId();
 echo "The event has been added successsfully";
    }
    catch(PDOException $e){
        echo "Error adding details to register";
    }
}


if(isset($_GET['fetchEvents']) && $_GET['fetchEvents']=="true"){

    try{
        $sql="SELECT * FROM `events_table` ORDER by eventdate desc";
        $stmt=$connection->query($sql);
        $res=$stmt->fetchAll(PDO::FETCH_ASSOC);
        echo json_encode($res);
    }
    catch(PDOException $e){
        echo "Error retrieving events data. Try again later.";
    }
   

}