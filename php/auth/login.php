<?php
include_once("../db.php");
require("../funcs/func.php");
//handling the lohin process
if(isset($_GET['handleLogin']) && $_GET['handleLogin']=="true" ){
    $res=[];
    $password=getValue("password");
    $email=getValue("email");
    
    //validate and sanitize email password;
    if(strlen($email)>5 && strlen($password)>5){
        //verify from server
        $sql="SELECT *
         FROM `applicants_account` 
         WHERE  `email`=:email OR `username`=:username";

        $resp=$connection->prepare($sql);

        if($resp){
               //check email
               $resp->execute(Array(":email"=>$email , ":username"=>$email));
            $data=$resp->fetch(PDO::FETCH_ASSOC);
            
            //use the data
            $verifique=$data["verification"];
            $storedpwd=$data['password'];
            $active=$data["activated"];
            $userId=$data['id'];
            $pwd= hashPassword($password,$verifique);          
            $sizeof=strlen($active);
           if($sizeof==0){
            $res["status"]=201;
            $res["text"]="Email or password is incorrect.Try again.";
            echo json_encode($res);
           }else if($active==0){
        $res["status"]=201;
            $res["text"]="Kindly activate your account.";
            echo json_encode($res);
           
           }else{

           

            //check if passwords match
            if($pwd==$storedpwd){
    //log in
    $query="SELECT id from `aprecom_members` WHERE verified=:verified";
    $stmt=$connection->prepare($query);
    if($stmt){
        //retrieve user id and send token
       
        $stmt->bindValue(":verified",$verifique);
        $stmt->execute();

        $data=$stmt->fetch(PDO::FETCH_ASSOC);
        $res["status"]=200;
        $res["token"]=$userId."-".$verifique."-".$data['id'];
        $res['success']="login successful";
        echo json_encode($res);

    }
            }else{
                //if passwords don't match
                $res["status"]=201;
                $res["text"]="Email or password is incorrect.Try again";
                echo json_encode($res);
            }
        }//active x
            
        }else{
            $res["status"]=201;
        $res["text"]="Email or password is incorrect.Try again";
        echo json_encode($res);
        }


    }else{
        $res["status"]=201;
        $res["text"]="email and password too short" . strlen($password);
        echo json_encode($res);
    }
}
