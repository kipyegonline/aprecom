<?php
include_once("../db.php");
require("../funcs/func.php");
///getting email from  the user
if(isset($_GET['authEmail']) && $_GET['authEmail']=="true" ){

    $email=strip_tags($_GET['email']);
    $res=verifyEmail($email);
    echo json_encode($res);

}

//creating user account
if(isset($_GET['createUser']) && $_GET['createUser']=="true" ){
   
$res=[];
$name=getValue('name');
$email=getValue('email');
$password=getValue("password");
$pwdc=getValue('confirmPassword');
$verified=getValue('verified');
$category=getValue("category");
$res=verifyEmail($email);

if( strlen($name)>5 && strlen($email)>5 &&strlen($password)>6 &&strlen($pwdc)>6){

    if($res['exist']){
        $res["status"]=201;
        $res['text']="This email already exists on our system";
        $res['exist']=false;
        echo json_encode($res);

    }else if($password != $pwdc ){
        $res["status"]=201;
        $res['text']="The passwords do not match";
        $res['exist']=false;
        echo json_encode($res);

    }else{
        //send to server
       
        createAccount($name,$email,$password,$verified,$category);
    }

}else{
    $res["status"]=201;
        $res['text']="Your usename should be more than 5 characters";
        $res['exist']=false;
        echo json_encode($res);
}

}
//activating user account
if(isset($_GET['activateAccount']) && $_GET['activateAccount']=="true" ){
    $vvid=$_GET['vvid'];
    $newId=$_GET['newId'];
    
    activaAccount($vvid,$newId);

}
//verifying email is realy yours

function verifyEmail($email){
    
    global $connection;
   $res=[];    
$sql="SELECT COUNT(*) from `applicants_account` where email='{$email}'";

$stmt=$connection->query($sql);

$rows= $stmt->fetchColumn();
if($rows>0){
    $res["status"]=201;
    $res['text']="This email already exists on our system";
    $res['exist']=true;
    return $res;
}else{
    $res["status"]=201;
    $res['text']="";
    $res['exist']=false;
    return $res;
}


}

//signing up at last
function createAccount($name,$email,$password,$verified,$category){
    $res=[];
    try{
        $password=hashPassword($password,$verified);
        global $connection;
    $sql="INSERT INTO  `applicants_account` (username,email,password,verification,activated,category)
    VALUES(:username,:email,:password,:verification, :activated, :category)";
    $stmt=$connection->prepare($sql);
    if($stmt){
        $stmt->execute(Array(            
            ":username"=>$name,
            ":email"=>$email,
            ":password"=>$password,
            ":verification"=>$verified,
            ":activated"=>false,
            ":category"=>$category
        ));

        $id=$connection->lastInsertId();
        if($id>0){
            
            $link="http://www.prhub.co.ke/aprecom/#/activate-account-{$verified}";
            $to=$email;
            $subject="APReCoM activation link";
            $body="Dear {$name}, \n Kindly Follow this link to activate your APReCoM membership application account.\n
            $link";

            $headers="From: info@prhub.co.ke";
            

            //send activation link to email
            
            if(sendMail($to,$subject,$headers,$body)){
                $res['status']=200;
                $res['msg']= "Account activation link has been sent to your email address .";
                echo json_encode($res);
            }else{
                $res['status']=201;

                $res['msg']= "Server  is unavailable. Try again later.";
                echo json_encode($res);
            }
        }else{
            echo "Error creating account";
        }
    }
    }
    catch(PDOException $e){
  echo "Error creating account .". $e->getMessage();
        }
    
}

//activating account at last
function activaAccount($verifiq,$newvvid){
    $res=[];
    global $connection;

    $column="SELECT COUNT(*) from `applicants_account` where verification='{$verifiq}' AND activated=0";
 
 $columnChecker=$connection->query($column);
 
 $rows= $columnChecker->fetchColumn();
 if($rows==0){
    $res['status']=201;
    $res['msg']="The activation link is now invalid.";
    echo json_encode($res);
 }else{

 $query="SELECT verification,password, category FROM `applicants_account` WHERE verification='{$verifiq}'";
    $stmt=$connection->query($query);
    if($stmt){
  $data=$stmt->fetch(PDO::FETCH_ASSOC);
  $truthy=true;
  $update="UPDATE `applicants_account` SET activated={$truthy}  WHERE verification='{$verifiq}'  LIMIT 1 ";
  $updated=$connection->exec($update);
  if($updated){
    $category=$data["category"];
    createCompany($verifiq,$category);
    
  }
  
  

    }else{
        $res['status']=201;
        $res['msg']="Error verifying account...Try again later";
        echo json_encode($res);
    }
}

}
//creating company at last

function createCompany($verified,$category){
    $res=[];
    try{
        global $connection;
        $column="SELECT COUNT(*) from `aprecom_members` where verified='{$verified}'";
 
        $columnChecker=$connection->query($column);
        
        $rows= $columnChecker->fetchColumn();
        if($rows>0){
            $res["status"]="200";
            $res['msg']="Success! Your account is  active.";
             echo json_encode($res);
            
        }else{
            $sql="INSERT INTO  `aprecom_members` (verified,category)
    VALUES(:verification,:category)";
    $stmt=$connection->prepare($sql);
    if($stmt){
        $stmt->execute(Array(            
            ":verification"=>$verified,":category"=>$category
        ));

        $id=$connection->lastInsertId();
        if($id>0){
            $res["status"]="200";
           $res['msg']="Success! Your account is active.";
            echo json_encode($res);
        }else{
            $res["status"]="201";
            $res['msg']="Error creating account. Try again later";
            echo json_encode($res);
        }
    }

        }
    
    }
    catch(PDOException $e){
  echo "Error creating account .". $e->getMessage();
        }
    
}
//column checker, it sucks though
function ColumnChecker($table,$var, $column){
    global $connection;
       
 $sql="SELECT COUNT(*) from `{$table}` where {$var}='{$column}'";
 
 $stmt=$connection->query($sql);
 
 $rows= $stmt->fetchColumn();
 if($rows>0){
     return true;
 }else{
     return false;
 }
}

//creating super user account for me
function createSuperUser(){
    global $connection;
    $superChecker="SELECT COUNT(*) from `applicants_account` where id=1 ";
    $sfeed=$connection->query($superChecker);
    if($sfeed){
        if($sfeed->fetchColumn()>0){
            
        }else{

            createAccount("kipyegonline","vinnykipx@gmail.com","//matata11","communic@te11","NaN"); 
            echo "creado";
        }
    }
}
createSuperUser();