<?php
include_once("../db.php");
require("../funcs/func.php");
//updating the user password
if(isset($_GET['updatePassword']) && $_GET['updatePassword']=="true" ){
    $res=[];
    $newpassword=getValue("newPassword");
    $confpassword=getValue('confirmPassword');
    $verifiq=getValue("verification");
    $newVerifique=getvalue("newVerification");

    if(strlen($newpassword)>6 && strlen($confpassword)>6){
        if($newpassword != $confpassword){
            $res['status']=201;
        $res['error']="passwords do not match";
        echo json_encode($res);
        }else{
            
            //check if reset token is still valid
$validateToken="SELECT COUNT(*) FROM applicants_account WHERE verification='{$verifiq}'";

if(runQueries($validateToken)){
    /**Before we change password we need to check whether it looks like the existing one*/ 
         
    //get user id and hash new password for comparison
       $sql="SELECT  id,verified from aprecom_members WHERE verified='{$verifiq}'";
       $data=getData($sql);
       $verified= $data['verified'];
       $userId=$data['id'];
       //hash
       $passwordchecker=hashPassword($newpassword,$verified); 
       //thje imncoming password  
       $incomingPassword=hashPassword($newpassword,$newVerifique);  
        
       //check DB 
       $query="SELECT count(*) FROM applicants_account WHERE `password`='{$passwordchecker}' ";
       
       if(runQueries($query)){
        $res['status']=201;
        $res['msg']="New password cannot be same  as old password";
        echo json_encode($res);
       }else{

        $updatepassword="UPDATE applicants_account SET password=:newpassword,verification=:verification, activated=:activated where verification='{$verifiq}' LIMIT 1";
        //updating the database

        setUpdate($updatepassword,$incomingPassword,$newVerifique,$userId);

       
        
       }//old password or new

       
    }else{
        $res['status']=201;
        $res['msg']="This reset link is expired. Kindly request another reset on the login page";
       
        echo json_encode($res);
    }//token validation


        }//main part
    }else{
        $res['status']=201;
        $res['msg']="password must be 6 or more characters long; including a number and uppercase letters";
        echo json_encode($res);
    }//passsord length

}


//email the password to the user
if(isset($_GET['EmailresetPassword']) && $_GET['EmailresetPassword']=="true" ){
    $res=[];
    $email=htmlentities(strip_tags($_GET['email']));
    
    
    if(strlen($email)>5){
        $query="SELECT count(*) FROM applicants_account WHERE email='{$email}' ";
       if(runQueries($query)){
        $query="SELECT username, verification FROM applicants_account WHERE email='{$email}' ";
        sendResetLink($query,$email);
        activateAccount(false,$email);
       
       }else{
        $res['status']=201;
        $res['msg']="This email address does not exist in our database";
        echo json_encode($res);
       }




    }else{
        $res['status']=201;
        $res['msg']="Invalid email address";
        echo json_encode($res);
    }

}

//custom helper function for cjeckin columns
function runQueries($query){
    global $connection;
   
          $stmt=$connection->query($query);
          if($stmt){
              if($stmt->fetchColumn()>0){
                  return true;
              }else{
                  return false;
              }
          }
}

//reset link func sent
function sendResetLink($query,$email){
    $feed=[];
    global $connection;   
    $feed=[];
          $stmt=$connection->query($query);
          if($stmt){
            $res=$stmt->fetch();
            $ver=$res['verification'];
            $username=$res['username'];
            $headers="From: info@prhub.co.ke";
                  
           $body="APRECOM password reset";
           
            $link="http://wwww.prhub.co.ke/aprecom/#/reset-password-{$ver}";

            $msg="Dear {$username}, \n Kindly Follow this link to reset your password \n
            {$link}";
            if(sendMail($email,$body,$headers,$msg)){
                $feed['status']=200;
                $feed['msg']="A reset link has been sent to your email address";
                echo json_encode($feed);
            }else{
                $feed['status']=201;
            $feed['msg']="Server encountered error. Try again later";
            echo json_encode($feed);
            }
            
                  
          }
}

//fetching the user data from the server
function getData($query){
    global $connection;   
          $stmt=$connection->query($query);
          if($stmt){
              $data=$stmt->fetch(PDO::FETCH_ASSOC);
              return $data;
          }

}

//set update func
function setUpdate($query,$incomingPassword,$newVerifique,$id){
    global $connection;
    $res=[];
    $stmt=$connection->prepare($query);
    $stmt->execute(Array("newpassword"=>$incomingPassword,":verification"=>$newVerifique,":activated"=>true));
    
    if($stmt->rowCount()>0){
        updateVerify($id,$newVerifique);
        $res['status']=200;
        $res['msg']="Password changed successfully.";
        echo json_encode($res);
    }else{
        $res['status']=201;
        $res['msg']="The server could not change password. Try again later";

        echo json_encode($res);
    }
    
}
//verify password against the existing password

function updateVerify($id,$newVerifique){
    global $connection;
    $res=[];
    $query="UPDATE aprecom_members SET verified=:verified 
        where id=:id LIMIT 1";
    $stmt=$connection->prepare($query);
    $stmt->execute(Array(":verified"=>$newVerifique,":id"=>$id)); 
    $res['msg2']=$stmt->rowCount();
    
}
//activating account func.

function activateAccount($activated,$email){
    global $connection;
    $res=[];
    $query="UPDATE applicants_account SET activated=:activated  WHERE email='{$email}'
        LIMIT  1";
    $stmt=$connection->prepare($query);
    $stmt->execute(Array(":activated"=>$activated)); 
   
    
}
