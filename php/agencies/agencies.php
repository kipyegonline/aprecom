<?php
require_once("../db.php");
include_once('../funcs/func.php');
if(isset($_GET['getAgencies']) AND $_GET['getAgencies']=="true" ){


try{
$sql="SELECT * FROM aprecom_members where edited=1 order by company ";
$stmt=$connection->query($sql);
if($stmt){
    $res=[];
    
    while($row=$stmt->fetch(PDO::FETCH_ASSOC)){
        
    $row['socials']=sendSocials($row['id']);
$res[]=$row;

    }

echo json_encode($res);
}else{
    echo "cannot fetch data rn";
}
}
catch(PDOException $e){
    echo "Error adding details to register";
}

}


if(isset($_GET['fetchSocials']) AND $_GET['fetchSocials']=="true" ){
    
$id=$_GET['id'];
$data=sendSocials($id);
echo json_encode($data);

}


function sendSocials($id){
global $connection;
$sql="SELECT * from social_media WHERE company_id={$id}";
$stmt=$connection->query($sql);
if($stmt){
   $res=$stmt->fetch(PDO::FETCH_ASSOC);
   if($res['twitter'] !=null || $res['linkedin'] !=null || $res['facebook'] !=null){
       $res['status']=200;
       return $res;
   }else{
    $res['status']=201; 
    return $res;
   }
   
    

}
}

if(isset($_GET['addAgencies']) && $_GET['addAgencies']=="true"){
    $feedback=[];
    $company=getValue("company");
    $website=getValue("website");
    $email=getValue("email");
    $phone=getValue("phone");
    $category=getValue("category");
    $ownership=getValue("ownership");
    $regNumber=getValue("regNumber");
    $yearOfReg=getValue("YearofReg");
    
    $telephone=getValue("telephone");
    $posta=getValue("posta");
    $address=getValue("address");
    $companyProfile=getValue("companyProfile");
    $membershipContact=getValue("membershipContact");
    $verified=getValue("verified");
    $edited=getValue("edited");
    $service_edit=getValue("service_edit");
    $yearsOp=getValue("yearsOp");

    $query="INSERT INTO aprecom_members (`company`,`website`,`email`,`phone`,`category`,
    `ownership`,`regNumber`,`yearOfReg`,`telephone`,`posta`,`address`,`companyProfile`,`membershipContact`,`verified`,`edited`,`service_edit`,`yearsOp`) 
    VALUES(:company,:website, :email, :phone,:category,
    :ownership,:regNumber,:yearOfReg,:telephone,:posta,:address,:companyProfile,:membershipContact,:verified,:edited,:service_edit,:yearsOp)";

    $stmt3=$connection->prepare($query);
    if(!$stmt3){
        $feedback['status']=201;
        $feedback['msg']="Error adding agencies";
        echo json_encode($feedback);
    }else{
        $stmt3->execute(Array(
            ":company"=>$company,
            ":website"=>$website,
            ":email"=>$email,
            ":phone"=>$phone,
            ":category"=>$category,            
            ":ownership"=>$ownership,
            ":regNumber"=>$regNumber,
            ":yearOfReg"=>$yearOfReg,
            ":telephone"=>$telephone,
            ":posta"=>$posta,
            ":address"=>$address,
            ":companyProfile"=>$companyProfile,
            ":membershipContact"=>$membershipContact,
            ":verified"=>$verified,
            ":edited"=>$edited,
            ":service_edit"=>$service_edit,
            ":yearsOp"=>$yearsOp
        ));
        if($connection->lastInsertId()>0){
            $feedback['status']=200;
        $feedback['msg']="{$company} added successfully ";
        echo json_encode($feedback);
        }else{
            $feedback['status']=201;
        $feedback['msg']="Error adding agencies";
        echo json_encode($feedback);
        }

    }
}

    ?>