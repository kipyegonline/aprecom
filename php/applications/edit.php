<?php
require_once("../db.php");

if(isset($_GET["handleEdit"]) AND $_GET['handleEdit']=="true"){
    
    
    $id=getEdits("id");
     $phone=getEdits("phone");
    $email=getEdits("email");
    $website=getEdits("website");
    $about=getEdits("about");
    $address=getEdits("address");
    $telephone=getEdits("editor");
    $posta=getEdits("editornum");
  
    
    updateValues($id,$phone,$email, $website,$about,$address, $telephone, $posta);

}

if(isset($_GET['insertSocials']) && $_GET['insertSocials']=="true"){

    $id=getEdits("id");
    $twitter=processSm("twitter");
    $facebook=processSm("facebook");
    $linkedin=processSm("linkedin");
    $youtube=processSm("youtube");
    $googlemap=processSm("googlemap");
    
    insertValues($id,$twitter,$facebook,$linkedin,$youtube,$googlemap);

}


function getEdits($val){

   $value=$_POST[$val];

    if(isset($value) && !empty($value) && $value !=="null"){
           
            return $value;
        }else{
           
            return false;
        }
       
   
    
    
}

function processSm($val){

    $value=$_POST[$val];

        if(isset($value) && !empty($value) && strlen($value)>0 && $value !=="null"){
           
            return $value;
        }else{
            
            return false;
        }
        
    
}
    

function  updateValues($id,$phone,$email, $website,$about,$address, $telephone, $posta){
    global $connection;
    $feedback=[];
    
    $sql="UPDATE aprecom_members 
    SET
    phone=:phone,
    telephone=:telephone,
    email=:email,
    website=:website,
    posta=:posta,
    address=:address,
    companyProfile=:about
    
    WHERE id=:id LIMIT 1";

    $stmt=$connection->prepare($sql);
    $stmt->execute(Array(
   ":phone"=>$phone,
   ":telephone"=>$telephone,
   ":email"=>$email,
   ":website"=>$website,
   ":about"=>$about,
   ":posta"=>$posta,
   ":address"=>$address,
   ":id"=>$id
    ));
   if($stmt->rowCount()>0){
    $feedback['status']=200;
    $feedback['msg']='Information updated successfully.';
    echo json_encode($feedback);
   }else{
    $feedback['status']=201;
    $feedback['msg']='Update failed. Make new changes and try again.';
    echo json_encode($feedback);
   }
    
    
}

function insertValues($id,$twitter,$facebook,$linkedin,$youtube,$googlemap){

    global $connection;
    $feedback=[];
    

    $checker="SELECT count(*) FROM social_media  WHERE company_id={$id}";
    
    $checkerRes=$connection->query($checker);
    
    if($checkerRes->fetchColumn()>0){

        $sql="UPDATE social_media SET 
        twitter=:twitter,
        facebook=:facebook,
        linkedin=:linkedin,
        youtube=:youtube,
        googlemap=:googlemap
         WHERE company_id=:company_id 
         LIMIT 1
        ";
        $stmt=$connection->prepare($sql);
        $stmt->execute(Array(
            ":twitter"=>$twitter,
            ":facebook"=>$facebook,
            ":linkedin"=>$linkedin,
            ":youtube"=>$youtube,
            ":googlemap"=>$googlemap,
            ":company_id"=>$id
            
        ));
        
        if($stmt->rowCount()>0){
            $feedback['status']=200;
            $feedback['msg']='Social media updated successfully.';
            echo json_encode($feedback);

        } else{
            $feedback['status']=201;
            $feedback['msg']='Update failed. Make new changes and try again.';
            echo json_encode($feedback);
        }

    }else{


        $sql="INSERT INTO social_media (twitter,facebook,linkedin,youtube,googlemap,company_id) 
        VALUES(:twitter,:facebook,:linkedin,:youtube,:googlemap,:company_id)";
        $stmt=$connection->prepare($sql);
        $stmt->execute(Array(
            ":twitter"=>$twitter,
            ":facebook"=>$facebook,
            ":linkedin"=>$linkedin,
            ":youtube"=>$youtube,
            ":googlemap"=>$googlemap,
            ":company_id"=>$id
            
        ));
        
        
        if($connection->lastInsertId()>0){
            $feedback['status']=200;
            $feedback['msg']='Social media inserted successfully.';
            echo json_encode($feedback);

        } else{
            $feedback['status']=201;
            $feedback['msg']='Social media insert failed. Try again later.';
            echo json_encode($feedback);
        }
    }



}

?>