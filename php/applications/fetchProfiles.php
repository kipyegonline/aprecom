<?php
require_once("../db.php");
include_once('../funcs/func.php');

//send directors
    if(isset($_GET['getDirectors']) AND $_GET['getDirectors']=="true" ){
        $company_id=getId($_GET['uuid']);
        $query="SELECT id, directorName as name,contact as num from company_directors  where company_id='{$company_id}'";
        sendItems($query);
    }
    //send employeees

    if(isset($_GET['getEmployees']) AND $_GET['getEmployees']=="true" ){
        $company_id=getId($_GET['uuid']);
        $query="SELECT id, employeeName as name,membershipNum as num from client_employees  where company_id='{$company_id}'";
        sendItems($query);
        
    }
    //send services

        if(isset($_GET['getclientServices']) AND $_GET['getclientServices']=="true" ){
            
            $company_id=getId($_GET['uuid']);
           
            $query="SELECT id, clientName as name,contact as num from client_services where company_id='{$company_id}'";
            sendItems($query);
        }

        if(isset($_GET['getUsername']) AND $_GET['getUsername']=="true" ){
            $err=[];
            $company_id=getVerified($_GET['uuid']);
            
            
        $sql="SELECT username from `applicants_account` where verification='{$company_id}'";
        $stmt=$connection->query($sql);
        if($stmt){
           $res= $stmt->fetch(PDO::FETCH_ASSOC);
           echo json_encode($res);
        }else{
            
            $err['status']=201;
            $err['error']="username not found {$company_id}";
            echo  json_encode($err);
        }

       

        }
        

    function sendItems($query){
        $err=[];
        global $connection;
        
        $stmt=$connection->query($query);
        if($stmt){
           $res= $stmt->fetchAll(PDO::FETCH_ASSOC);
           echo json_encode($res);
        }else{
            $err['status']=201;
            $err['error']="error fetching data";
            echo  json_encode($err);
        }
    }

    