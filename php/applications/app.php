<?php
require_once("../db.php");
include_once('../funcs/func.php');
$feedback=[];
if(isset($_GET['handleApplication']) AND $_GET['handleApplication']=="true" ){
   
  
   $company=getValue("company");
   $website=getValue("website");
   $email=getValue("email");
   $phone=getValue("phone");
   $telephone=getValue("telephone");
   $verified=getValue("verified");
   $posta=getvalue("posta");
   $year=getValue('year');
   $address=getValue('address');
   $ownership=getValue('ownership');
   $yearsOp=getValue('yearsOp');
   $regNumber=getValue('regNumber');
   $membershipContact=getValue("membershipContact");
   $edited=0;
   $category=getValue("category");

   
if(verify($company) && verify($email) &&verify($phone) && verify($telephone) && verify($year) &&verify($posta) && verify($address) &&verify($ownership) && verify($yearsOp) && verify($regNumber) &&verify($membershipContact)){

  
   try{
      $sql="UPDATE  `aprecom_members` SET
      company=:company,
      website=:website,
      email=:email,
      phone=:phone,     
      edited=:edited,     
      telephone=:telephone, 
      posta=:posta, 
      yearOfReg=:yearOfReg,       
       address=:address, 
       ownership=:ownership, 
       yearsOp=:yearsOp,  
       regNumber=:regNumber,
       membershipContact=:membershipContact 
        WHERE id=:uuid LIMIT 1";
      
      $stmt=$connection->prepare($sql);
      if($stmt){


      $stmt->execute(Array(
      ":company"=>$company,
      ":website"=>$website,
      ":email"=>$email,
      ":phone"=>$phone,
      ":edited"=>$edited,
      ":telephone"=>$telephone,
      ":posta"=>$posta,
      ":yearOfReg"=>$year,
      ":address"=>$address,
      ":ownership"=>$ownership,
      ":yearsOp"=>$yearsOp,
      ":regNumber"=>$regNumber,
      ":membershipContact"=>$membershipContact,
      ":uuid"=>getId($verified)
   ));
 

   $feedback['status']=200;
   $feedback['msg']="Information submitted..redirecting to part 2";

      
      echo json_encode($feedback);


      
}else{
   $feedback['status']=201;
   $feedback["error"]="Error adding information to database. Try again later";

   echo json_encode($feedback);
}
         }
      catch(PDOException $e){
              echo "Error adding application.Try again later ". $e->getMessage();
          }



}else{
   $feedback['status']=201;
   $feedback["error"]="Some fields are missing";
   echo json_encode($feedback); 
}




   

}

if(isset($_GET['getAppData']) AND $_GET['getAppData']=="true" ){
   $uuid=getId($_GET['uuid']);
$sql="SELECT * FROM `aprecom_members` WHERE id={$uuid} LIMIT 1";

$stmt=$connection->query($sql);
if($stmt){
   $data=$stmt->fetchAll(PDO::FETCH_ASSOC);
   echo json_encode($data);
}

}


function verify($value){
   if(strlen(isset($value))>0){
      return true;
   }
}



?>