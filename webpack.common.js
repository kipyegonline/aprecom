
const Webpack=require('webpack');
const CleanWebpackPlugin=require('clean-webpack-plugin');
const HtmlWebpackPlugin=require('html-webpack-plugin');




module.exports={
    entry:'./src/js/App.js',

    module:{
        rules:[
            {
                test:/\.js$/,
                exclude:/node_modules/,
                use:[{
                    loader:'babel-loader',
                    options:{
                        presets:['@babel/preset-env','@babel/preset-react']
                    }
                }]
            },
            {
                test:/\.html$/,
                use:['html-loader']
            },
            {
                test:/\.(png|jpg|jpeg|csv|svg)$/,
                use:[{
                    loader:'file-loader',
                    options:{
                        name:'[name].[hash].[ext]',
                        outputPath:'images',
                        publicPath:'./images'
                    }
                    
                }]
            },
            
        ]
    },
    plugins:[
        new CleanWebpackPlugin({
            verbose:true
        }),
        new HtmlWebpackPlugin({
            template:'./index.html'
        }),
        new Webpack.ProvidePlugin({
            $:'jquery',
            jQuery:'jquery',
           
            React:'react'
        }),
        
        
    ],
    
};