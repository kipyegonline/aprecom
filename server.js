
const React=require("react")
const path=require("path")
const  renderToString=require("react-dom/server").renderToString
const express = require("express");
const Home=require("./src/js/pages/Home/Home").default;
const app = express();
//const  {html} =require("./src/js/App")
const logger=(req,res,next)=>{
  console.log(`${req.method} request for ${req.url}`)
  next()
}
app.use(logger)
app.use(express.static("public"))
global.React=React;

app.get("/render",(req,res)=>{
  console.log('client-serv')
  if (typeof window === 'undefined') {
    global.window = {}
  }
  if (typeof document === 'undefined') {
    global.document= {}
  }
  const html=renderToString(<Home/>)
  const content=`
  <html>
  <head></head>
  <body>
  <div id="roo">${html}</div>
  </body>
  <script src="bundle.js"></script>
  </html>`
  res.status(200)
  .send(content)
});

app.listen(5000, function() {

    console.log("Express App running again on port 5000");

});
